import scipy.special as spec
import scipy as sp
import pylab as pl


gamma = 1.17
beta = (1-gamma**(-2))**0.5
c0 = 3*10**8
eps0 = 8.85*10**-12
Z0=377
a = 0.001
b = 0.005
d = 0.015
er_graph = 10
cond_graph = 7*10**4
mur_graph = 1
loss_graph = 0


def wave_vec(freq):
    return 2*sp.pi*freq/(beta*gamma)

def eps(freq, er, cond):
    return complex(er, -cond/(2*sp.pi*freq*eps0))

def mu(freq, mur, loss):
    return mur*complex(1, sp.tan(loss))

def nu(freq, er, cond, mur, loss):
    return wave_vec(freq)*(1-beta**2*eps(freq,er,cond)*mu(freq,mur,loss))**0.5

def x_1(freq):
    return wave_vec(freq)*b/gamma

def x_2(freq, er, cond, mur, loss):
    return nu(freq, er, cond, mur, loss)*b

def s(freq):
    s = wave_vec(freq)*a/gamma

def P_1(freq):
    return spec.ivp(1, x_1(freq), 1)/spec.iv(1, x_1(freq))

def Q_1(freq):
    return spec.kvp(1, x_1(freq), 1)/spec.kv(1, x_1(freq))

def P_2(freq, er, cond, mur, loss):
    return spec.ivp(1, x_2(freq, er, cond, mur, loss), 1)/spec.iv(1, x_2(freq, er, cond, mur, loss))

def Q_2(freq, er, cond, mur, loss):
    return spec.kvp(1, x_2(freq, er, cond, mur, loss), 1)/spec.kv(1, x_2(freq, er, cond, mur, loss))

def alpha_2(freq, er, cond, mur, loss):
    return spec.kv(1, y)*spec.iv(1, x_2(freq, er, cond, mur, loss))/(spec.iv(1,y)*spec.kv(1,x_2(freq, er, cond, mur, loss)))

def eta_2(freq, er, cond, mur, loss):
    return spec.kvp(1, y, 1)*spec.iv(1, x_2(freq, er, cond, mur, loss))/(spec.ivp(1,y,1)*spec.kv(1,x_2(freq, er, cond, mur, loss)))

def Q_eta(freq, er, cond, mur, loss):
    return Q_2(freq, er, cond, mur, loss)-eta_2(freq, er, cond, mur, loss)*P_2(freq, er, cond, mur, loss)/(1-eta_2(freq, er, cond, mur, loss))

def Q_alpha(freq, er, cond, mur, loss):
    return Q_2(freq, er, cond, mur, loss)-alpha_2(freq, er, cond, mur, loss)*P_2(freq, er, cond, mur, loss)/(1-alpha_2(freq, er, cond, mur, loss))

def Z_trans_re(freq, er, cond, mur, loss):
    return complex(0,Z0*spec.iv(1, s(freq))**2*spec.kv(1,x_1(freq))/(sp.pi*a**2*beta*gamma**2*spec.iv(1,x_1(freq)))* \
                   (gamma*nu(freq, er, cond, mur, loss)*(P_1(freq)-Q_1(freq))*(beta*x_1(freq)*x_2(freq, er, cond, mur, loss))**2* \
                    (gamma*nu(freq,er,cond,mur,loss)*P_1(freq) - k(freq)*mu(freq, mur, loss)*Q_eta(freq, er, cond, mur,loss)) / \
                    ((gamma*nu(freq,er,cond,mur,loss)*x_2(freq,er,cond,mur,loss)-wave_vec(freq)*x_1(freq))**2 - (beta*x_1(freq)*x_2(freq,er,cond,mur,loss))**2*(gamma*nu(freq,er,cond,mur,loss)*P_1(freq) -\
                    wave_vec(freq)*mu(freq,mur,loss)*Q_eta(freq,er,cond,mur,loss))*(gamma*nu(freq,er,cond,mur,loss)*P_1(freq) - wave_vec(freq)*eps(freq,er,cond)*Q_alpha(freq,er,cond,mur,loss)))))

print spec.ivp(1,100,1)

freq_list = []
for i in range(0,9,1):
    for j in range(1,100,1):
        freq_list.append(float((j/10.0)*10.0**i))

freq_list = list(set(freq_list))
freq_list.sort()

print Z_trans_re(1.0,er_graph,cond_graph,mur_graph,loss_graph)

test = []
for i in freq_list:
    print Z_trans_re(i,er_graph,cond_graph,mur_graph,loss_graph)
    test.append(Z_trans_re(i,er_graph,cond_graph,mur_graph,loss_graph))
