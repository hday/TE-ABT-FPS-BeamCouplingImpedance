import scipy as sp
import csv
import pylab as pl
import scipy.optimize as op

Z0 = 377.0
e0 = 8.85*10**-12  # Permitivitty of free space
rho = 10**6        # Resistivity of ferrite
muprime = 460.0    # mu prime
tau = 1.0/(20.0*10**6) # relaxation time
er=12.0             # Relative permitivitty

directory = "E:/PhD/1st_Year_09-10/Data/Material Properties/"
# Data directory


def mur(freq):
    return (1 + (muprime/(1+complex(0,1)*tau*freq)))

def epsr(freq):
    return e0*(er - 1j/(2*sp.pi*freq*e0*rho))

mu_real =[]
mu_imag = []
eps_real=[]
eps_imag=[]

fitFunc = lambda p, x: p[0]+p[0]**2*(p[1]*p[2]**2)/(p[2]**2+complex(0,1)*x*p[3]-x**2)
errFunc = lambda p, x, y, err: (y-fitFunc(p, x).imag)

freq_list = []
for i in range(4,10,1):
    for j in range(1,1000,1):
        freq_list.append(float((j/10.0)*10.0**i))

freq_list = list(set(freq_list))
freq_list.sort()
freqList=sp.array(freq_list)
print len(freqList)
murPlot = mur(freqList)

##### Test fit for input to GdFidl #####

pInit = [5.0, 220, 100.0*10**6, 20.1*10.0**9]
temp=murPlot.imag
yErr=temp
pFinal, something = op.leastsq(errFunc, pInit, args=(freqList, temp, yErr))

test = fitFunc(pInit, freqList)
testReal = fitFunc(pFinal, freqList)
print pFinal
pl.semilogx()
pl.plot(freqList, murPlot.real, "b-", label="actual $\mu_{r}^{'}$")
pl.plot(freqList, murPlot.imag, "r-", label="actual $\mu_{r}^{''}$")
##pl.plot(freqList, test.imag, label="Initial")
pl.plot(freqList, testReal.real, "g-", label="Fit $\mu_{r}^{'}$")
pl.plot(freqList, testReal.imag, "k-", label="Fit $\mu_{r}^{''}$")
pl.legend()
pl.show()
pl.clf()

