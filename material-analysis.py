import scipy as sp
import pylab as pl
import csv,time,json

def eps_linear(s11, frequency):
    wavelength = 3*10**8/frequency          # Obtain frequency
    d = 0.003                               # define thickness of stripline
    eps = -3*wavelength**3/(8*sp.pi**3*d**3)*(-(1+s11)/(s11-1) - 2*sp.pi*d/wavelength)
    return eps

def eps_quadratic_positive(s11, frequency):
    wavelength = 3*10**8/frequency          # Obtain frequency
    d = 0.003                               # define thickness of stripline
    eps = 5*wavelength**2/(16*sp.pi**2*d**2) + (25*wavelength**4/(256*sp.pi**4*d**4) + 15*wavelength**5/(64*sp.pi**5*d**5)*(-(1+s11)/(s11-1) - 2*sp.pi*d/wavelength))
    return eps

def eps_quadratic_negative(s11, frequency):
    wavelength = 3*10**8/frequency          # Obtain frequency
    d = 0.003                               # define thickness of stripline
    eps = 5*wavelength**2/(16*sp.pi**2*d**2) - (25*wavelength**4/(256*sp.pi**4*d**4) + 15*wavelength**5/(64*sp.pi**5*d**5)*(-(1+s11)/(s11-1) - 2*sp.pi*d/wavelength))
    return eps

start = time.time()
directory = "C:/Users/hday/Documents/PhD/1st_Year_09-10/Software/materials/" #Directory of data
data_file = "SC-open.s2p"      # File containing data

input_file = open(directory+data_file, "r+")

data_raw = csv.reader(input_file, delimiter=' ')       #Assign and open input file

s11_data= []    # Create blank for data
wire_t = 5*10**-4                  # Radius of wire
seperation = 1.6*10**-2            # Seperation of plates

i = 0

for row in data_raw:
    if i<6:
        pass
    else:    
        for i in range(0,len(row)):
            row[i] = float(row[i])

        s11_data.append([row[0], complex(10**(row[1]/20)*sp.cos(row[2]),10**(row[1]/20)*sp.sin(row[2]))])                  
    i=i+1
input_file.close()                              # close input file

output_file = open("test_output.txt", 'w+')
for i in s11_data:
    json.dump(i, output_file)

output_file.close()

freq_list = []
for i in range(0, len(s11_data)):
    freq_list.append(s11_data[i][0])
                     
eps_list_real = []
eps_list_imag = []

for term in s11_data:
    eps_list_real.append(eps_linear(term[1],term[0]).real)
    eps_list_imag.append(eps_linear(term[1],term[0]).imag)

figure = pl.figure()
figure.clf()
ax= figure.add_subplot(1,1,1)

line1 = ax.plot(freq_list,eps_list_real, color='black')
line2 = ax.plot(freq_list,eps_list_imag, color='red')
ax.set_yscale('linear')
ax.set_xscale('linear')
pl.xlabel("Frequency (MHz)")                  #Label axes
pl.ylabel("Impedance (Ohms/m)")
pl.savefig('eps_linear.png')

eps_list_real = []
eps_list_imag = []

for term in s11_data:
    eps_list_real.append(eps_quadratic_positive(term[1],term[0]).real)
    eps_list_imag.append(eps_quadratic_positive(term[1],term[0]).imag)

figure = pl.figure()
figure.clf()
ax= figure.add_subplot(1,1,1)

line1 = ax.plot(freq_list,eps_list_real, color='black')
line2 = ax.plot(freq_list,eps_list_imag, color='red')
ax.set_yscale('linear')
ax.set_xscale('linear')
pl.xlabel("Frequency (MHz)")                  #Label axes
pl.ylabel("Impedance (Ohms/m)")
pl.savefig('eps_quadratic_positive.png')

eps_list_real = []
eps_list_imag = []

for term in s11_data:
    eps_list_real.append(eps_quadratic_negative(term[1],term[0]).real)
    eps_list_imag.append(eps_quadratic_negative(term[1],term[0]).imag)

figure = pl.figure()
figure.clf()
ax= figure.add_subplot(1,1,1)

line1 = ax.plot(freq_list,eps_list_real, color='black')
line2 = ax.plot(freq_list,eps_list_imag, color='red')
ax.set_yscale('linear')
ax.set_xscale('linear')
pl.xlabel("Frequency (MHz)")                  #Label axes
pl.ylabel("Impedance (Ohms/m)")
pl.savefig('eps_quadratic_negative.png')

##
##output_file = open(directory+"output_imag.csv", 'w+')           #Assign and open output file
##
##data_out = csv.writer(output_file, delimiter=',', lineterminator = '\n')
##data_out.writerows(coefficients)
##
##images2swf.writeSwf(directory+"movie_imag.swf", image_list,3)
##output_file.close() 

print time.time() - start

