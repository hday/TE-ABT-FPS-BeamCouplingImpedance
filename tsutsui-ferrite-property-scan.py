import sympy.mpmath as mpmath
import scipy, numpy, csv, time, os, sys
from matplotlib import pyplot
import numbers
import pylab as pl

Z0 = 377.0
a = 0.02725
b = 0.027
d = b+0.0415
length = 1
disp = 1
e0 = 8.85*10**-12
eprime = 12.0
rho = 10**6
muprime = 460.0
tau = 1.0/(20.0*10**6)
er=12.0
mu0 = 4*scipy.pi*10**-7

def k(freq):
    return 2*scipy.pi*freq/(3*10**8)

def kxn(n):
    return ((2*n + 1)*scipy.pi)/(2*a)

def kyn(n,freq):
    return ((er*mur(freq) - 1)*k(freq)**2 - kxn(n)**2)**0.5

def sh(n):
    return mpmath.sinh(kxn(n)*b)

def ch(n):
    return mpmath.cosh(kxn(n)*b)

def tn(n,freq):
    return mpmath.tan(kyn(n,freq)*(b-d))

def ct(n,freq):
    return mpmath.cot(kyn(n,freq)*(b-d))

def FX(n,freq):
    return (kxn(n)/k(freq))*((1+(mur(freq)*er))*sh(n)*ch(n))

def FY(n,freq):
    return (kyn(n,freq)/k(freq))*(mur(freq)*(sh(n)**2)*tn(n,freq) - er*(ch(n)**2)*ct(n,freq))

def FY_2(n,freq):
    return (kyn(n,freq)/k(freq))*(mur(freq)*(ch(n)**2)*tn(n,freq) - er*(sh(n)**2)*ct(n,freq))

def ferrite_long(freq):
    return (1j*Z0)/(2*a) * (mpmath.nsum(lambda n: (((FX(n,freq) +FY(n,freq))/(er*mur(freq)-1)) - ((k(freq)*sh(n)*ch(n))/kxn(n)))**(-1), [0,10]))

def ferrite_dip_horz(freq):
    return (1j*Z0)/(2*a) * mpmath.nsum(lambda n: (((kxn(n)**2)/k(freq))*(((FX(n,freq) + FY(n,freq))/(er*mur(freq)-1) - (k(freq)*sh(n)*ch(n)/kxn(n))))**-1), [0, 10])

def ferrite_dip_vert(freq):
    return (1j*Z0)/(2*a) * mpmath.nsum(lambda n: (((kxn(n)**2)/k(freq))*(((FX(n,freq) + FY_2(n,freq))/(er*mur(freq)-1) - (k(freq)*sh(n)*ch(n)/kxn(n))))**-1), [0, 10])

def ferrite_quad_horz(freq):
    return (-1j*Z0)/(2*a) * mpmath.nsum(lambda n: (((kxn(n)**2)/k(freq))*(((FX(n,freq) + FY(n,freq))/(er*mur(freq)-1) - (k(freq)*sh(n)*ch(n)/kxn(n))))**-1), [0, 10])

def ferrite_quad_vert(freq):
    return (1j*Z0)/(2*a) * mpmath.nsum(lambda n: (((kxn(n)**2)/k(freq))*(((FX(n,freq) + FY(n,freq))/(er*mur(freq)-1) - (k(freq)*sh(n)*ch(n)/kxn(n))))**-1), [0, 10])


start = time.time()

for i in range(5,15,1):
    mod = i/10.0

    def mur(freq):
        return (1 + (mod*muprime - 1j*freq*tau*muprime)/(1+tau**2*freq**2))

    temp_real = []
    temp_imag = []

    freq_list = []
    for i in range(6,10,1):
        for j in range(1,100,1):
            freq_list.append(float((j/10.0)*10.0**i))

    freq_list = list(set(freq_list))
    freq_list.sort()

    for i in freq_list:
        temp_real.append(ferrite_long(i).real*length)
        temp_imag.append(ferrite_long(i).imag*length)

    target_directory = "E:/PhD/1st_Year_09-10/Software/muprime_scan/"
    try:
        os.mkdir(target_directory)
    except:
        pass
    try:
        os.mkdir(target_directory+"video/")
    except:
        pass

    output = open(target_directory+"muprime_"+str(mod)+'_mki-long.csv', 'w+')
    for i in range(0,len(temp_real)):
                  output.write(str(freq_list[i])+","+str(temp_real[i])+','+str(temp_imag[i])+'\n')

    output.close()
    pl.clf()
##    pl.semilogy()
    name = mur(10.0**5)
    pl.plot(freq_list,temp_real, color='black')
    pl.plot(freq_list,temp_imag, color='red')
    pl.xlabel("Frequency (Hz)")                  #Label axes
    pl.ylabel("Impedance ($\Omega/m$)")
    pl.figtext(0.1,0.9, "$\mu '(0.1MHz) = $"+str(name.real), size=12.0, bbox={'facecolor':'white', 'pad':10})
##    pl.savefig(target_directory+"muprime_"+str(mod)+'_mki-long.png')
    pl.axis([0,2*10**9,-400,800])
    pl.savefig(target_directory+"video/"+str(int(mod*10))+'.png')

    x= []
    temp_real = []
    temp_imag = []

    for i in freq_list:
        temp_real.append(ferrite_dip_vert(i).real*length*disp)
        temp_imag.append(ferrite_dip_vert(i).imag*length*disp)


for i in range(1,20,1):
    mod = i/10.0

    def mur(freq):
        return (1 + (muprime - 1j*freq*tau*muprime*mod)/(1+tau**2*freq**2))

    temp_real = []
    temp_imag = []

    freq_list = []
    for i in range(6,10,1):
        for j in range(1,100,1):
            freq_list.append(float((j/10.0)*10.0**i))

    freq_list = list(set(freq_list))
    freq_list.sort()

    for i in freq_list:
        temp_real.append(ferrite_long(i).real*length)
        temp_imag.append(ferrite_long(i).imag*length)

    target_directory = "E:/PhD/1st_Year_09-10/Software/mudoubleprime_scan/"
    try:
        os.mkdir(target_directory)
    except:
        pass
    try:
        os.mkdir(target_directory+"video/")
    except:
        pass

    output = open(target_directory+"mudoubleprime_"+str(mod)+'_mki-long.csv', 'w+')
    for i in range(0,len(temp_real)):
                  output.write(str(freq_list[i])+","+str(int(mod*10))+','+str(temp_imag[i])+'\n')

    output.close()
    pl.clf()
##    pl.semilogy()
    name = mur(2.0*10**7)
    pl.plot(freq_list,temp_real, color='black')
    pl.plot(freq_list,temp_imag, color='red')
    pl.xlabel("Frequency (Hz)")                  #Label axes
    pl.ylabel("Impedance ($\Omega/m$)")
    pl.axis([0,2*10**9,-400,800])
    pl.figtext(0.1,0.9, "$\mu ''(20MHz) = $"+str(name.imag), size=12.0, bbox={'facecolor':'white', 'pad':10})
##    pl.savefig(target_directory+"mudoubleprime_"+str(mod)+'_mki-long.png')
    pl.savefig(target_directory+"video/"+str(int(mod*10))+'.png')

    x= []
    temp_real = []
    temp_imag = []

    for i in freq_list:
        temp_real.append(ferrite_dip_vert(i).real*length*disp)
        temp_imag.append(ferrite_dip_vert(i).imag*length*disp)




print time.time() - start
