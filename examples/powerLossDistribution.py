"""
This file contains an example of importing and presenting impedance models and loss maps
using the impedance.ImpedanceModel class.
"""

import csv, time, os, sys
import scipy as sp
import pylab as pl
import scipy.optimize as op
import scipy.interpolate as interp
import scipy.integrate as integrate
import scipy.fftpack as fftPack
import mpl_toolkits.mplot3d as ToolKit3d
sys.path.append("../")
import impedance.impedance as imp

def minValIndex(arrMark, minValue=10.0):
    ###function returns an array of indices of contents of arrMark that fall below value minValue
    return sp.array([i for i in range(0,len(arrMark)) if arrMark[i]>minValue])

start=time.time()

### Define some parameters of beam current for power loss calculations

C = 299792458.0
f_rev = C/27000.0
nBunches = 2808
qPart = 1.6*10.0**-19
nPart=1.15*10.0**11
bCur = nPart*qPart*nBunches*f_rev
bLength=1.001*10.0**-9
testImp = [250*10**6, 10.0, 10000]
bunchCur=nPart*qPart*f_rev
print bCur, bunchCur

##### Directory definiton #####
##### ImpedanceModel takes a directory as an argument at iniatian. It's expected that this
##### directory contains an impedance file called longitudinal-impedance-real.txt and some
##### loss maps of form "f****.txt" where **** is the frequency is MHz. Any number of loss
##### maps may be in the directory

MKIPostLS1FerriteCollar8mmOffset4B3 = imp.ImpedanceModel("./117mmOffsetLS1FerriteRingOffset/", "117mm Overlap Post-LS1 Ferrite Ring Offset 8mm 4B3") #Creates object and labels
MKIPostLS1FerriteCollar8mmOffset4B3.ReadLossFiles() #Reads loss files
MKIPostLS1FerriteCollar8mmOffset4B3.BinnedLosses() #Calculates binned losses from imported loss maps. Bins created by finding zeros (i.e. within objects)
MKIPostLS1FerriteCollar8mmOffset4B3.CalcGaussPowLoss(bCur, 25.0*10**-9, bLength) #Calculates power loss assuming a given beam current, bunch separation and bunch length

meshtime=time.time()-start
print meshtime

##### ImpedanceModel.CalcGaussPowLoss returns 3 items, total power loss, power loss by frequnecy, and cumulative power loss along z axis
print MKIPostLS1FerriteCollar8mmOffset4B3.powLossTot, sp.sum(MKIPostLS1FerriteCollar8mmOffset4B3.freqPowLoss[:,1]), MKIPostLS1FerriteCollar8mmOffset4B3.cumPowLoss[-1]

MKIPostLS1FerriteCollar8mmOffset4B3.PlotBinnedLosses() #Plots binned losses
pl.xlabel("Displacement from centre of magnet (mm)", fontsize=24.0)
pl.ylabel("Power Loss (W/m)", fontsize=24.0)
##pl.ylabel("Power Loss (a.u)", fontsize=16.0)
pl.legend(loc="upper right")
pl.show()
pl.clf()

pl.plot(MKIPostLS1FerriteCollar8mmOffset4B3.zMesh, MKIPostLS1FerriteCollar8mmOffset4B3.lossMapProf, label="Weighted Losses Summed FerriteCollarOffset 4B3") #plots frequency weighted loss map, unbinned
pl.legend()
pl.show()
pl.clf()

MKIPostLS1FerriteCollar8mmOffset4B3.PlotImpedance() #plots real impedance
pl.xlabel("Frequency (GHz)", fontsize=24.0)
pl.ylabel("$Z_{\parallel}$ ($\Omega$)", fontsize=24.0)
##pl.ylabel("Power Loss (a.u)", fontsize=16.0)
pl.xlim([0,2.0])
pl.legend(loc="upper left")
pl.show()
pl.clf()

MKIPostLS1FerriteCollar8mmOffset4B3.PlotCumBinnedLosses() #Plots cumulative losses calculated from binned values
pl.xlabel("Displacement from centre of magnet (mm)", fontsize=24.0)
pl.ylabel("Power Loss (W)", fontsize=24.0)
##pl.ylabel("Power Loss (a.u)", fontsize=16.0)
pl.legend(loc="upper left")
pl.show()
pl.clf()
