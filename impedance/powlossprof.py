import csv, time, os, sys
import scipy as sp
import pylab as pl
import scipy.optimize as op
import scipy.interpolate as interp
import scipy.integrate as integrate
import scipy.fftpack as fftPack
import mpl_toolkits.mplot3d as ToolKit3d

def gaussProf(freq, bLength):
    """ Returns gaussian profile at frequency freq for a bunch of length bLength
    """
    sigma = bLength/(4.0*2.0*sp.log(2.0))
    return sp.e**(-(2.0*sp.pi*freq*sigma)**2)

def cosProf(freq, bunLength):
    """ Returns cos^2 profile at frequency freq for a bunch of length bLength
    """
    return (sp.sin(sp.pi*freq*bunLength)/(sp.pi*freq*bunLength*(1-(freq*bunLength)**2)))**2


def gaussProfFreq(freq, bLength):
    sigma = bLength/(4.0*2.0*sp.log(2.0))
    return sp.e**(-(2.0*sp.pi*freq*sigma)**2/2)

def cosProfFreq(freq, bunLength):
    return (sp.sin(sp.pi*freq*bunLength)/(sp.pi*freq*bunLength*(1-(freq*bunLength)**2)))**2

def paraProfFreq(freq, bunLength):                                     
    return (sp.sin(sp.pi*freq*bunLength)*((2*sp.pi*freq)**2*bunLength**2)+(8*sp.cos(sp.pi*freq*bunLength)*2*sp.pi*bunLength))/(2*sp.pi*freq)**2/bunLength**2
    return sp.sin(sp.pi*freq*bunLength)/(sp.pi*freq)

def gaussProfTime(dist, bLength, order):
    sigma = bLength/(4.0*2.0*sp.log(2.0))
    return sp.e**(-(dist/sigma)**2/2)

def paraProfTime(dist, bunch_length):
    if bunch_length/2 <= dist:
        return 0
    elif bunch_length/2 >= dist:
        return 1-(2*dist/bunch_length)**2
    else:
        return 0

def cosProfTime(dist, bunch_length):
    if bunch_length/2 <= dist:
        return 0
    if bunch_length/2 >= dist:
        return sp.cos(sp.pi/bunch_length*dist)**2
    else:
        return 0

def heatingValGauss(impArr, beamCur, bunSpac, bunLen):
    """ Returns the power loss assuming a gaussian profile of length bLength
    of a beam with bunch spacing bunSpac, beam current beamCur interacting with
    an impedance impArr
    """
    SplineFitImp = interp.InterpolatedUnivariateSpline(impArr[:,0], impArr[:,1])
    freqListHeating = sp.linspace(1/bunSpac/10**9,impArr[-1,0],impArr[-1,0]*10**9*bunSpac)
    splineFitHeatingImp = SplineFitImp(freqListHeating)
    heatingTotalPart = []
    for i in range(0,len(splineFitHeatingImp)):
        heatingTotalPart.append([freqListHeating[i], abs(2*splineFitHeatingImp[i]*(beamCur**2)*gaussProf(freqListHeating[i]*10.0**9, bunLen))])
    heatingTotalPart=sp.array(heatingTotalPart)
    return heatingTotalPart[:,1].sum(), heatingTotalPart


