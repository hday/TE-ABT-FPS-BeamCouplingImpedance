import csv, os, sys, time
import scipy as sp
import pylab as pl

def gauss_prof(dist, sigma, order):
    return sp.e**(-order*(2*dist/(sigma))**2)

def para_prof(dist, bunch_length):
    return 1-(2*dist/bunch_length)**2

def cos_prof(dist, bunch_length):
    return sp.cos(sp.pi/bunch_length*dist)**2

start = time.time()

C = 299792458.0
circ=26659.0
n_bunches = 1404
p_bunch = 1.15*10**11
Q_part = 1.6*10**-19
f_rev = 2*10**7



I_b = p_bunch*Q_part*f_rev
power_tot = I_b**2#*n_bunches

print I_b, power_tot


mark_length = 1.1*10**-9
time_domain_parabolic = []
time_domain_cos = []
time_domain_gauss = []
time_domain_gauss_trunc = []
data_points = 10000.0
sample_rate = 0.1/(mark_length/data_points)
bunch_length = 1.2*10**-9

## For 50ns spacing 22.726

t=pl.r_[-22.726*mark_length:22.726*mark_length:1/sample_rate]

## For 25ns spacing
##    t=pl.r_[-11.357*mark_length:11.357*mark_length:1/sample_rate]
    ##print len(t)
omega = sample_rate

for i in t:
    if abs(i)<=bunch_length/2:
        time_domain_parabolic.append(para_prof(i,bunch_length))           ## Parabolic bunch profile
        time_domain_cos.append(cos_prof(i,bunch_length))  ## cos^2 profile
        time_domain_gauss_trunc.append(gauss_prof(i,bunch_length,3))           ## gaussian profile
    else:
        time_domain_parabolic.append(0)
        time_domain_cos.append(0)
        time_domain_gauss_trunc.append(0)
    time_domain_gauss.append(gauss_prof(i,bunch_length,3))           ## gaussian profile

        
time_domain_parabolic = sp.array(time_domain_parabolic)
time_domain_cos = sp.array(time_domain_cos)
time_domain_gauss = sp.array(time_domain_gauss)
time_domain_gauss_trunc = sp.array(time_domain_gauss_trunc)
N=len(t)
freq_domain_parabolic=sp.fft(time_domain_parabolic)
print time_domain_parabolic
freq_domain_cos=sp.fft(time_domain_cos)
freq_domain_gauss=sp.fft(time_domain_gauss)
freq_domain_gauss_trunc=sp.fft(time_domain_gauss_trunc)
f = sample_rate*sp.r_[0:(N/2)]/N
n=len(f)

freq_domain_parabolic = freq_domain_parabolic[0:n]/freq_domain_parabolic[0]
freq_domain_cos = freq_domain_cos[0:n]/freq_domain_cos[0]
freq_domain_gauss = freq_domain_gauss[0:n]/freq_domain_gauss[0]
freq_domain_gauss_trunc = freq_domain_gauss_trunc[0:n]/freq_domain_gauss_trunc[0]

freq_domain_parabolic = sp.real(20*sp.log(freq_domain_parabolic))
freq_domain_cos = sp.real(20*sp.log(freq_domain_cos))
freq_domain_gauss = sp.real(20*sp.log(freq_domain_gauss))
freq_domain_gauss_trunc = sp.real(20*sp.log(freq_domain_gauss_trunc))

pl.plot(t*10**9, time_domain_parabolic, 'k-', label="Parabolic")
pl.plot(t*10**9, time_domain_cos, 'r-', label="cos$^{2}$")
pl.plot(t*10**9, time_domain_gauss, 'b-', label="Gaussian")
pl.plot(t*10**9, time_domain_gauss_trunc, 'g-', label="Truncated Gaussian")
pl.legend(loc="upper left")
pl.xlabel("Time (ns)", fontsize = "16")
pl.ylabel("Amplitude", fontsize = "16")
pl.axis([-1.5*bunch_length*10**9, 1.5*bunch_length*10**9, 0, 1])
pl.show()
pl.clf()

##pl.semilogy()
pl.plot(f/10**9, freq_domain_parabolic, 'k-',label="Parabolic")
pl.plot(f/10**9, freq_domain_cos, 'r-', label="cos$^{2}$")
pl.plot(f/10**9, freq_domain_gauss, 'b-', label="Gaussian")
##pl.plot(f/10**9, freq_domain_gauss_trunc, 'g-', label="Truncated Gaussian")
pl.legend(loc="upper right")
pl.xlabel("Frequency (GHz)", fontsize = "16")
pl.ylabel("Current Amplitude (dB)", fontsize = "16")
pl.axis([0, 2, -100, 0])
pl.show()
pl.clf()

print "Time Taken for code to run = %f (s)" % (time.time()-start)
