import scipy as sp
import pylab as pl
import time, os, sys
import scipy.integrate as inte

###### Define Impedance from resonator ######

def Z_bb(freq, data):
    return data[2]/complex(1, data[1]*(freq/data[0] - data[0]/freq))

###### Define constants ####
charge = 18.4*10**-9
t_bunch = 50*10**-9
sigma_z = 0.075
C = 299792458.0
circ = 26679.0
eps0 = 8.854 * 10 **-12
mu0 = 4*sp.pi*10**-7
Z0 = 120*sp.pi
start=time.time()
bound_typ = "PerfE"
x01 = 0.001
x02 = 0.0011
dy = 0.001
power_loss_tot = 0.0
top_directory = "E:/PhD/1st_Year_09-10/Data/SLAC-phase-2-collimator/eigenmode_data/2mm_long/"

eigenmode_store = [] ###### first column - freq, second - Q, Third R, Fourth - Power Loss

directory_list = [top_directory+i+"/" for i in os.listdir(top_directory)]

for work_directory in directory_list:

    if work_directory == top_directory+"tex_output/":
        pass
    else:
        for pos in os.listdir(work_directory):
            if "PerfE" in pos:
                bound_typ = "PerfE"
            if "PerfH" in pos:
                bound_typ = "PerfH"
##        print work_directory
        file_input = open(work_directory+"E_field_"+bound_typ+".fld", 'r+')
        data = file_input.readlines()
        Ez = []
        for i in data[2:]:
            temp = map(float, i.rsplit())
            Ez.append([temp[2],complex(temp[-2],temp[-1])])

        ##Ez = sp.array(Ez)

        ##print Ez
        if bound_typ == "PerfE":
            temp = []
            for i in range(1,len(Ez)):
                temp.append([-Ez[-i][0], complex((Ez[-i][1]).real,(Ez[-i][1]).imag)])
            for i in range(0,len(Ez)):
                temp.append([Ez[i][0],complex((Ez[i][1]).real,(Ez[i][1]).imag)])
            Ez = sp.array(temp)
        elif bound_typ == "PerfH":
            temp = []
            for i in range(1,len(Ez)):
                temp.append([-Ez[-i][0], complex(-(Ez[-i][1]).real,-(Ez[-i][1]).imag)])
            for i in range(0,len(Ez)):
                temp.append([Ez[i][0], complex((Ez[i][1]).real,(Ez[i][1]).imag)])
            Ez = sp.array(temp)

        ##pl.plot(Ez[:,0], Ez[:,1].real)
        ##pl.plot(Ez[:,0], Ez[:,1].imag)
        ##pl.show()
        ##pl.clf()

        potential = 0.0
        gap = (Ez[1][0]-Ez[0][0])

        potential = inte.simps(abs(Ez[:,1]), Ez[:,0], gap)

        file_input = open(work_directory+"E_field_"+bound_typ+".fld", 'r+')
        data = file_input.readlines()
        Ey = []
        for i in data[2:]:
            temp = map(float, i.rsplit())
            Ey.append([temp[2],complex(temp[-4],temp[-3])])

        ##Ey = sp.array(Ey)
        ##print Ey

        if bound_typ == "PerfE":
            temp = []
            for i in range(1,len(Ey)):
                temp.append([-Ey[-i][0], complex(-(Ey[-i][1]).real,-(Ey[-i][1]).imag)])
            for i in range(0,len(Ey)):
                temp.append([Ey[i][0],complex((Ey[i][1]).real,(Ey[i][1]).imag)])
            Ey = sp.array(temp)
        elif bound_typ == "PerfH":
            temp = []
            for i in range(1,len(Ey)):
                temp.append([-Ey[-i][0], complex((Ey[-i][1]).real,(Ey[-i][1]).imag)])
            for i in range(0,len(Ey)):
                temp.append([Ey[i][0], complex((Ey[i][1]).real,(Ey[i][1]).imag)])
            Ey = sp.array(temp)

        file_input = open(work_directory+"E_field_"+bound_typ+"_displaced.fld", 'r+')
        data = file_input.readlines()
        Ezdisp1 = []
        for i in data[2:]:
            temp = map(float, i.rsplit())
            Ezdisp1.append([temp[2],complex(temp[-2],temp[-1])])

        ##Ey = sp.array(Ey)
        ##print Ezdisp

        if bound_typ == "PerfE":
            temp = []
            for i in range(1,len(Ezdisp1)):
                temp.append([-Ezdisp1[-i][0], complex(-(Ezdisp1[-i][1]).real,-(Ezdisp1[-i][1]).imag)])
            for i in range(0,len(Ezdisp1)):
                temp.append([Ezdisp1[i][0],complex((Ezdisp1[i][1]).real,(Ezdisp1[i][1]).imag)])
            Ezdisp1 = sp.array(temp)
        elif bound_typ == "PerfH":
            temp = []
            for i in range(1,len(Ezdisp1)):
                temp.append([-Ezdisp1[-i][0], complex((Ezdisp1[-i][1]).real,(Ezdisp1[-i][1]).imag)])
            for i in range(0,len(Ezdisp1)):
                temp.append([Ezdisp1[i][0], complex((Ezdisp1[i][1]).real,(Ezdisp1[i][1]).imag)])
            Ezdisp1 = sp.array(temp)


        file_input = open(work_directory+"H_field_"+bound_typ+"_displaced.fld", 'r+')
        data = file_input.readlines()
        Hx = []
        for i in data[2:]:
            temp = map(float, i.rsplit())
            Hx.append([temp[2],complex(temp[-6],temp[-5])])

        ##Bx = sp.array(Bx)
        ##print Bx

        if bound_typ == "PerfE":
            temp = []
            for i in range(1,len(Hx)):
                temp.append([-Hx[-i][0], complex((Hx[-i][1]).real,(Hx[-i][1]).imag)])
            for i in range(0,len(Hx)):
                temp.append([Hx[i][0],complex((Hx[i][1]).real,(Hx[i][1]).imag)])
            Hx = sp.array(temp)
        elif bound_typ == "PerfH":
            temp = []
            for i in range(1,len(Hx)):
                temp.append([-Hx[-i][0], complex(-(Hx[-i][1]).real,-(Hx[-i][1]).imag)])
            for i in range(0,len(Hx)):
                temp.append([Hx[i][0], complex((Hx[i][1]).real,(Hx[i][1]).imag)])
            Hx = sp.array(temp)

        file_input = open(work_directory+"q_1.csv", 'r+')
        data = file_input.readlines()
        Q = []
        for i in data[1:]:
            Q.append(map(float, i.rsplit(',')))

        Q = sp.array(Q)
        ##print Q

        file_input = open(work_directory+"stored_energy.csv", 'r+')
        data = file_input.readlines()
        stored_energy = []
        for i in data[1:]:
            stored_energy.append(map(float, i.rsplit(',')))

        ##print stored_energy

        file_input = open(work_directory+"freq.csv", 'r+')
        data = file_input.readlines()
        freq = []
        for i in data[1:]:
            temp = map(float, i.rsplit(','))
##            print temp, work_directory
            freq.append([temp[0],complex(temp[2],temp[1])])

        freq = sp.array(freq)

        gap = Ez[1,0]-Ez[0,0]
        confluence = []
        for i in range(0,len(Ez)):
            confluence.append(Ez[i,1]*complex(sp.cos(freq[0,1]*2*sp.pi*Ez[i,0]/C), sp.sin(freq[0,1]*2*sp.pi*Ez[i,0]/C)))


        ##print freq
        long_r_over_q = (potential**2/(2*sp.pi*freq[0,1]*stored_energy[0][1]))

        long_r_over_q_alt = abs(inte.simps(confluence, Ez[:,0], gap))**2/(2*2*sp.pi*freq[0,1]*stored_energy[0][1])
##        print Ezdisp1, work_directory
        gap = Ezdisp1[1,0]-Ezdisp1[0,0]
        confluence = []
        for i in range(0,len(Ezdisp1)):
            confluence.append(Ezdisp1[i,1]*complex(sp.cos(freq[0,1]*2*sp.pi*Ezdisp1[i,0]/C), sp.sin(freq[0,1]*2*sp.pi*Ezdisp1[i,0]/C)))

        trans_r_over_q_from_dEzdy1 = abs(inte.simps(confluence, Ezdisp1[:,0], gap))**2/(4*2*sp.pi*freq[0,1]*stored_energy[0][1]*(2*sp.pi*freq[0,1]*x01/C)**2)
        k_y_from_dEzdy1 = trans_r_over_q_from_dEzdy1*2*sp.pi*freq[0,1]/4


        gap = Ey[1,0]-Ey[0,0]
        confluence = []
##        print len(Hx), len(Ey), work_directory
        for i in range(0,len(Ey)):
            confluence.append(complex(Ey[i,1],Z0*Hx[i,1])*complex(sp.cos(freq[0,1]*2*sp.pi*Ey[i,0]/C), sp.sin(freq[0,1]*2*sp.pi*Ey[i,0]/C)))

        trans_r_over_q_from_Ey_Bx = abs(inte.simps(confluence, Ey[:,0], gap))**2/(2*sp.pi*freq[0,1]*stored_energy[0][1])
        k_y_from_Ey_Bx = trans_r_over_q_from_Ey_Bx*2*sp.pi*freq[0,1]/4

    ##    print "Frequency of mode is %f" % freq[0,1]
    ##    print "Q-factor of mode is %f" % Q[0,1]
    ##    print "Transverse kicker factor k_y from dEz/dy1 (V/nC/mm) = %f" % (k_y_from_dEzdy1/10**12)
    ##    print "Transverse R/Q from dEz/dy1 (Ohms/mm) = %f" % (trans_r_over_q_from_dEzdy1/10**3)
    ##    print "Transverse Rs1 = %f (Ohms/mm)" % (trans_r_over_q_from_dEzdy1*Q[0,1]/10**3)
    ##    ##print "Transverse kicker factor k_y from  dEz/dy2 (V/nC/mm) = %f" % (k_y_from_dEzdy2)
    ##    ##print "Transverse R/Q from dEz/dy2 (Ohms/mm) = %f" % (trans_r_over_q_from_dEzdy2)
    ##    ##print "Transverse  Rs2 = %f (Ohms/mm)" % (trans_r_over_q_from_dEzdy2*Q[0,1])
    ##    print "Transverse kicker factor k_y from Ey+cBx (V/nC/mm) = %f" % (k_y_from_Ey_Bx/10**12)
    ##    print "Transverse R/Q from Ey+cBx (Ohms/mm) = %f" % (trans_r_over_q_from_Ey_Bx/10**3)
    ##    print "Transverse Rs = %f (Ohms/mm)" % (trans_r_over_q_from_Ey_Bx*Q[0,1]/10**3)
    ##    print "Longitudinal R/Q = %f" % long_r_over_q
    ##    print "Longitudinal Rs = %f (Ohms)" % (long_r_over_q*Q[0,1])
    ##    print "Longitudinal R/Q alt= %f" % long_r_over_q_alt
    ##    print "Longitudinal Rs alt= %f (Ohms)" % (long_r_over_q_alt*Q[0,1])
    ##    print "Power loss is: %f (Watts)" % ((charge/t_bunch)**2*long_r_over_q_alt*Q[0,1]*sp.e**(-(2*sp.pi*freq[0,1]*sigma_z/C)**2))

        eigenmode_store.append([freq[0,1], Q[0,1], long_r_over_q_alt*Q[0,1], (charge/t_bunch)**2*long_r_over_q_alt*Q[0,1]*sp.e**(-(2*sp.pi*freq[0,1]*sigma_z/C)**2)])
        power_loss_tot += ((charge/t_bunch)**2*long_r_over_q_alt*Q[0,1]*sp.e**(-(2*sp.pi*freq[0,1]*sigma_z/C)**2)).real


freq_list = []
for i in range(0,20000,1):
        freq_list.append(float(i/10.0*10.0**6))

freq_list = list(set(freq_list))
freq_list.sort()

impedance_profile = []

for i in freq_list:
    total = 0.0
    for entry in eigenmode_store:
        total += Z_bb(i, entry)
    impedance_profile.append([total.real, total.imag])

impedance_profile = sp.array(impedance_profile)

##pl.semilogx()
pl.semilogy()
pl.plot(freq_list, impedance_profile[:,0], 'k-', label = "Real Impedance")
##pl.plot(freq_list, impedance_profile[:,1], 'r-', label = "Imaginary Impedance")
pl.xlabel("Frequency (Hz)")
pl.ylabel("Impedance ($\Omega$)")
pl.legend(loc = "upper right")
pl.grid(linestyle="--", which = "major")
##pl.axis([0,2*10**9, -20, 50])
pl.show()
pl.savefig(top_directory+"tex_output/long_imp.eps")
pl.savefig(top_directory+"tex_output/long_imp.pdf")
pl.clf()

print "Power loss assuming falling on a resonance using a guassian distribution = %f W" % power_loss_tot

n_bunches = 1404
p_bunch = 1.15*10**11
Q_part = 1.6*10**-19
f_rev = 2.0*10**7

I_b = p_bunch*Q_part*f_rev
power_tot = I_b**2#*n_bunches

directory_prof = "E:/PhD/1st_Year_09-10/Data/mki-heating/longitudinal-profile/"

prof_values = []
data=open(directory_prof+"long_bunch_power_spectrum",'r+')
linelist=data.readlines()
data.close()
for row in linelist[2:]:
    row=map(float,row.rsplit(','))
    prof_values.append(row)

prof_values=sp.array(prof_values)

x_val = prof_values[:,0]
y_val = prof_values[:,1]
lin_y = 10**(y_val/20)/(10**(y_val[3]/20))
lin_fit_imp = sp.polyfit(x_val,lin_y,80)

poly_coeffs_prof = sp.polyfit(x_val,y_val,70)
x_test_prof = sp.linspace(0,3,9000)
prof_fit = sp.polyval(poly_coeffs_prof, x_test_prof)
lin_prof_fit = sp.polyval(lin_fit_imp,x_test_prof)


##pl.plot(prof_values[:,0],prof_values[:],'r+')
##pl.plot(x_test_prof,lin_prof_fit,'b-')
##pl.show()
##pl.clf()
##
try:
    os.mkdir(top_directory+"tex_output/")

except:
    pass

target_file = "eigenmode_tex_output.tex"
eigenmode_output = open(top_directory+"tex_output/"+target_file, 'w+')

eigenmode_output.write("""\\begin{center}
                        \\begin{tabular}{| l | c | c | r |}
  \hline
  f (MHz) & Q & $R/Q_{long} $ & $R_{s} (\Omega)$\\\ \hline \n""")

for entry in eigenmode_store:
    eigenmode_output.write(""" %4.f & %3.1f & %.4f & %2.3f \\\ \hline \n""" % (entry[0]/10.0**6, entry[1], (entry[2]/entry[1]), entry[2]))


eigenmode_output.write("""\hline
                        \end{tabular}
                        \end{center}""")

eigenmode_output.close()

convolution_imp = []
count = f_rev
for i in range(0,2000000000,f_rev):
    total = 0.0
    for entry in eigenmode_store:
        total += Z_bb(i, entry)
    if i == 0:
        convolution_imp.append([i,0.0])
    else:
        convolution_imp.append([i,power_tot*2*sp.polyval(lin_fit_imp,i/10.0**9)*total.real])

##while count < 2*10**9:
##    total = 0.0
##    for entry in eigenmode_store:
##        total += Z_bb(count, entry)
##    if count == 0:
##        convolution_imp.append([count,0.0])
##    else:
##        convolution_imp.append([count,power_tot*2*sp.polyval(lin_fit_imp,count/10.0**9)*total.real])
##    count+=f_rev


convolution_imp=sp.array(convolution_imp)
total_imp=0
running_total = []

for i in range(0,len(convolution_imp)):
    total_imp += convolution_imp[i,1]
    running_total.append(total_imp)

print "Power loss using a measured spectrum (R. Steinhagan) = %f W" % total_imp

f_rev = C/circ

I_b = p_bunch*Q_part*f_rev
print I_b
power_tot = I_b**2*n_bunches/(2*sp.pi*f_rev**2)


spectra_directory = "E:/PhD/1st_Year_09-10/Data/Bunch_Spectra/"
spectra_list = [spectra_directory+i for i in os.listdir(spectra_directory)]
spectra_list.remove(spectra_directory+"tex_output")

count = 1
for file_ent in spectra_list:
    data_file = open(file_ent, 'r+')
    data = data_file.readlines()
    convolution_imp = []
    data_file.close()
    for i in range(0,len(data)/2):
        total = 0.0
        for entry in eigenmode_store:
            total += Z_bb(float(data[i]), entry)
        if i == 0:
            convolution_imp.append([float(data[i]),0.0])
        else:
            convolution_imp.append([float(data[i]),power_tot*2*(float(data[1])-float(data[0]))**2*10**(float(data[i+32000])/40)*total.real])
##                print float(data[i])
##        else:
##            pass

    convolution_imp=sp.array(convolution_imp)
    total_imp=0
    running_total = []
    for i in range(0,len(convolution_imp)):
        total_imp += float(convolution_imp[i,1])
        running_total.append(total_imp)

    ax1 = pl.subplot(111)
    pl.plot(data[:32000],data[32000:], 'b-')
    pl.xlabel('Frequency (Hz)')
    pl.ylabel("S(f) (dB)")
    if "BeforeRamp" in file_ent:
        text = "Before Ramp $P_{loss}$ = %f W" % total_imp
    elif "Ramp" in file_ent:
        text = "Ramp $P_{loss}$ = %f W" % total_imp
    elif "FlatTop" in file_ent:
        text = "Flat Top $P_{loss}$ = %f W" % total_imp
    elif "Squeeze" in file_ent:
        text = "Squeeze $P_{loss}$ = %f W" % total_imp
    elif "Adjust" in file_ent:
        text = "Adjust $P_{loss}$ = %f W" % total_imp
    elif "StableBeams" in file_ent:
        text = "Stable Beams $P_{loss}$ = %f W" % total_imp

    pl.axis([0, 3.5*10**9, -60, 10])
    pl.figtext(0.55,0.6, text, size=12.0, bbox={'facecolor':'white', 'pad':10})
    ax2 = pl.twinx()
    
    pl.plot(freq_list, impedance_profile[:,0], 'k-', label = "Real Impedance")
    pl.plot(freq_list, impedance_profile[:,1], 'r-', label = "Imaginary Impedance")
    pl.ylabel("Impedance ($\Omega$)")
    pl.legend(loc = "upper right")
    pl.axis([0, 3.5*10**9, -15, 20])
    idea =file_ent.replace(spectra_directory, '')
    idea = idea.rstrip('.csv')
    pl.savefig(top_directory+"tex_output/"+idea+'.png')
    pl.savefig(top_directory+"tex_output/"+idea+'.eps')
    pl.savefig(top_directory+"tex_output/"+idea+'.pdf')
    pl.clf()
    count+=1
    
    pl.plot(data[:32000], running_total, 'k-', label = "Cumulative Power Loss")
    pl.xlabel('Frequency (Hz)')
    pl.ylabel('Cumulative Power Loss (W)')
    pl.savefig(top_directory+"tex_output/"+idea+'_cumulative_power_loss.png')
    pl.savefig(top_directory+"tex_output/"+idea+'_cumulative_power_loss.eps')
    pl.savefig(top_directory+"tex_output/"+idea+'_cumulative_power_loss.pdf')
    pl.figtext(0.55,0.6, text, size=12.0, bbox={'facecolor':'white', 'pad':10})
    pl.clf()


    pl.plot(data[:32000], convolution_imp[:,1], 'k-', label = "Cumulative Power Loss")
    pl.xlabel('Frequency (Hz)')
    pl.ylabel('Power Loss (W)')
    pl.savefig(top_directory+"tex_output/"+idea+'_frequency_power_loss.png')
    pl.savefig(top_directory+"tex_output/"+idea+'_frequency_power_loss.eps')
    pl.savefig(top_directory+"tex_output/"+idea+'_frequency_power_loss.pdf')
    pl.figtext(0.55,0.6, text, size=12.0, bbox={'facecolor':'white', 'pad':10})
    pl.clf()

    lin_approx = []
    for i in range(32000, len(data)):
        lin_approx.append(10**(float(data[i])/40))
    pl.plot(data[:32000],lin_approx, 'b-', label="Meas. Phillipe, Themis")
    pl.plot(x_test_prof*10**9,lin_prof_fit,'k-', label="Meas. R. Steinhagen")
    pl.xlabel('Frequency (Hz)')
    pl.ylabel("S(f)")
    pl.legend(loc="upper right")
    pl.savefig(top_directory+"tex_output/"+idea+'_spectra_comp.png')
    pl.clf()
    

print "Time Taken for code to run = %f (s)" % (time.time()-start)




##pl.plot(convolution_imp[:,0], running_total)
##pl.show()
##pl.clf()
