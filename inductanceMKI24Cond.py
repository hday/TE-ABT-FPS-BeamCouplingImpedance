import scipy as sp
import pylab as pl
import os, sys

class ClosestDict(dict):
    def get(self, key):
        key=min(self.iterkeys(), key=lambda x:abs(x-key))
        return dict.get(self,key)
    
mu0 = 4*sp.pi*10.0**-7
muCopper = 1.0
muDoubleCopper = 1.0
condCopper = 1.0*10.0**7
rIn = 0.0005
rOut = 0.010
lenCable=0.0

directXToT = ClosestDict({0.0: 1.0, 0.5: 0.9998, 0.6: 0.9997, 0.7:0.9994, 0.8:0.9989,
              0.9:0.9983, 1.0:0.9974, 1.1:0.9962, 1.2:0.9946, 1.3:0.9927,
              1.4:0.9902, 1.5:0.9871, 1.6:0.9834, 1.7:0.9790, 1.8:0.9739,
              1.9:0.9680, 2.0:0.9611, 2.2:0.9448, 2.4:0.9248, 2.6:0.9013,
              2.8:0.8745, 3.0:0.8452, 3.2:0.8140, 3.4:0.7818, 3.6:0.7493,
              3.8:0.7173, 4.0:0.6863, 4.2:0.6568, 4.4:0.6289, 4.6:0.6028,
              4.8:0.5785, 5.0:0.556, 5.2:0.5351, 5.4:0.5157, 5.6:0.4976,
              5.8:0.4809, 6.0:0.4652, 6.2:0.4506, 6.4:0.4368, 6.6:0.4239,
              6.8:0.4117, 7.0:0.4002, 7.2:0.3893, 7.4:0.3790, 7.6:0.3692,
              7.8:0.3599, 8.0:0.3511, 8.2:0.3426, 8.4:0.3346,8.6:0.3269,
              8.8:0.3196, 9.0:0.3126, 9.2:0.3058, 9.4:0.2994, 9.6:0.2932,
              9.8:0.2873, 10.0:0.2816, 10.5:0.2682, 11.0:0.2562, 11.5:0.2452,
              12.0:0.2350, 12.5:0.2257, 13.0:0.2170, 13.5:0.2090, 14.0:0.2016,
              14.5:0.1947, 15.0:0.1882, 16.0:0.1765, 17.0:0.1661, 18.0:0.1589,
              19.0:0.1487, 20.0:0.1413, 21.0:0.1346, 22.0:0.1285, 23.0:0.1229,
              24.0:0.1178, 25.0:0.1131, 26.0:0.1087, 28.0:0.1010, 30.0:0.942,
              32.0:0.0884, 34.0:0.0832, 36.0:0.0785, 38.0:0.0744, 40.0:0.0707,
              42.0:0.0673, 44.0:0.0643, 46.0:0.0615, 48.0:0.0589, 50.0:0.0566,
              60.0:0.0471, 70.0:0.0404, 80.0:0.0354, 90.0:0.0314, 100.0:0.0283
              })


def skinDepth(freq, conductivity, mur, epsr):
    return (1/(conductivity*sp.pi*freq*mur*mu0))**0.5

def inductCoax(freq, rInner, rOuter, cond):
    return 2*10**(-4)*sp.log((2*rOuter+ skinDepth(freq, cond, 1, 1))/(2*rInner+ skinDepth(freq, cond, 1, 1)))

def inductInnerCond(freq, rInner, lenCoax, cond, muPrime, muDouble):
    return 2*lenCoax*(sp.log(2*lenCoax/rInner)-1+(muDouble/4)*directXToT.get(2*sp.pi*rInner*(2*muPrime*mu0*freq/cond)**0.5))

def inductOuterCond(rOuter, lenCoax):
    return 2*lenCoax*(sp.log(2*lenCoax/rOuter)-1)

def inductCoaxLine(freq, rInner, rOuter, cond, muPrime):
##    print sp.log(rOuter/rInner), directXToT.get(2*sp.pi*rInner*(2*muPrime*freq*cond)**0.5)
    return 2*(sp.log(rOuter/rInner) + 0.25*directXToT.get(2*sp.pi*rInner*(2*mu0*muPrime*freq*cond)**0.5))

def fileInductRead(targetFile):
    fileRead=open(targetFile, 'r+')

    listData = fileRead.readlines()
    fileRead.close()
    listCurrents = []
    listStoredEnergyElectric=[]
    listStoredEnergyMagnetic=[]
    for entry in listData:
        if entry.startswith("Total current (integral J ds)  "):
            tidbit=entry.replace("Total current (integral J ds)                    = ","")
            listCurrents.append(float(tidbit.replace(" [A]", "")))
        elif entry.startswith("Stored energy/unit length (integral A.J/2 ds)"):
            tidbit=entry.replace("Stored energy/unit length (integral A.J/2 ds)    = ", "")
            listStoredEnergyElectric.append(float(tidbit.replace(" [J/mm]", ""))*10.0**3)
        elif entry.startswith("Stored energy/unit length (integral B.H/2 ds) "):
            tidbit=entry.replace("Stored energy/unit length (integral B.H/2 ds)    = ", "")
            listStoredEnergyMagnetic.append(float(tidbit.replace(" [J/mm]", ""))*10.0**3)
        else:
            pass

    tempCurrents = []
    tempStoredEnergyElectric=[]
    tempStoredEnergyMagnetic=[]

    for i in range(0,len(listStoredEnergyMagnetic)-1,2):
        tempCurrents.append(listCurrents[i]+listCurrents[i+1])
        tempStoredEnergyElectric.append((listStoredEnergyElectric[i]**2+listStoredEnergyElectric[i+1]**2)**0.5)
        tempStoredEnergyMagnetic.append((listStoredEnergyMagnetic[i]**2+listStoredEnergyMagnetic[i+1]**2)**0.5)


    arrCurrents=sp.array(tempCurrents)
    arrStoredEnergyElectric=sp.array(tempStoredEnergyElectric)
    arrStoredEnergyMagnetic=sp.array(tempStoredEnergyMagnetic)
    listInductances = []
##    print arrStoredEnergyMagnetic

    #Calculate inductances using U=0.5 I^2 L U and I multiplied by 4 for quarter geometry

    geoFact=2.0
    for i in range(0,len(arrCurrents)):
        listInductances.append([arrStoredEnergyElectric[i]*geoFact*2/(geoFact*arrCurrents[i])**2,arrStoredEnergyMagnetic[i]*geoFact*2/(geoFact*arrCurrents[i])**2])

    arrInductances=sp.array(listInductances)

    return arrInductances

    

####### Data from Opera2D ########

####### Define Files ########

directTar = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/ferriteTorroidHeating/crossSection24ConductorsAndThatsAll/"
cenCondInductAll = [directTar+"centralCondCond"+str(i)+"All.lp" for i in range(3,15,1)]
cenCondInductCen = [directTar+"centralCondCond"+str(i)+"Cond1.lp" for i in range(3,15,1)]
cenCondInductOther = [directTar+"centralCondCond"+str(i)+"Cond"+str(i)+".lp" for i in range(3,15,1)]
cond3InductAll = [directTar+"centralCondCond"+str(i)+"All.lp" for i in range(4,15,1)]
cond3InductCond3 = [directTar+"centralCondCond"+str(i)+"Cond1.lp" for i in range(4,15,1)]
cond3InductOther = [directTar+"centralCondCond"+str(i)+"Cond"+str(i)+".lp" for i in range(4,15,1)]
cond4InductAll = [directTar+"centralCondCond"+str(i)+"All.lp" for i in range(5,15,1)]
cond4InductCond4 = [directTar+"centralCondCond"+str(i)+"Cond1.lp" for i in range(5,15,1)]
cond4InductOther = [directTar+"centralCondCond"+str(i)+"Cond"+str(i)+".lp" for i in range(5,15,1)]

####### Define total inductances for single components #######

totalInductSing = [directTar+"logScreenCondReg"+str(i)+".lp" for i in [1,3,4,5,6,7,8,9,10,11,12,13,14]]

####### Import Data into data arrays #######

totalInductSingDat = []
for entry in totalInductSing:
    totalInductSingDat.append(fileInductRead(entry))
totalInductSingDat=sp.array(totalInductSingDat)

cenCondInductAllDat=[]
for entry in cenCondInductAll:
    cenCondInductAllDat.append(fileInductRead(entry))
cenCondInductAllDat=sp.array(cenCondInductAllDat)

cenCondInductCenDat=[]
for entry in cenCondInductCen:
    cenCondInductCenDat.append(fileInductRead(entry))
cenCondInductCenDat=sp.array(cenCondInductCenDat)

cenCondInductOtherDat=[]
for entry in cenCondInductOther:
    cenCondInductOtherDat.append(fileInductRead(entry))
cenCondInductOtherDat=sp.array(cenCondInductOtherDat)

cond3InductAllDat=[]
for entry in cond3InductAll:
    cond3InductAllDat.append(fileInductRead(entry))
cond3InductAllDat=sp.array(cond3InductAllDat)

cond3InductCond3Dat=[]
for entry in cond3InductCond3:
    cond3InductCond3Dat.append(fileInductRead(entry))
cond3InductCond3Dat=sp.array(cond3InductCond3Dat)

cond3InductOtherDat=[]
for entry in cond3InductOther:
    cond3InductOtherDat.append(fileInductRead(entry))
cond3InductOtherDat=sp.array(cond3InductOtherDat)

cond4InductAllDat=[]
for entry in cond4InductAll:
    cond4InductAllDat.append(fileInductRead(entry))
cond4InductAllDat=sp.array(cond4InductAllDat)

cond4InductCond4Dat=[]
for entry in cond4InductCond4:
    cond4InductCond4Dat.append(fileInductRead(entry))
cond4InductCond4Dat=sp.array(cond4InductCond4Dat)

cond4InductOtherDat=[]
for entry in cond4InductOther:
    cond4InductOtherDat.append(fileInductRead(entry))
cond4InductOtherDat=sp.array(cond4InductOtherDat)


####### Define source files inductance screen conductors GND plate #######

directTar = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/ferriteTorroidHeating/crossSection24ConductorsAndGNDReturn/"
gndCondScreenCond = [directTar+"cond"+str(i)+"Cond39Cond"+str(i)+".lp" for i in range(3,15)]
gndCondScreenGND = [directTar+"cond"+str(i)+"Cond39Cond39.lp" for i in range(3,15)]
gndCenCondGND = directTar+"cond1Cond39Cond39.lp"
gndCenCondCen = directTar+"cond1Cond39Cond1.lp"
cenCondOnly = fileInductRead(directTar+"cond1CurAll.lp")
cenCondOnly1 = fileInductRead(directTar+"cond1CurCond1.lp")
cond3Cond4Cond3 = fileInductRead(directTar+"cond3Cond4Cond3.lp")
cond3Cond4Cond4 = fileInductRead(directTar+"cond3Cond4Cond4.lp")
cond3Cond5Cond3 = fileInductRead(directTar+"cond3Cond5Cond3.lp")
cond3Cond5Cond5 = fileInductRead(directTar+"cond3Cond5Cond5.lp")

gndScreenCondCondDat=[]
for entry in gndCondScreenCond:
    gndScreenCondCondDat.append(fileInductRead(entry))
gndScreenCondCondDat=sp.array(gndScreenCondCondDat)

gndCondScreenGNDDat=[]
for entry in gndCondScreenGND:
    gndCondScreenGNDDat.append(fileInductRead(entry))
gndCondScreenGNDDat=sp.array(gndCondScreenGNDDat)

####### Define source files inductance screen conductors with metalCylinder


####### Analytical Calculations ########


freqList = []
for i in range(1,10):
    for j in range(1, 9):
        freqList.append(i/10.0*10.0**j)

freqList=list(set(freqList))
freqList.sort()
freqList=sp.array(freqList)

selfInductInner = []
totalInductCoax = []
thing = []
for entry in freqList:
    thing.append([2*sp.pi*rIn*(2*muCopper*mu0*entry*condCopper)**0.5, directXToT.get(2*sp.pi*rIn*(2*muCopper*mu0*entry*condCopper)**0.5)])
    selfInductInner.append(inductInnerCond(entry, rIn, lenCable, condCopper, muCopper, muDoubleCopper))
    totalInductCoax.append(inductCoaxLine(entry, rIn, rOut, condCopper, muCopper))

selfInductOuter = inductOuterCond(rOut, lenCable)

##inductCoax =inductCoax(freqList, rIn, rOut, condCopper)
thing=sp.array(thing)
pl.semilogx()
##pl.loglog()


condListCen=[3,4,5,6,7,8,9,10,11,12,13,14]
print len(cenCondInductAllDat), len(condListCen)
##for i in range(0,len(cenCondInductAllDat)):
##    pl.plot(freqList, cenCondInductAllDat[i][:,1]*10**9, "+", label="Energy in all Cond "+str(condListCen[i])+"and Cen Cond")
##for i in range(0,len(cenCondInductCenDat)):
##    pl.plot(freqList, cenCondInductCenDat[i][:,1]*10**9, "+",label="Energy in cen cond Cond "+str(condListCen[i])+"Cen Cond")
##for i in range(0,len(cenCondInductOtherDat)):
##    pl.plot(freqList, cenCondInductOtherDat[i][:,1]*10**9, "+", label="Energy in Cond "+str(condListCen[i])+" Cond "+str(condListCen[i])+"Cen Cond")

condList3=[4,5,6,7,8,9,10,11,12,13,14]
print len(cond3InductOtherDat), len(condList3)
##for i in range(0,len(cond3InductAllDat)):
##    pl.plot(freqList, cond3InductAllDat[i][:,1]*10**9, 'x', label="Energy in all Cond "+str(condList3[i])+"Cond 3")
##for i in range(0,len(cond3InductCond3Dat)-1):
##    pl.plot(freqList, cond3InductCond3Dat[i][:,1]*10**9, 'x',label="Energy in cond 3 Cond "+str(condList3[i])+"Cond 3")
##for i in range(0,len(cond3InductOtherDat)-0):
##    pl.plot(freqList, cond3InductOtherDat[i][:,1]*10**9, 'x', label="Energy in Cond "+str(condList3[i])+" Cond "+str(condList3[i])+"Cond 3")

condList4=[5,6,7,8,9,10,11,12,13,14]
print len(cond4InductOtherDat), len(condList4)
##for i in range(0,len(cond4InductAllDat)):
##    pl.plot(freqList, cond4InductAllDat[i][:,1]*10**9, label="Energy in all Cond "+str(condList4[i])+"Cond 4")
##for i in range(0,len(cond4InductCond4Dat)-1):
##    pl.plot(freqList, cond4InductCond4Dat[i][:,1]*10**9, label="Energy in cond 4 Cond "+str(condList4[i])+"Cond 4")
##for i in range(0,len(cond4InductOtherDat)-0):
##    pl.plot(freqList, cond4InductOtherDat[i][:,1]*10**9, label="Energy in Cond "+str(condList4[i])+" Cond "+str(condList4[i])+"Cond 4")

##for i in range(0, len(totalInductSingDat)):
##    pl.plot(freqList, totalInductSingDat[i][:,1]*10**9, label="Current only in Cond"+str(i))
               
    
pl.legend(loc="lower left")
pl.xlabel("Frequency (Hz)")
pl.ylabel("Inductance (nH)")
##pl.axis([10**0,10**9,0,500])
##pl.show()
pl.clf()

######### Plotting with GND Busbar #########

freqList = []
for i in range(1,10):
    for j in range(1, 10):
        freqList.append(i/10.0*10.0**j)

freqList=list(set(freqList))
freqList.sort()
freqList=sp.array(freqList)

##pl.semilogx()
pl.loglog()


pl.plot(freqList,cond3Cond4Cond3[:,1]*10.0**9)
pl.plot(freqList,cond3Cond4Cond4[:,1]*10.0**9)
pl.plot(freqList,cond3Cond5Cond3[:,1]*10.0**9)
pl.plot(freqList,cond3Cond5Cond5[:,1]*10.0**9)
##pl.plot(freqList,cenCondOnly[:,1]*10.0**9)
##pl.plot(freqList,cenCondOnly1[:,1]*10.0**9)
condListGND=[3,4,5,6,7,8,9,10,11,12,13,14]
##for i in range(0, len(gndScreenCondCondDat)):
##    pl.plot(freqList, gndScreenCondCondDat[i][:,1]*10.0**9)
##for i in range(0, len(gndCondScreenGNDDat)):
##    pl.plot(freqList, gndCondScreenGNDDat[i][:,1]*10.0**9, label=str(condListGND[i]))
pl.legend(loc="lower left")
pl.xlabel("Frequency (Hz)")
pl.ylabel("Inductance (nH)")
##pl.axis([10**0,10**9,0,500])
##pl.show()
pl.clf()

######## Comparing screen conductor inductance with and without GND busbar

freqList1 = []
for i in range(1,10):
    for j in range(1, 9):
        freqList1.append(i/10.0*10.0**j)

freqList1=list(set(freqList1))
freqList1.sort()
freqList1=sp.array(freqList1)

ax1 = pl.subplot(111)
pl.semilogx()
condList3=[4,5,6,7,8,9,10,11,12,13,14]
print len(cond3InductOtherDat), len(condList3)
##for i in range(0,len(cond3InductAllDat)):
##    pl.plot(freqList1, cond3InductAllDat[i][:,1]*10**9, 'x', label="Energy in all Cond "+str(condList3[i])+"Cond 3")
for i in range(0,2):
    pl.plot(freqList1, cond3InductCond3Dat[i][:,1]*10**9, 'x',label="Energy in cond 3 Cond "+str(condList3[i])+"Cond 3")
for i in range(0,2):
    pl.plot(freqList1, cond3InductOtherDat[i][:,1]*10**9, 'x', label="Energy in Cond "+str(condList3[i])+" Cond "+str(condList3[i])+"Cond 3")
pl.plot(freqList,cond3Cond4Cond3[:,1]*10.0**9, label="Energy in Cond 3 ,cond3/4 GND")
pl.plot(freqList,cond3Cond4Cond4[:,1]*10.0**9, label="Energy in Cond 4 ,cond3/4 GND")
pl.plot(freqList,cond3Cond5Cond3[:,1]*10.0**9, label="Energy in Cond 3 ,cond3/5 GND")
pl.plot(freqList,cond3Cond5Cond5[:,1]*10.0**9, label="Energy in Cond 5 ,cond3/5 GND")
pl.legend(loc="lower left")
pl.xlabel("Frequency (Hz)")
pl.ylabel("Inductance (nH)")
pl.show()
pl.clf()
