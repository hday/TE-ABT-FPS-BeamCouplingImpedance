import scipy as sp
import pylab as pl
import time, os, sys
import scipy.integrate as inte

import sympy.mpmath as mpmath
import scipy, numpy, csv, time, os, sys
from matplotlib import pyplot
import numbers
import pylab as pl

Z0 = 377.0
a = 0.02725
b = 0.027
d = b+0.0415
length = 1
disp = 1
e0 = 8.85*10**-12
eprime = 12.0
rho = 10**6
muprime = 460.0
tau = 1.0/(20.0*10**6)
er=12.0
mu0 = 4*scipy.pi*10**-7

C = 299792458.0
circ=26659.0
n_bunches = 1368
possible_bunches = 1404
p_bunch = 1.15*10**11
Q_part = 1.6*10**-19
f_rev = 2*10**7
length = 2.88



I_b = p_bunch*Q_part*f_rev
power_tot = I_b**2*n_bunches/possible_bunches


def k(freq):
    return 2*scipy.pi*freq/(3*10**8)

def kxn(n):
    return ((2*n + 1)*scipy.pi)/(2*a)

def kyn(n,freq):
    return ((er*mur(freq) - 1)*k(freq)**2 - kxn(n)**2)**0.5

def sh(n):
    return mpmath.sinh(kxn(n)*b)

def ch(n):
    return mpmath.cosh(kxn(n)*b)

def tn(n,freq):
    return mpmath.tan(kyn(n,freq)*(b-d))

def ct(n,freq):
    return mpmath.cot(kyn(n,freq)*(b-d))

def FX(n,freq):
    return (kxn(n)/k(freq))*((1+(mur(freq)*er))*sh(n)*ch(n))

def FY(n,freq):
    return (kyn(n,freq)/k(freq))*(mur(freq)*(sh(n)**2)*tn(n,freq) - er*(ch(n)**2)*ct(n,freq))

def FY_2(n,freq):
    return (kyn(n,freq)/k(freq))*(mur(freq)*(ch(n)**2)*tn(n,freq) - er*(sh(n)**2)*ct(n,freq))

def ferrite_long(freq):
    return (1j*Z0)/(2*a) * (mpmath.nsum(lambda n: (((FX(n,freq) +FY(n,freq))/(er*mur(freq)-1)) - ((k(freq)*sh(n)*ch(n))/kxn(n)))**(-1), [0,10]))

for i in range(1,20,1):
    mod = i/10.0

    def mur(freq):
        return (1 + (muprime - 1j*freq*tau*muprime*mod)/(1+tau**2*freq**2))

    temp_real = []
    temp_imag = []


    target_directory = "E:/PhD/1st_Year_09-10/Software/mudoubleprime_scan/"
    try:
        os.mkdir(target_directory)
    except:
        pass
    try:
        os.mkdir(target_directory+"video/")
    except:
        pass

    bunch_length = 1.1*10**-9
    mark_length = 1.1*10**-9


    time_domain = []
    data_points = 10000.0
    sample_rate = 0.1/(mark_length/data_points)

## For 50ns spacing 22.726

    t=pl.r_[-22.726*mark_length:22.726*mark_length:1/sample_rate]

## For 25ns spacing
##    t=pl.r_[-11.357*mark_length:11.357*mark_length:1/sample_rate]
    ##print len(t)
    omega = sample_rate

    for i in t:
        if abs(i)<=bunch_length/2:
            time_domain.append(1-(2*i/bunch_length)**2)
        else:
            time_domain.append(0)
    time_domain = sp.array(time_domain)
    N=len(t)
    freq_domain=sp.fft(time_domain)
    f = sample_rate*sp.r_[0:(N/2)]/N
    n=len(f)

##    pl.plot(t,time_domain)
##    pl.axis([-1.5*10**-9, 1.5*10**-9, 0 , 1])
##    pl.show()
##    pl.clf()

    freq_domain = freq_domain[0:n]**2/freq_domain[0]**2
    convolution_imp = []
    total = 0.0
    name = mur(1.0)

    for i in range(0,len(f)):
        if f[i] < 2*10**9:
            if float(f[i]) == 0:
                convolution_imp.append([float(f[i]), 0])
            else:
                convolution_imp.append([f[i],power_tot*2*abs(freq_domain[i])*ferrite_long(float(f[i])).real])
                total+=power_tot*2*abs(freq_domain[i])*ferrite_long(float(f[i])).real
        else:
            pass

    convolution_imp = sp.array(convolution_imp)
    pl.plot(convolution_imp[:,0],convolution_imp[:,1], 'k.')
    pl.xlabel('Frequency (Hz)')
    pl.ylabel("Power loss (W/m)")
    pl.figtext(0.2,0.5, "$\mu '' = $"+str(name.imag/mu0)+ " Total power loss = "+str(total)+" W", size=12.0, bbox={'facecolor':'white', 'pad':10})
##    pl.savefig(target_directory+"mudoubleprime_"+str(mod)+'_mki-long.png')
    pl.axis([0,2*10**9,0,15])
    pl.savefig(target_directory+"video/"+str(int(mod*10))+'_power_loss.png')

##    pl.show()
    pl.clf()

