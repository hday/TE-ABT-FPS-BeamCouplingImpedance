import csv, os, sys
import scipy as sp
import pylab as pl


def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp


disp = 0.001
length = 2.88
f_rev = 3*10**8/27000.0
directory = "E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/full-device-impedances/" #Directory of data
without_metallisation = "without-metalisation/vertical-dipolar-impedance.csv"
screen_conductors_15 = "with-15-screen-conductors/vertical-dipolar-impedance.csv"
screen_conductors_24 = "with-24-screen-conductors/vertical-dipolar-impedance.csv"
screen_conductors_15_long_9_short = "15-long-9-short-screen/vertical-dipolar-impedance.csv"
screen_conductors_15_4hv = "15-and-4-hv-plate/vertical-dipolar-impedance.csv"
screen_conductors_15_5hv = "15-and-5-hv-plate/vertical-dipolar-impedance.csv"
screen_conductors_none = "no-conductors/vertical-dipolar-impedance.csv"
no_damping_ferrites = "no-damping-ferrites/vertical-dipolar-impedance.csv"
no_screen = "no-screen/vertical-dipolar-impedance.csv"
alt_screen_1 = "alt-screen-design/vertical-dipolar-impedance.csv"
alt_screen_embedded_cerr = "embedded-in-tube/vertical-dipolar-impedance.csv"
alt_screen_embedded_cerr_sec = "embedded-in-tube/vertical-dipolar-impedance-second.csv"
alt_screen_no_cerr = "embedded-tube-vacuum/vertical-dipolar-impedance.csv"
alt_screen_thick_cerr = "thick_ceramic_5.5mm/vertical-dipolar-impedance.csv"
measurements_24 = "E:/PhD/1st_Year_09-10/Data/mki-heating/measurements/24-strips-measurements.csv"
measurements_15 = "E:/PhD/1st_Year_09-10/Data/mki-heating/measurements/15-strips-measurements.csv"

measure_stash_path = "E:/PhD/1st_Year_09-10/Data/mki-heating/measurements/mki_measurements_mbarnes/"

##dip_BL_file = "vertical.csv"
##dip_kroyer_file = "vertical_mine.csv"


list_files = [measure_stash_path+i for i in os.listdir(measure_stash_path)]
updated_file_list = []
for i in range(0,len(list_files)):
    if '.csv' in list_files[i]:
        updated_file_list.append(list_files[i])
    else:
        pass


data_without_metalisation = sp.array(extract_dat(directory+without_metallisation))
data_screen_conductors_15 = sp.array(extract_dat(directory+screen_conductors_15))
data_screen_conductors_24 = sp.array(extract_dat(directory+screen_conductors_24))
data_screen_conductors_15_long_9_short = sp.array(extract_dat(directory+screen_conductors_15_long_9_short))
data_screen_conductors_15_4hv = sp.array(extract_dat(directory+screen_conductors_15_4hv))
data_screen_conductors_15_5hv = sp.array(extract_dat(directory+screen_conductors_15_5hv))
data_screen_conductors_none = sp.array(extract_dat(directory+screen_conductors_none))
data_no_damping_ferrites = sp.array(extract_dat(directory+no_damping_ferrites))
data_no_screen = sp.array(extract_dat(directory+no_screen))
data_alt_screen_1 = sp.array(extract_dat(directory+alt_screen_1))
data_alt_screen_embedded_cerr = sp.array(extract_dat(directory+alt_screen_embedded_cerr))
data_alt_screen_embedded_cerr_sec = sp.array(extract_dat(directory+alt_screen_embedded_cerr_sec))
data_alt_screen_no_cerr = sp.array(extract_dat(directory+alt_screen_no_cerr))
data_alt_screen_thick_cerr = sp.array(extract_dat(directory+alt_screen_thick_cerr))

strip_24_meas = []

input_file = open(measurements_24, 'r+')
tar = csv.reader(input_file, delimiter=",")

i=0
for row in tar:
    if i!=0:
        row = map(float, row)
        row[1] = float(row[1])*length 
##        n = float(row[0])*10**6/f_rev
##        row[1] = float(row[1])/0.003
##        row[3] = float(row[3])/0.003
        strip_24_meas.append(row)
    i+=1

input_file.close()
strip_24_meas = sp.array(strip_24_meas)

strip_15_meas = []

input_file = open(measurements_15, 'r+')
tar = csv.reader(input_file, delimiter=",")

i=0
for row in tar:
    if i!=0:
        row = map(float, row)
        row[0] = float(row[0])/10**6
        row[1] = float(row[1])*length 
##        row[1] = float(row[1])/0.003
##        row[3] = float(row[3])/0.003
        strip_15_meas.append(row)
    i+=1

input_file.close()
strip_15_meas = sp.array(strip_15_meas)


measurement_repository = []
for data_file in updated_file_list:
    input_file = open(data_file, 'r+')
    tar = input_file.readlines()
    temp = []

    for row in tar:
            row = row.split(',')
            row = map(float, row)
##            row[0] = float(row[0])/10**6
            row[1] = float(row[1])*length
            temp.append(row)


    input_file.close()
    measurement_repository.append(temp)



    
##
##
##for i in range(20,4000, 20):
##    pl.axvline(i, 0, 200)
##pl.loglog()
##pl.semilogy()
##pl.plot(data_without_metalisation[:,0], data_without_metalisation[:, 1], 'k-', label= "No Metalisation")
pl.plot(data_screen_conductors_15[:,0], data_screen_conductors_15[:, 1], 'b-', label= "15 Conductive Strips $\Re{}e(Z)$")
##pl.plot(data_screen_conductors_15[:,0], data_screen_conductors_15[:, 3], 'b--', label= "15 Conductive Strips $\Im{}m(Z)$")
pl.plot(data_screen_conductors_24[:,0], (data_screen_conductors_24[:, 1]), 'k-', label= "24 Conductive Strips $\Re{}e(Z)$")
##pl.plot(data_screen_conductors_24[:,0], (data_screen_conductors_24[:, 3]), 'r-', label= "24 Conductive Strips $\Im{}m(Z)$")
pl.plot(data_screen_conductors_15_long_9_short[:,0], data_screen_conductors_15_long_9_short[:, 1], 'r--', label= "15 long, 9 short")
pl.plot(data_screen_conductors_15_4hv[:,0], data_screen_conductors_15_4hv[:, 1], 'r-', label= "15 long, 4hv")
##pl.plot(data_screen_conductors_15_5hv[:,0], data_screen_conductors_15_5hv[:, 1], 'r--', label= "15 long, 5hv")
##pl.plot(data_screen_conductors_none[:,0], data_screen_conductors_none[:, 1], 'b-', label= "No Conductive Strips")
pl.plot(data_no_damping_ferrites[:,0], data_no_damping_ferrites[:, 1], 'b--', label= "No Damping Ferrites")
##pl.plot(data_no_screen[:,0], data_no_screen[:, 1], 'g-', label= "No Screen")
pl.plot(data_alt_screen_1[:,0], data_alt_screen_1[:, 1], 'g--', label= "Alt Screen 1")
##pl.plot(data_alt_screen_embedded_cerr[:,0], data_alt_screen_embedded_cerr[:, 1], 'r-', label= "Embedded in Ceramic $\Re{}e(Z)$")
pl.plot(data_alt_screen_embedded_cerr_sec[:,0], data_alt_screen_embedded_cerr_sec[:, 1], 'g-', label= "Embedded in Ceramic Sec $\Re{}e(Z)$")
##pl.plot(data_alt_screen_embedded_cerr_sec[:,0], abs(data_alt_screen_embedded_cerr_sec[:, 2]), 'r--', label= "Embedded in Ceramic Sec $\Im{}m(Z)$")
##pl.plot(data_alt_screen_no_cerr[:,0], data_alt_screen_no_cerr[:, 1], 'b-', label= "Embedded no Ceramic $\Re{}e(Z)$")
pl.plot(data_alt_screen_thick_cerr[:,0], data_alt_screen_thick_cerr[:, 1], 'm-', label= "Thick Ceramic (5.5mm) $\Re{}e(Z)$")

####pl.plot(strip_24_meas[:,0], strip_24_meas[:,1], 'k-', markersize=8.0,label = "Real Longitudinal Impedance - 24 strip meas")                    # plot fitted curve
####pl.plot(strip_15_meas[:,0], strip_15_meas[:,1], 'bx', markersize=8.0,label = "Real Longitudinal Impedance - 15 strip meas")                    # plot fitted curve
##
####pl.plot(theory[:,0], theory[:,1], 'g-', markersize=8.0,label = "Real Longitudinal Impedance - Theory")                    # plot fitted curve
####pl.plot(theory[:,0], theory[:,2], 'g--', markersize=8.0,label = "Imaginary Longitudinal Impedance - Theory")                    # plot fitted curve
##mark = 0
##des_file = measure_stash_path+'mki36'+'-impedance-results.csv'
##while updated_file_list[mark]!=des_file:
##    mark+=1
##
##measurement = sp.array(measurement_repository[mark])
##
##pl.plot(measurement[:,0], measurement[:,1], 'kx',markersize=12.0, label="Measurement 24 staggered conductors")


pl.grid(linestyle="--", which = "major")
pl.xlabel("Frequency (MHz)",fontsize = 16)                  #Label axes
pl.ylabel("Longitudinal Impedance $(\Omega)$",fontsize = 16)
##pl.title("", fontsize = 16)
pl.legend(loc="upper right")
pl.axis([0,0.3,-300,600])
##pl.savefig(directory+"test"+".pdf")                  # Save fig for film
pl.show()
pl.clf()   
