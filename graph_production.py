import csv
import scipy as sp
import pylab as pl


slac_length = 1.1
displacement_60 = 0.003
displacement_2 = 0.0005
f_rev = 3*10**8/27000.0
directory = "E:/PhD/1st_Year_09-10/Reports/IPAC_2011_papers/comparison-between-the-current-lhc-collimators-and-the-slac-phase-2-collimator-impedance/figures/" #Directory of data
graph_2mm = "phase_1_graph_2mm_dip_vert.csv"
graph_60mm = "phase_1_graph_60mm_dip_vert.csv"
glidcop_2mm = "phase_1_glidcop_2mm_dip_vert.csv"
glidcop_60mm = "phase_1_glidcop_60mm_dip_vert.csv"
moly_2mm = "phase_1_moly_2mm_dip_vert.csv"
moly_60mm = "phase_1_moly_60mm_dip_vert.csv"
slac_2mm = "2mm-vert-dip-impedance.csv"
slac_60mm = "60mm-vert-dip-impedance.csv"


data_graph_2mm = []

input_file = open(directory+graph_2mm, 'r+')
tar=input_file.readlines()


##i=0
for row in tar:
    row = map(float, row.rsplit(','))
##    row[1] = row[1]/slac_length
##    row[3] = row[3]/slac_length
    data_graph_2mm.append(row)


input_file.close()
data_graph_2mm = sp.array(data_graph_2mm)

data_graph_60mm = []

input_file = open(directory+graph_60mm, 'r+')
tar=input_file.readlines()


##i=0
for row in tar:
    row = map(float, row.rsplit(','))
##    row[1] = row[1]/slac_length
##    row[3] = row[3]/slac_length
    data_graph_60mm.append(row)


input_file.close()
data_graph_60mm = sp.array(data_graph_60mm)

data_glidcop_2mm = []

input_file = open(directory+glidcop_2mm, 'r+')
tar=input_file.readlines()


##i=0
for row in tar:
    row = map(float, row.rsplit(','))
##    row[1] = row[1]/slac_length
##    row[3] = row[3]/slac_length
    data_glidcop_2mm.append(row)


input_file.close()
data_glidcop_2mm = sp.array(data_glidcop_2mm)

data_glidcop_60mm = []

input_file = open(directory+glidcop_60mm, 'r+')
tar=input_file.readlines()


##i=0
for row in tar:
    row = map(float, row.rsplit(','))
##    row[1] = row[1]/slac_length
##    row[3] = row[3]/slac_length
    data_glidcop_60mm.append(row)


input_file.close()
data_glidcop_60mm = sp.array(data_glidcop_60mm)

data_moly_2mm = []

input_file = open(directory+moly_2mm, 'r+')
tar=input_file.readlines()


##i=0
for row in tar:
    row = map(float, row.rsplit(','))
##    row[1] = row[1]/slac_length
##    row[3] = row[3]/slac_length
    data_moly_2mm.append(row)


input_file.close()
data_moly_2mm = sp.array(data_moly_2mm)

data_moly_60mm = []

input_file = open(directory+moly_60mm, 'r+')
tar=input_file.readlines()


##i=0
for row in tar:
    row = map(float, row.rsplit(','))
##    row[1] = row[1]/slac_length
##    row[3] = row[3]/slac_length
    data_moly_60mm.append(row)


input_file.close()
data_moly_60mm = sp.array(data_moly_60mm)


data_slac_2mm = []

input_file = open(directory+slac_2mm, 'r+')
tar=input_file.readlines()


##i=0
for row in tar:
    row = map(float, row.rsplit(','))
    row[1] = -row[1]/displacement_2/slac_length
    row[3] = -row[3]/displacement_2/slac_length
    data_slac_2mm.append(row)


input_file.close()
data_slac_2mm = sp.array(data_slac_2mm)

data_slac_60mm = []

input_file = open(directory+slac_60mm, 'r+')
tar=input_file.readlines()


##i=0
for row in tar:
    row = map(float, row.rsplit(','))
    row[1] = -row[1]/displacement_60/slac_length
    row[3] = -row[3]/displacement_60/slac_length
    data_slac_60mm.append(row)


input_file.close()
data_slac_60mm = sp.array(data_slac_60mm)


pl.loglog()
pl.plot(data_slac_2mm[:,0], data_slac_2mm[:,1], 'k-', markersize=8.0,label = "SLAC - 2mm spacing")                    # plot fitted curve
pl.plot(data_slac_60mm[:,0], data_slac_60mm[:,1], 'k--', markersize=8.0,label = "SLAC - 60mm spacing")                    # plot fitted curve
pl.plot(data_graph_2mm[:,0]/10**9, data_graph_2mm[:,1], 'g-', markersize=8.0,label = "CERN Graphite - 2mm spacing")                    # plot fitted curve
pl.plot(data_graph_60mm[:,0]/10**9, data_graph_60mm[:,1], 'g--', markersize=8.0,label = "CERN Graphite - 60mm spacing")                    # plot fitted curve
pl.plot(data_glidcop_2mm[:,0]/10**9, data_glidcop_2mm[:,1], 'r-', markersize=8.0,label = "CERN Glidcop - 2mm spacing")                    # plot fitted curvepl.plot(data_slac_2mm[:,0]/10**9, data_slac_2mm[:,1], 'k-', markersize=8.0,label = "With Ferrite - $\Re{}e(Z)$")                    # plot fitted curve
pl.plot(data_glidcop_60mm[:,0]/10**9, data_glidcop_60mm[:,1], 'r--', markersize=8.0,label = "CERN Glidcop - 60mm spacing")                    # plot fitted curvepl.plot(data_slac_2mm[:,0]/10**9, data_slac_2mm[:,1], 'k-', markersize=8.0,label = "With Ferrite - $\Re{}e(Z)$")                    # plot fitted curve
pl.plot(data_moly_2mm[:,0]/10**9, data_moly_2mm[:,1], 'b-', markersize=8.0,label = "CERN Molybdenum - 2mm spacing")                    # plot fitted curve
pl.plot(data_moly_60mm[:,0]/10**9, data_moly_60mm[:,1], 'b--', markersize=8.0,label = "CERN Molybdenum - 60mm spacing")                    # plot fitted curve

pl.grid(linestyle="--", which = "major")
pl.xlabel("Frequency (GHz)",fontsize = 20)                  #Label axes
pl.ylabel("Impedance $(\Omega/m^{2})$",fontsize = 20)
##pl.title("", fontsize = 16)
pl.legend(loc="lower left")
##pl.axis([10**-1,2,10**2,10**7])
##pl.savefig(directory+"cross-section-hor-dip"+".pdf")                  # Save fig for film
pl.show()
pl.clf()   
