import csv, time, os, sys
import scipy as sp
import pylab as pl
import numpy as np
import scipy.optimize as op
import scipy.interpolate as interp
from scipy import signal

####### Define constants for calculations ##########

C = 299792458.0
c = 299792458.0
Z0 = 377
lenMKP = 3.423+(2*0.035)+(2*0.005)
Zc = 320.0
mu0=4*sp.pi*10.0**-7
eps0= 8.854187817*10.0**-12
dAper = 0.045
rWire = 0.0005
lWire = 3.551
lDUT = 2.45
resCopper = 17.24*10.0**-9
resOuter = 1/(3.3*10.0**7)
ZChar = 60*sp.log(dAper/rWire)

def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp

def eigenmodeGen(freqFile, qFile, rOverQFile):
    inputFreq=open(freqFile, "r+")
    datFreq=inputFreq.readlines()
    inputFreq.close()
    outputFreq=[]
    for line in datFreq[-20:-1]:
        temp=(line[25:].rstrip()).strip()
        outputFreq.append(float(temp))
    inputQ=open(qFile, "r+")
    datQ=inputQ.readlines()
    inputQ.close()
    outputQ=[]
    for line in datQ[-20:-1]:
        temp=(line[25:].rstrip()).strip()
        outputQ.append(float(temp))
    inputrOverQ=open(rOverQFile, "r+")
    datrOverQ=inputrOverQ.readlines()
    inputrOverQ.close()
    outputrOverQ=[]
    for line in datrOverQ[-20:-1]:
        temp=(line[25:].rstrip()).strip()
        outputrOverQ.append(float(temp))
    resDat=[]
    for i in range(0,len(outputFreq)):
        resDat.append([outputFreq[i], outputQ[i], outputrOverQ[i]])

    return resDat

def Z_bb(freq, data):
    return data[2]*data[1]/complex(1, data[1]*(freq/data[0] - data[0]/freq))

def gaussProf(freq, bLength):
    sigma = bLength/(4.0*2.0*sp.log(2.0))
    return sp.e**(-(2.0*sp.pi*freq*sigma)**2*(2**0.5))

def cosProf(freq, bunLength):
    return (sp.sin(sp.pi*freq*bunLength)/(sp.pi*freq*bunLength*(1-(freq*bunLength)**2)))**2


def readS21FromVNA(inputFile):
    tar=open(inputFile, "r+")
    inputData=tar.readlines()
    tar.close()
    outputData=[]
    for line in inputData[6:]:
        temp=line.split("\t")
        outputData.append(map(float,temp))
    return outputData

def logToLin(data):
    return 10**(data/20)

def linToLog(data):
    return 20*sp.log10(data)

def lumpImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return 2*Zc*(1-dataNorm)/dataNorm

def logImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return -2*Zc*sp.log(dataNorm)

def skinDepth(freq, cond):
    return (1/(sp.pi*freq*mu0*cond))**0.5

def fileImport(tarFile):
    readPart=open(tarFile, 'r+')
    datUnPro = readPart.readlines()
    readPart.close()
    freqDat=[]
    qLoaded=[]
    s21DB=[]
    for row in datUnPro[20:]:
        freqDat.append(float(row.split(",")[0]))
        qLoaded.append(float(row.split(",")[1]))
        s21DB.append(float(row.split(",")[2]))

    return sp.array(freqDat), sp.array(qLoaded), sp.array(s21DB)

def resImpGet(tarArr1,tarArr2,tarArr3):

    freqList, qTotal, transCoeff = tarArr1, tarArr2, tarArr3

    linTransCoeff = 10.0**(transCoeff/10.0)
    coupCoeff = linTransCoeff/(1-linTransCoeff)
    qUnloaded=qTotal*(1+coupCoeff)

    measAttenuation = 8.686*sp.pi*freqList/(C*qUnloaded)
    skinD = skinDepth(freqList, 1/resCopper)
    skinCor = (rWire+skinD)/rWire

    attenCable = 0
    attenCopper = skinCor*8.686*(sp.pi*eps0*resCopper*freqList)**0.5/(sp.log(dAper/rWire))*((1/rWire)+(1/dAper)*(resCopper/resOuter)**0.5)
    resLow = 4*lWire*resCopper/(sp.pi*rWire**2)
    resHigh = resLow*rWire/(4*skinD)*skinCor

    skinPipe = skinDepth(freqList, resOuter)
    zMeas = resHigh*(measAttenuation-attenCopper-attenCable)/(attenCopper)

    return freqList, zMeas/lDUT

def resImpGetFromFile(targetFile):

    freqList, qTotal, transCoeff = fileImport(targetFile)

    linTransCoeff = 10.0**(transCoeff/10.0)
    coupCoeff = linTransCoeff/(1-linTransCoeff)
    qUnloaded=qTotal*(1+coupCoeff)

    measAttenuation = 8.686*sp.pi*freqList/(c*qUnloaded)
    skinD = skinDepth(freqList, 1/resCopper)
    skinCor = (rWire+skinD)/rWire

    attenCopper = skinCor*8.686*(sp.pi*eps0*resCopper*freqList)**0.5/(sp.log(dAper/rWire))*((1/rWire)+(1/dAper)*(resCopper/resOuter)**0.5)
    resLow = 4*lWire*resCopper/(sp.pi*rWire**2)
    resHigh = resLow*rWire/(4*skinD)*skinCor

    skinPipe = skinDepth(freqList, resOuter)
    zMeas = resHigh*(measAttenuation-attenCopper)/attenCopper

    return freqList, zMeas/lDUT

targetDirProbeMeas = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/testForResMethod/oldtank10/"
transmissionData=[]
temp=[]
listOfFiles = ["RESONATOR2LONG1SHORT"+str(i)+"-"+str(i+200)+"MHZ.S2P" for i in range(0,2000,200)]
for inputFile in listOfFiles:
    tar=open(targetDirProbeMeas+inputFile)
    inputData=tar.readlines()
    for line in inputData[6:]:
        bit=line.split("\t")
        temp.append(map(float,bit))

##targetDirProbeMeas = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/06-02-14/mki-Tank10-cr2/"
##transmissionData=[]
##temp=[]
##listOfFiles = ["HIGHFIDELITY"+str(i)+"-"+str(i+200)+"MHZ.S2P" for i in range(0,2000,200)]
##for inputFile in listOfFiles:
##    tar=open(targetDirProbeMeas+inputFile)
##    inputData=tar.readlines()
##    for line in inputData[6:]:
##        bit=line.split("\t")
##        temp.append(map(float,bit))


#### For S2P ######
                  
transmissionData=sp.array(temp)
widths=sp.arange(10,30)
peakList=signal.find_peaks_cwt(transmissionData[:,3],widths)

fitFunc = lambda p, x: (p[0]/(1+complex(0,1)*p[1]*(x/p[2] - p[2]/x))).real
errFunc = lambda p, x, y, err: (y-fitFunc(p, x))

fitNo = 5
testList=sp.linspace(1,10,10)

peakDataStash=[]
for peak in peakList:
    pInit = [logToLin(transmissionData[peak,3]),100,transmissionData[peak,0]/10**9]
    freqVal = transmissionData[peak-fitNo:peak+fitNo,0]/10**9
    yErr = logToLin(transmissionData[peak-fitNo:peak+fitNo,3])
    temp = yErr
    out=op.leastsq(errFunc, pInit, args=(freqVal,temp,yErr), full_output=1)
    pFinal = out[0]
##    print pFinal
    peakDataStash.append([logToLin(transmissionData[peak,3]), pFinal[1], transmissionData[peak,0]/10**9])


peakDataStash=sp.array(peakDataStash)
print peakDataStash

##### Check peaks in peakDataStash are valid ########

check=0
while check<len(peakDataStash):
    if peakDataStash[check,1]<100:
        peakDataStash=sp.vstack([peakDataStash[:check],peakDataStash[check+1:]])
    else:
        check+=1

print len(peakDataStash)

##### Plotting Measurements ########


##pl.semilogy()
pl.plot(transmissionData[:,0]/10**9, (transmissionData[:,3]), 'k-')
for i in peakList:
    pl.plot(transmissionData[i,0]/10**9, (transmissionData[i,3]), 'rx')
for i in range(0,len(peakDataStash)):
    plotWidth=sp.linspace((peakDataStash[i,2]-0.05),(peakDataStash[i,2]+0.05),500)
    plotValues = fitFunc(peakDataStash[i,:],plotWidth)
    
    pl.plot(plotWidth, linToLog(plotValues), 'b-')
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("S$_{21}$ (dB)")
pl.ylim(-90,-30)
pl.xlim(0,2)
##pl.show()
pl.clf()

pl.plot(transmissionData[:,0]/10**9, transmissionData[:,2])
##for i in peakList:
##    pl.plot(transmissionData[i,0]/10**9, transmissionData[i,1], 'ro')
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("S$_{11}$ (dB)")
##pl.show()
pl.clf()


pl.plot(transmissionData[:,0]/10**9, transmissionData[:,5])


pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("S$_{22}$ (dB)")
##pl.show()
pl.clf()


pl.plot(peakDataStash[:,0], peakDataStash[:,1], 'k-')
##pl.show()
pl.clf()

##
##pl.plot(peakDataStash[:,0], peakDataStash[:,2], 'k-')
##pl.show()
##pl.clf()

testPieceFreq, testPieceImp=resImpGet(peakDataStash[:,2]*10**9,peakDataStash[:,1], linToLog(peakDataStash[:,0]))
print len(testPieceImp), len(peakList)
pl.plot(testPieceFreq/10**9, testPieceImp, 'kx')
##pl.axis([0,2,0,50])
##pl.show()
pl.clf()


###### For CSV ######

targetDirProbeMeas = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/testForResMethod/oldtank10/"
transmissionData=[]
temp=[]
listOfFiles = ["RESONATOR2LONGONESHORT"+str(i)+"MHZTO"+str(i+200)+"MHZ.CSV" for i in range(0,2000,200)]
for inputFile in listOfFiles:
    tar=open(targetDirProbeMeas+inputFile)
    inputData=tar.readlines()
    for line in inputData[4:]:
        bit=line.split(",")
        temp.append(map(float,bit))
                  
transmissionData=sp.array(temp)
widths=sp.arange(10,30)
peakList=signal.find_peaks_cwt(transmissionData[:,1],widths)

fitFunc = lambda p, x: (p[0]/(1+complex(0,1)*p[1]*(x/p[2] - p[2]/x))).real
errFunc = lambda p, x, y, err: (y-fitFunc(p, x))

fitNo = 5
testList=sp.linspace(1,10,10)

peakDataStash=[]
for peak in peakList:
    pInit = [logToLin(transmissionData[peak,1]),100,transmissionData[peak,0]/10**9]
    freqVal = transmissionData[peak-fitNo:peak+fitNo,0]/10**9
    yErr = logToLin(transmissionData[peak-fitNo:peak+fitNo,1])
    temp = yErr
    out=op.leastsq(errFunc, pInit, args=(freqVal,temp,yErr), full_output=1)
    pFinal = out[0]
##    print pFinal
    peakDataStash.append([logToLin(transmissionData[peak,1]), pFinal[1], transmissionData[peak,0]/10**9])

peakDataStash=sp.array(peakDataStash)

##### Check peaks in peakDataStash are valid ########

check=0
while check<len(peakDataStash):
    if peakDataStash[check,1]<100:
        peakDataStash=sp.vstack([peakDataStash[:check],peakDataStash[check+1:]])
    else:
        check+=1

print len(peakDataStash)

##### Plotting Measurements ########


##pl.semilogy()
pl.plot(transmissionData[:,0]/10**9, (transmissionData[:,1]), 'k-')
for i in peakList:
    pl.plot(transmissionData[i,0]/10**9, (transmissionData[i,1]), 'rx')
for i in range(0,len(peakDataStash)):
    plotWidth=sp.linspace((peakDataStash[i,2]-0.05),(peakDataStash[i,2]+0.05),500)
    plotValues = fitFunc(peakDataStash[i,:],plotWidth)
    
    pl.plot(plotWidth, linToLog(plotValues), 'b-')
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("S$_{21}$ (dB)")
pl.ylim(-90,-30)
pl.xlim(0,2)
##pl.show()
pl.clf()


pl.plot(peakDataStash[:,0], peakDataStash[:,1], 'k-')
##pl.show()
pl.clf()

##
##pl.plot(peakDataStash[:,0], peakDataStash[:,2], 'k-')
##pl.show()
##pl.clf()

measurements_15 = sp.array(extract_dat("E:/PhD/1st_Year_09-10/Data/mki-heating/measurements/15-strips-measurements.csv"))

testPieceFreqOther, testPieceImpOther=resImpGet(peakDataStash[:,2]*10**9,peakDataStash[:,1], linToLog(peakDataStash[:,0]))
print len(testPieceImp), len(peakList)
pl.plot(measurements_15[:,0]/10.0**9, measurements_15[:,1], 'bx',markersize=16.0,label="15 Screen Conductors - Measured")
pl.plot(testPieceFreq/10**9, testPieceImp, 'kx')
pl.plot(testPieceFreqOther/10**9, testPieceImpOther, 'bx')
##pl.axis([0,2,0,50])
##pl.show()
pl.clf()


######## Test with MKQH ##########
######## Short Cables ########

targetDirProbeMeas = "E:/PhD/1st_Year_09-10/Data/MKQH/13-02-14/mkqhSPS/"
transmissionDataSmall=[]
temp=[]
listOfFiles = ["RESSMALL"+str(i)+"TO"+str(i+200)+"MHZ.S2P" for i in range(0,2000,200)]
for inputFile in listOfFiles:
    tar=open(targetDirProbeMeas+inputFile)
    inputData=tar.readlines()
    for line in inputData[6:]:
        bit=line.split("\t")
        temp.append(map(float,bit))


#### For S2P ######
                  
transmissionDataMKQHSmall=sp.array(temp)
widths=sp.arange(60,200)
peakListMKQHSmall=signal.find_peaks_cwt(transmissionDataMKQHSmall[:,3],widths)

fitFunc = lambda p, x: (p[0]/(1+complex(0,1)*p[1]*(x/p[2] - p[2]/x))).real
errFunc = lambda p, x, y, err: (y-fitFunc(p, x))

fitNo = 40
testList=sp.linspace(1,10,10)

peakDataStashMKQHSmall=[]
for peak in peakList:
    pInit = [logToLin(transmissionDataMKQHSmall[peak,3]),60,transmissionDataMKQHSmall[peak,0]/10**9]
    freqVal = transmissionDataMKQHSmall[peak-fitNo:peak+fitNo,0]/10**9
    yErr = logToLin(transmissionDataMKQHSmall[peak-fitNo:peak+fitNo,3])
    temp = yErr
    out=op.leastsq(errFunc, pInit, args=(freqVal,temp,yErr), full_output=1)
    pFinal = out[0]
##    print pFinal
    peakDataStashMKQHSmall.append([logToLin(transmissionDataMKQHSmall[peak,3]), pFinal[1], transmissionDataMKQHSmall[peak,0]/10**9])


peakDataStashMKQHSmall=sp.array(peakDataStashMKQHSmall)

##### Check peaks in peakDataStash are valid ########

check=0
otherPile=sp.array([])
while check<len(peakDataStashMKQHSmall):
##    if peakDataStashMKQHSmall[check,1]>100 and peakDataStashMKQHSmall[check,1]<1000:
    for i in peakList:
        if abs(peakDataStashMKQHSmall[check,2]-transmissionDataMKQHSmall[peak,0])<1*10**6:
            peakDataStashMKQHSmall=sp.vstack([peakDataStashMKQHSmall[:check],peakDataStashMKQHSmall[check+1:]])
    else:
        check+=1

######## Long Cables ########

targetDirProbeMeas = "E:/PhD/1st_Year_09-10/Data/MKQH/13-02-14/mkqhSPS/"
transmissionDataLong=[]
temp=[]
listOfFiles = ["RESLONG"+str(i)+"TO"+str(i+200)+"MHZ.S2P" for i in range(0,2000,200)]
for inputFile in listOfFiles:
    tar=open(targetDirProbeMeas+inputFile)
    inputData=tar.readlines()
    for line in inputData[6:]:
        bit=line.split("\t")
        temp.append(map(float,bit))


#### For S2P ######

                  
transmissionDataMKQHLong=sp.array(temp)
widths=sp.arange(30,100)
peakListMKQHLong=signal.find_peaks_cwt(transmissionDataMKQHLong[:,3],widths)

fitFunc = lambda p, x: (p[0]/(1+complex(0,1)*p[1]*(x/p[2] - p[2]/x))).real
errFunc = lambda p, x, y, err: (y-fitFunc(p, x))

fitNo = 5
testList=sp.linspace(1,10,10)

peakDataStashMKQHLong=[]
for peak in peakList:
    pInit = [logToLin(transmissionDataMKQHLong[peak,3]),100,transmissionDataMKQHLong[peak,0]/10**9]
    freqVal = transmissionDataMKQHLong[peak-fitNo:peak+fitNo,0]/10**9
    yErr = logToLin(transmissionDataMKQHLong[peak-fitNo:peak+fitNo,3])
    temp = yErr
    out=op.leastsq(errFunc, pInit, args=(freqVal,temp,yErr), full_output=1)
    pFinal = out[0]
##    print pFinal
    peakDataStashMKQHLong.append([logToLin(transmissionDataMKQHLong[peak,3]), pFinal[1], transmissionDataMKQHLong[peak,0]/10**9])


peakDataStashMKQHLong=sp.array(peakDataStashMKQHLong)

splineFitMKQH=interp.InterpolatedUnivariateSpline(transmissionDataMKQHLong[:,0],transmissionDataMKQHLong[:,3])
splineFitFreq = sp.linspace(0.001,2,2000)
splineFitData=splineFitMKQH(splineFitFreq*10**9)

##### Check peaks in peakDataStash are valid ########

check=0
while check<len(peakDataStashMKQHLong):
    if peakDataStashMKQHLong[check,1]>100:
        peakDataStashMKQHLong=sp.vstack([peakDataStashMKQHLong[:check],peakDataStashMKQHLong[check+1:]])
    else:
        check+=1


pl.plot(transmissionDataMKQHSmall[:,0]/10**9, (transmissionDataMKQHSmall[:,3]), 'k-')
##pl.plot(transmissionDataMKQHLong[:,0]/10**9, (transmissionDataMKQHLong[:,3]), 'b-')
for i in peakListMKQHSmall:
    pl.plot(transmissionDataMKQHSmall[i,0]/10**9, (transmissionDataMKQHSmall[i,3]), 'kx')
for i in range(0,len(peakDataStashMKQHSmall)):
    plotWidth=sp.linspace((peakDataStashMKQHSmall[i,2]-0.05),(peakDataStashMKQHSmall[i,2]+0.05),500)
    plotValues = fitFunc(peakDataStashMKQHSmall[i,:],plotWidth)
    
    pl.plot(plotWidth, linToLog(plotValues), 'k--')
##
##for i in peakListMKQHLong:
##    pl.plot(transmissionDataMKQHLong[i,0]/10**9, (transmissionDataMKQHLong[i,3]), 'bx')
##for i in range(0,len(peakDataStashMKQHLong)):
##    plotWidth=sp.linspace((peakDataStashMKQHLong[i,2]-0.05),(peakDataStashMKQHLong[i,2]+0.05),500)
##    plotValues = fitFunc(peakDataStashMKQHLong[i,:],plotWidth)
##    
##    pl.plot(plotWidth, linToLog(plotValues), 'b--')

##pl.plot(splineFitFreq, splineFitData)
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("S$_{21}$ (dB)")
##pl.ylim(-90,-30)
pl.xlim(0,2)
pl.show()
pl.clf()


measMKQHVNAfreq, measMKQHVNAimp = resImpGetFromFile("E:/PhD/1st_Year_09-10/Data/MKQH/13-02-14/mkqhSPS/resonatorSmall")
freqShortCable, impShortCable=resImpGet(peakDataStashMKQHSmall[:,2]*10**9,peakDataStashMKQHSmall[:,1], linToLog(peakDataStashMKQHSmall[:,0]))
freqLongCable, impLongCable=resImpGet(peakDataStashMKQHLong[:,2]*10**9,peakDataStashMKQHLong[:,1], linToLog(peakDataStashMKQHLong[:,0]))

pl.plot(measMKQHVNAfreq/10**9, measMKQHVNAimp, "bx")
pl.plot(freqShortCable/10**9, impShortCable, "kx")
##pl.plot(freqLongCable/10**9, impLongCable)
##pl.show()
pl.clf()
