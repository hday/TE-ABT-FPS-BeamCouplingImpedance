import sympy.mpmath as mpmath
import scipy, numpy, csv, time
from matplotlib import pyplot
import numbers
import pylab as pl
import scipy.special as spec

Z0 = 377.0
a = 1       # beam radius
b = 0.005   # pipe radius
d = 0.015   # pipe outer radius
e0 = 8.85*10**-12
gamma = 1.05
beta = 0.3
eprime = 1.0
rho = 10.0**5
muprime = 1.0
tau_m = 1.0/(20.0*10**6)
tau_e = 8.0*10**-13
eprime= 1.0
mu0 = 4*scipy.pi*10**-7
mur = 1.0



def k(freq):
    return 2*scipy.pi*freq/(beta*3*10**8)

##def rho(freq):
##    return rho_dc/(complex(1,2*scipy.pi*freq*tau_e))

def er(freq):
    return eprime*complex(1,-rho/(2*scipy.pi*freq*e0))

##def mur(freq):
##    return (1 + (muprime/(1+1j*tau*freq)))

def nu(freq):
    return k(freq)*(1-beta**2*er(freq)*mur)**0.5

def s(freq):
    return k(freq)*a/gamma

def x_1(freq):
    return k(freq)*b/gamma

def x_2(freq):
    return nu(freq)*b

def y(freq):
    return nu(freq)*d

def alpha_2(freq):
    return spec.kv(1,y(freq))*spec.iv(1,x_2(freq))/(spec.iv(1,y(freq))*spec.kv(1,x_2(freq)))# Determined by boundary conditions

def eta_2(freq):
    return spec.kvp(1,y(freq))*spec.iv(1,x_2(freq))/(spec.ivp(1,y(freq))*spec.kv(1,x_2(freq)))

def P_1(freq):
    return spec.ivp(1,x_1(freq))/spec.iv(1,x_1(freq))

def Q_1(freq):
    return spec.kvp(1,x_1(freq))/spec.kv(1,x_1(freq))

def P_2(freq):
    return spec.ivp(1,x_2(freq))/spec.iv(1,x_2(freq))

def Q_2(freq):
    return spec.kvp(1,x_2(freq))/spec.kv(1,x_2(freq))

def Q_eta(freq):
    return (Q_2(freq) - eta_2(freq)*P_2(freq))/(1-eta_2(freq))

def Q_alpha(freq):
    return (Q_2(freq) - alpha_2(freq)*P_2(freq))/(1-alpha_2(freq))


def re_wall_dip(freq):
    return (complex(0,1) * Z0 * spec.iv(1,s(freq))**2*spec.kv(1,x_1(freq)))/ \
           (scipy.pi * a**2 * beta * gamma**2 * spec.iv(1,x_1(freq)))* \
           (gamma*nu(freq)*(P_1(freq)-Q_1(freq))*(beta*x_1(freq)*x_2(freq))**2 \
            *(gamma*nu(freq)*P_1(freq)-k(freq)*mur*Q_eta(freq))) / \
            ((gamma*nu(freq)*x_2(freq)-k(freq)*x_1(freq))**2 - \
            (beta*x_1(freq)*x_2(freq))**2*(gamma*nu(freq)*P_1(freq)-k(freq)*\
            mur*Q_eta(freq))*(gamma*nu(freq)*P_1(freq) - \
            k(freq)*er(freq)*Q_alpha(freq)))


start = time.time()

x= []
temp_real = []
temp_imag = []

freq_list = []
for i in range(0,8,1):
    for j in range(1,10,1):
        freq_list.append(float(j*10.0**i))

for i in freq_list:
    temp_real.append(re_wall_dip(i).real)
    temp_imag.append(re_wall_dip(i).imag)

output = open('coll-circ-dip_data_graph.csv', 'w+')
for i in range(0,len(temp_real)):
              output.write(str(freq_list[i])+','+str(temp_real[i])+','+str(temp_imag[i])+'\n')

output.close()

pl.loglog(freq_list, temp_real, 'k-', label = "Real Longitudinal")
pl.loglog(freq_list, temp_imag, 'r-', label = "Imaginary Longitudinal")


pl.xlabel("Frequency (Hz)", fontsize=16)                  #Label axes
pl.ylabel("Impedance ($\Omega/m^{2}$)", fontsize=16)
pl.legend(loc="lower left")
pl.show()
pl.savefig('coll_dip_circ_data_graph.pdf')
pl.clf()



print time.time() - start
