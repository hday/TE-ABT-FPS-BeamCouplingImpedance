import scipy as sp
import pylab as pl
import os, sys

class ClosestDict(dict):
    def get(self, key):
        key=min(self.iterkeys(), key=lambda x:abs(x-key))
        return dict.get(self,key)
    
mu0 = 4*sp.pi*10.0**-7
muCopper = 1.0
muDoubleCopper = 1.0
condCopper = 1.0*10.0**7
rIn = 0.0005
rOut = 0.010
lenCable=0.0

directXToT = ClosestDict({0.0: 1.0, 0.5: 0.9998, 0.6: 0.9997, 0.7:0.9994, 0.8:0.9989,
              0.9:0.9983, 1.0:0.9974, 1.1:0.9962, 1.2:0.9946, 1.3:0.9927,
              1.4:0.9902, 1.5:0.9871, 1.6:0.9834, 1.7:0.9790, 1.8:0.9739,
              1.9:0.9680, 2.0:0.9611, 2.2:0.9448, 2.4:0.9248, 2.6:0.9013,
              2.8:0.8745, 3.0:0.8452, 3.2:0.8140, 3.4:0.7818, 3.6:0.7493,
              3.8:0.7173, 4.0:0.6863, 4.2:0.6568, 4.4:0.6289, 4.6:0.6028,
              4.8:0.5785, 5.0:0.556, 5.2:0.5351, 5.4:0.5157, 5.6:0.4976,
              5.8:0.4809, 6.0:0.4652, 6.2:0.4506, 6.4:0.4368, 6.6:0.4239,
              6.8:0.4117, 7.0:0.4002, 7.2:0.3893, 7.4:0.3790, 7.6:0.3692,
              7.8:0.3599, 8.0:0.3511, 8.2:0.3426, 8.4:0.3346,8.6:0.3269,
              8.8:0.3196, 9.0:0.3126, 9.2:0.3058, 9.4:0.2994, 9.6:0.2932,
              9.8:0.2873, 10.0:0.2816, 10.5:0.2682, 11.0:0.2562, 11.5:0.2452,
              12.0:0.2350, 12.5:0.2257, 13.0:0.2170, 13.5:0.2090, 14.0:0.2016,
              14.5:0.1947, 15.0:0.1882, 16.0:0.1765, 17.0:0.1661, 18.0:0.1589,
              19.0:0.1487, 20.0:0.1413, 21.0:0.1346, 22.0:0.1285, 23.0:0.1229,
              24.0:0.1178, 25.0:0.1131, 26.0:0.1087, 28.0:0.1010, 30.0:0.942,
              32.0:0.0884, 34.0:0.0832, 36.0:0.0785, 38.0:0.0744, 40.0:0.0707,
              42.0:0.0673, 44.0:0.0643, 46.0:0.0615, 48.0:0.0589, 50.0:0.0566,
              60.0:0.0471, 70.0:0.0404, 80.0:0.0354, 90.0:0.0314, 100.0:0.0283
              })


def skinDepth(freq, conductivity, mur, epsr):
    return (1/(conductivity*sp.pi*freq*mur*mu0))**0.5

def inductCoax(freq, rInner, rOuter, cond):
    return 2*10**(-4)*sp.log((2*rOuter+ skinDepth(freq, cond, 1, 1))/(2*rInner+ skinDepth(freq, cond, 1, 1)))

def inductInnerCond(freq, rInner, lenCoax, cond, muPrime, muDouble):
    return 2*lenCoax*(sp.log(2*lenCoax/rInner)-1+(muDouble/4)*directXToT.get(2*sp.pi*rInner*(2*muPrime*mu0*freq/cond)**0.5))

def inductOuterCond(rOuter, lenCoax):
    return 2*lenCoax*(sp.log(2*lenCoax/rOuter)-1)

def inductCoaxLine(freq, rInner, rOuter, cond, muPrime):
##    print sp.log(rOuter/rInner), directXToT.get(2*sp.pi*rInner*(2*muPrime*freq*cond)**0.5)
    return 2*(sp.log(rOuter/rInner) + 0.25*directXToT.get(2*sp.pi*rInner*(2*mu0*muPrime*freq*cond)**0.5))

def fileInductRead(targetFile):
    fileRead=open(targetFile, 'r+')

    listData = fileRead.readlines()
    fileRead.close()
    listCurrents = []
    listStoredEnergyElectric=[]
    listStoredEnergyMagnetic=[]
    for entry in listData:
        if entry.startswith("Total current (integral J ds)  "):
            tidbit=entry.replace("Total current (integral J ds)                    = ","")
            listCurrents.append(float(tidbit.replace(" [A]", "")))
        elif entry.startswith("Stored energy/unit length (integral A.J/2 ds)"):
            tidbit=entry.replace("Stored energy/unit length (integral A.J/2 ds)    = ", "")
            listStoredEnergyElectric.append(float(tidbit.replace(" [J/mm]", ""))*10.0**3)
        elif entry.startswith("Stored energy/unit length (integral B.H/2 ds) "):
            tidbit=entry.replace("Stored energy/unit length (integral B.H/2 ds)    = ", "")
            listStoredEnergyMagnetic.append(float(tidbit.replace(" [J/mm]", ""))*10.0**3)
        else:
            pass

    tempCurrents = []
    tempStoredEnergyElectric=[]
    tempStoredEnergyMagnetic=[]

    for i in range(0,len(listStoredEnergyMagnetic)-1,2):
        tempCurrents.append(listCurrents[i]+listCurrents[i+1])
        tempStoredEnergyElectric.append((listStoredEnergyElectric[i]**2+listStoredEnergyElectric[i+1]**2)**0.5)
        tempStoredEnergyMagnetic.append((listStoredEnergyMagnetic[i]**2+listStoredEnergyMagnetic[i+1]**2)**0.5)


    arrCurrents=sp.array(tempCurrents)
    arrStoredEnergyElectric=sp.array(tempStoredEnergyElectric)
    arrStoredEnergyMagnetic=sp.array(tempStoredEnergyMagnetic)
    listInductances = []
##    print arrStoredEnergyMagnetic

    #Calculate inductances using U=0.5 I^2 L U and I multiplied by 4 for quarter geometry

    geoFact=2.0
    for i in range(0,len(arrCurrents)):
        listInductances.append([arrStoredEnergyElectric[i]*geoFact*2/(geoFact*arrCurrents[i])**2,arrStoredEnergyMagnetic[i]*4*2/(4*arrCurrents[i])**2])

    arrInductances=sp.array(listInductances)

    return arrInductances

def inductOutput(array, freq, fileName):
    output=open(fileName, 'w+')
    for i in range(0,len(array)):
        output.write(str(freq[i])+','+str(array[i,1])+'\n')
    output.close()

def lowFreqCircuitParam(array, freq, lower, upper):
    induct=(2*sp.pi*freq[lower])*(2*sp.pi*freq[upper])*array[lower,1]*array[upper,1]*((2*sp.pi*freq[lower])**2-(2*sp.pi*freq[upper])**2)/(2*sp.pi*freq[lower]*array[lower,1]*(2*sp.pi*freq[lower])**2*(2*sp.pi*freq[upper])-2*sp.pi*freq[upper]*array[upper,1]*(2*sp.pi*freq[upper])**2*(2*sp.pi*freq[lower]))
    res=((2*sp.pi*array[upper,1]*(2*sp.pi*induct)**2)/abs((2*sp.pi*induct)-(2*sp.pi*array[upper,1])))**0.5
    print freq[upper], freq[lower]
    return res, induct-array[-1,1]

def highFreqCircuitParam(array, freq, lower, upper):
    res=array[lower,0]*array[upper,0]*(freq[upper]**2-freq[lower]**2)/(array[lower,0]*freq[upper]**2-array[upper,0]*freq[lower]**2)
    induct = 1/(2*sp.pi*freq[upper])*(array[upper,0]*res**2/abs(res-array[upper,0]))**0.5
    return res, induct

####### Test of calling analysis of OPERA2D from python ########

####### A coaxial line ########

directTar = "E:/OperaModels/opera_logs/"
outputDir = "E:/OperaModels/MKIInductOutput/"

try:
    os.mkdir(outputDir)
except:
    pass

freqList = []
for i in range(-1,9):
    for j in range(1,10):
        freqList.append(j*10.0**i)

freqList.append(10.0**9)
freqList=sp.array(freqList)


####### Cross section of screen conductors and gnd plate ########

inductOnlyInnerCond = fileInductRead(directTar+"PP_201306241405375201.lp")
inductOutput(inductOnlyInnerCond, freqList, outputDir+"inductGNDInner.csv")
inductOnlyScreenCond = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201306250946426041.lp")
inductOutput(inductOnlyScreenCond, freqList, outputDir+"inductGNDScreens.csv")
##inductOnlyScreenCond = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201306251005027481.lp")
inductOnlyGndCond = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201306251021208201.lp")
inductOutput(inductOnlyGndCond, freqList, outputDir+"inductGNDCond.csv")
inductOnlyGndCondMutual = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201306251105404841.lp")
inductOnlyInnerCondMutual = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201306251103075361.lp")
inductOnlyInnerGNDMutual = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201306251045247561.lp")
inductOnlyGndOuterMutual = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201306251141281721.lp")
inductOnlyOuterGNDCondMutual = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201306251143407081.lp")
inductOnlyOuterGNDMutual = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201306251124435161.lp")
inductOnlyInnerOuterMutual = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201306251226407081.lp")
inductOnlyInnerOuterCondMutual = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201306251228565001.lp")
inductOnlyInnerOuterMutual = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201306251203356481.lp")
##
##inductInnerCondMetal = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201306260948352121.lp")
##inductInnerCondMetalOther = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201306261317598521.lp")
##inductScreenCondMetal = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201306261324257881.lp")
##inductMetallizationMetal = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201306261604495481.lp")
##inductMetallizationScreenCondAll = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307011356231281.lp")
##inductMetallizationScreenCondMetal = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307011410434481.lp")
##inductMetallizationScreenCondCond = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307011409161721.lp")
##inductMetallizationCentreAll = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307011419333721.lp")
##inductMetallizationCentreMetal = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307011431402081.lp")
##inductMetallizationCentreCent = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307011422285241.lp")
##inductScreenCondCentreAll = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307011443435881.lp")
##inductScreenCondCentreCond = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307011449403001.lp")
##inductScreenCondCentreCent = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307011448259841.lp")

inductExtResInner = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307090949323681.lp")
inductOutput(inductExtResInner, freqList, outputDir+"inductExtResInner.csv")
inductExtResInnerOnly = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307090959510601.lp")
inductExtResMetal = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091059569801.lp")
inductOutput(inductExtResMetal, freqList, outputDir+"inductExtResMetal.csv")
inductExtResMetalOnly = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091115579561.lp")
inductExtResScreens = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091043446801.lp")
inductOutput(inductExtResScreens, freqList, outputDir+"inductExtResScreens.csv")
inductExtResScreensOnly = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091023185241.lp")
inductExtResResistor = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091119333041.lp")
inductOutput(inductExtResResistor, freqList, outputDir+"inductExtResResistor.csv")
inductExtResResistorOnly = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091135415921.lp")
inductExtResFerrite= fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091111051921.lp")
inductOutput(inductExtResFerrite, freqList, outputDir+"inductExtResIFerrite.csv")
inductExtResFerriteOnly= fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307081654130761.lp")


inductExtResInnerScreensAll = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091350325481.lp")
inductExtResInnerScreensInner = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091504303201.lp")
inductExtResInnerScreensScreens = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091507381441.lp")
inductExtResInnerScreensMetal = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111222439481.lp")
inductExtResInnerScreensFerrite = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111220335921.lp")
inductExtResInnerScreensRes = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111216289121.lp")

inductExtResInnerMetalAll = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091333551841.lp")
inductExtResInnerMetalInner = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091344304681.lp")
inductExtResInnerMetalMetal = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091347472641.lp")
inductExtResInnerMetalScreens = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111225557121.lp")
inductExtResInnerMetalFerrite = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111230090921.lp")
inductExtResInnerMetalRes = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111232078161.lp")

inductExtResScreensMetalAll = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091513072041.lp")
inductExtResScreensMetalScreens = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091520108641.lp")
inductExtResScreensMetalMetal = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091522514561.lp")
inductExtResScreensMetalInner = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111301247481.lp")
inductExtResScreensMetalFerrite = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111258550761.lp")
inductExtResScreensMetalRes = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111253439921.lp")

inductExtResScreensFerriteAll = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091525096601.lp")
inductExtResScreensFerriteScreens = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091530004041.lp")
inductExtResScreensFerriteFerrite = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091533568321.lp")
inductExtResScreensFerriteInner = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111248560121.lp")
inductExtResScreensFerriteMetal = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111246348481.lp")
inductExtResScreensFerriteRes = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111250578601.lp")

inductExtResScreensResAll = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091536150081.lp")
inductExtResScreensResScreens = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091618366841.lp")
inductExtResScreensResRes = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091623064961.lp")
inductExtResScreensResInner = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111142215921.lp")
inductExtResScreensResMetal = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111138441441.lp")
inductExtResScreensResFerrite = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111129043721.lp")

inductExtResMetalResAll = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091700387481.lp")
inductExtResMetalResMetal = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091706457681.lp")
inductExtResMetalResRes = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091709126801.lp")
inductExtResMetalResFerrite = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111124250361.lp")
inductExtResMetalResScreens = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111122220361.lp")
inductExtResMetalResInner = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111118225281.lp")

inductExtResMetalFerriteAll = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091711445601.lp")
inductExtResMetalFerriteMetal = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091716296601.lp")
inductExtResMetalFerriteFerrite = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091720524161.lp")
inductExtResMetalFerriteInner = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111234056241.lp")
inductExtResMetalFerriteScreens = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111238145401.lp")
inductExtResMetalFerriteRes = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111240199201.lp")

inductExtResFerriteResAll = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091723308041.lp")
inductExtResFerriteResFerrite = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091729289281.lp")
inductExtResFerriteResRes = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091731454081.lp")
inductExtResFerriteResMetal = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307110926048281.lp")
inductExtResFerriteResScreens = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111027087401.lp")
inductExtResFerriteResInner = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111100003801.lp")

inductExtResInnerResAll = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091142121881.lp")
inductExtResInnerResAllTest = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091157559121.lp")
inductExtResInnerResInner = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091212553441.lp")
inductExtResInnerResRes = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091209541281.lp")
inductExtResInnerResFerrite = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111157048001.lp")
inductExtResInnerResMetal = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111152523481.lp")
inductExtResInnerResScreens = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111146453281.lp")

inductExtResInnerFerriteAll = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091317169561.lp")
inductExtResInnerFerriteInner = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091324387841.lp")
inductExtResInnerFerriteFerrite = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307091327375481.lp")
inductExtResInnerFerriteScreens = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111202441241.lp")
inductExtResInnerFerriteMetal = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111208533761.lp")
inductExtResInnerFerriteRes = fileInductRead("E:/OperaModels/MKICrossSection/opera_logs/PP_201307111214097721.lp")

print mu0/(2*sp.pi)*sp.log(21/0.5)

####### Analytical Calculations ########



selfInductInner = []
totalInductCoax = []
thing = []
for entry in freqList:
    thing.append([2*sp.pi*rIn*(2*muCopper*mu0*entry*condCopper)**0.5, directXToT.get(2*sp.pi*rIn*(2*muCopper*mu0*entry*condCopper)**0.5)])
    selfInductInner.append(inductInnerCond(entry, rIn, lenCable, condCopper, muCopper, muDoubleCopper))
    totalInductCoax.append(inductCoaxLine(entry, rIn, rOut, condCopper, muCopper))

selfInductOuter = inductOuterCond(rOut, lenCable)

##inductCoax =inductCoax(freqList, rIn, rOut, condCopper)
thing=sp.array(thing)

##pl.loglog()
pl.semilogx()

##pl.plot(freqList, inductExtResInner[:,1]*10**9, label="Inner Self Inductance")
##pl.plot(freqList, inductExtResInnerOnly[:,1]*10**9, label="Inner Self Inductance")
##pl.plot(freqList, inductExtResScreens[:,1]*10**9, label="Ext Res Screens Self Inductance")
##pl.plot(freqList, inductExtResScreensOnly[:,1]*10**9, label="Ext Res Screens Self Inductance")
##pl.plot(freqList, inductExtResMetal[:,1]*10**9, label="Ext Res Metal Self Inductance")
##pl.plot(freqList, inductExtResMetalOnly[:,1]*10**9, label="Ext Res Metal Self Inductance")
##pl.plot(freqList, inductExtResResistor[:,1]*10**9, label="Ext Res Resistor Self Inductance")
##pl.plot(freqList, inductExtResResistorOnly[:,1]*10**9, label="Ext Res Resistor Self Inductance")
##pl.plot(freqList, inductExtResFerrite[:,1]*10**9, label="Ext Res Ferrite Self Inductance")
##pl.plot(freqList, inductExtResFerriteOnly[:,1]*10**9, label="Ext Res Ferrite Self Inductance")
##pl.plot(freqList, inductExtResInnerScreensInner[:,1]*10**9, label="Inner  InnerScreens")
##pl.plot(freqList, inductExtResInnerScreensScreens[:,1]*10**9, label="Screens InnerScreens")
##pl.plot(freqList, inductExtResInnerMetalInner[:,1]*10**9, label="Inner  InnerMetal")
##pl.plot(freqList, inductExtResInnerMetalMetal[:,1]*10**9, label="Metal InnerMetal")
##pl.plot(freqList, inductExtResInnerFerriteInner[:,1]*10**9, label="Inner InnerFerr")
##pl.plot(freqList, inductExtResInnerFerriteFerrite[:,1]*10**9, label="Ferrite InnerFerr")
##pl.plot(freqList, inductExtResInnerResInner[:,1]*10**9, label="Inner InnerRes")
##pl.plot(freqList, inductExtResInnerResRes[:,1]*10**9, label="Res InnerRes")
##pl.plot(freqList, inductExtResScreensMetalScreens[:,1]*10**9, label="Screens ScreensMetal")
##pl.plot(freqList, inductExtResScreensMetalMetal[:,1]*10**9, label="Metal ScreensMetal")
##pl.plot(freqList, inductExtResScreensFerriteScreens[:,1]*10**9, label="Screens ScreensFerr")
##pl.plot(freqList, inductExtResScreensFerriteFerrite[:,1]*10**9, label="Ferrite ScreensFerr")
##pl.plot(freqList, inductExtResScreensResScreens[:,1]*10**9, label="Screens ScreensRes")
##pl.plot(freqList, inductExtResScreensResRes[:,1]*10**9, label="Res ScreensRes")
##pl.plot(freqList, inductExtResMetalFerriteMetal[:,1]*10**9, label="Metal MetalFerr")
##pl.plot(freqList, inductExtResMetalFerriteFerrite[:,1]*10**9, label="Ferrite MetalFerr")
##pl.plot(freqList, inductExtResMetalResMetal[:,1]*10**9, label="Metal MetalRes")
##pl.plot(freqList, inductExtResMetalResRes[:,1]*10**9, label="Res MetalRes")
##pl.plot(freqList, inductExtResFerriteResFerrite[:,1]*10**9, label="Ferrite FerriteRes")
##pl.plot(freqList, inductExtResFerriteResRes[:,1]*10**9, label="Res FerriteRes")

##pl.plot(freqList, inductExtResInnerScreensInner[:,0]*10**9, label="Inner InnerScreens")
##pl.plot(freqList, inductExtResInnerScreensScreens[:,0]*10**9, label="Screens InnerScreens")

##
pl.plot(freqList, inductOnlyScreenCond[:,0]*10**9, label="Screen Cond. Self Inductance")
pl.plot(freqList, inductOnlyGndCond[:,0]*10**9, label="GND Cond. Self Inductance")
pl.plot(freqList, inductOnlyGndCondMutual[:,0]*10**9, label="GND Cond. Mutual Inductance")
##pl.plot(freqList, inductOnlyInnerCondMutual[:,0]*10**9, label="Inner Cond. Mutual Inductance")
##pl.plot(freqList, inductOnlyInnerGNDMutual[:,0]*10**9, label="GND/Inner Mutual Inductance")
##pl.plot(freqList, inductOnlyGndOuterMutual[:,0]*10**9, label="GND Cond. Mutual Inductance")
##pl.plot(freqList, inductOnlyOuterGNDCondMutual[:,0]*10**9, label="Outer Cond. Mutual Inductance")
##pl.plot(freqList, inductOnlyOuterGNDMutual[:,0]*10**9, label="GND/Outer Mutual Inductance")
##pl.plot(freqList, inductOnlyInnerOuterMutual[:,0]*10**9, label="Inner Cond. Mutual Inductance")
##pl.plot(freqList, inductOnlyInnerOuterCondMutual[:,0]*10**9, label="Outer Cond. Mutual Inductance")
##pl.plot(freqList, inductOnlyInnerOuterMutual[:,0]*10**9, label="GND/Outer Mutual Inductance")
##pl.plot(freqList, inductInnerCondMetal[:,0]*10**9, label="Inner Self Inductance")
##pl.plot(freqList, inductInnerCondMetalOther[:,0]*10**9, label="Inner Self Inductance")
##pl.plot(freqList, inductScreenCondMetal[:,0]*10**9, label="Screen Self Inductance")
##pl.plot(freqList, inductExtResInner[:,0]*10**9, label="Ext Res Inner Self Inductance")
pl.legend(loc="lower left")
pl.xlabel("Frequency (Hz)")
pl.ylabel("Inductance (nH)")
##pl.axis([10**0,10**9,0,500])
pl.show()
pl.clf()


print lowFreqCircuitParam(inductExtResFerrite, freqList, 1, 42)
print lowFreqCircuitParam(inductExtResFerrite, freqList, 43, 53)
print lowFreqCircuitParam(inductExtResFerrite, freqList, 54, 60)[0]-lowFreqCircuitParam(inductExtResFerrite, freqList, 1, 53)[0]
print lowFreqCircuitParam(inductExtResFerrite, freqList, 61, 69)[0]-lowFreqCircuitParam(inductExtResFerrite, freqList, 1, 60)[0]
print lowFreqCircuitParam(inductExtResFerrite, freqList, 70, 79)[0]-lowFreqCircuitParam(inductExtResFerrite, freqList, 1, 69)[0]
print lowFreqCircuitParam(inductExtResFerrite, freqList, 80, 89)[0]-lowFreqCircuitParam(inductExtResFerrite, freqList, 1, 79)[0]
print lowFreqCircuitParam(inductExtResFerrite, freqList, 54, 60)[1]-lowFreqCircuitParam(inductExtResFerrite, freqList, 1, 53)[1]
print lowFreqCircuitParam(inductExtResFerrite, freqList, 61, 69)[1]-lowFreqCircuitParam(inductExtResFerrite, freqList, 1, 60)[1]
print lowFreqCircuitParam(inductExtResFerrite, freqList, 70, 79)[1]-lowFreqCircuitParam(inductExtResFerrite, freqList, 1, 69)[1]
print lowFreqCircuitParam(inductExtResFerrite, freqList, 80, 89)[1]-lowFreqCircuitParam(inductExtResFerrite, freqList, 1, 79)[1]

print inductOnlyInnerGNDMutual[:,0]/(inductOnlyInnerCond[:,0]*inductOnlyGndCond[:,0])**0.5

coupFactorInnerScreens = inductExtResInnerScreensInner[:,0]/(inductExtResInner[:,0]*inductExtResScreens[:,0])**0.5
coupFactorInnerMetal= inductExtResInnerMetalInner[:,0]/(inductExtResMetal[:,0]*inductExtResInner[:,0])**0.5
coupFactorInnerFerrite = inductExtResInnerFerriteInner[:,0]/(inductExtResFerrite[:,0]*inductExtResInner[:,0])**0.5
coupFactorInnerRes = inductExtResInnerResInner[:,0]/(inductExtResResistor[:,0]*inductExtResInner[:,0])**0.5
coupFactorScreensMetal= inductExtResScreensMetalScreens[:,0]/(inductExtResMetal[:,0]*inductExtResScreens[:,0])**0.5
coupFactorScreensFerrite = inductExtResScreensFerriteScreens[:,0]/(inductExtResFerrite[:,0]*inductExtResScreens[:,0])**0.5
coupFactorScreensRes = inductExtResScreensResScreens[:,0]/(inductExtResResistor[:,0]*inductExtResScreens[:,0])**0.5
coupFactorMetalFerrite = inductExtResMetalFerriteMetal[:,0]/(inductExtResFerrite[:,0]*inductExtResMetal[:,0])**0.5
coupFactorMetalRes = inductExtResMetalResMetal[:,0]/(inductExtResResistor[:,0]*inductExtResMetal[:,0])**0.5
coupFactorFerriteRes = inductExtResFerriteResFerrite[:,0]/(inductExtResResistor[:,0]*inductExtResFerrite[:,0])**0.5

print coupFactorInnerScreens[0]
print coupFactorInnerMetal[0]
print coupFactorInnerFerrite[0]
print coupFactorInnerRes[0]
##print coupFactorScreensMetal[0]
##print coupFactorScreensFerrite[0]
##print coupFactorScreensRes[0]
##print coupFactorMetalFerrite[0]
##print coupFactorMetalRes[0]
##print coupFactorFerriteRes[0]


