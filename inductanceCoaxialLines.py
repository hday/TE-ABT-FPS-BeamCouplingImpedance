import scipy as sp
import pylab as pl
import os, sys

class ClosestDict(dict):
    def get(self, key):
        key=min(self.iterkeys(), key=lambda x:abs(x-key))
        return dict.get(self,key)
    
mu0 = 4*sp.pi*10.0**-7
muCopper = 1.0
muDoubleCopper = 1.0
condCopper = 1.0*10.0**7
rIn = 0.0005
rOut = 0.010
lenCable=0.0

directXToT = ClosestDict({0.0: 1.0, 0.5: 0.9998, 0.6: 0.9997, 0.7:0.9994, 0.8:0.9989,
              0.9:0.9983, 1.0:0.9974, 1.1:0.9962, 1.2:0.9946, 1.3:0.9927,
              1.4:0.9902, 1.5:0.9871, 1.6:0.9834, 1.7:0.9790, 1.8:0.9739,
              1.9:0.9680, 2.0:0.9611, 2.2:0.9448, 2.4:0.9248, 2.6:0.9013,
              2.8:0.8745, 3.0:0.8452, 3.2:0.8140, 3.4:0.7818, 3.6:0.7493,
              3.8:0.7173, 4.0:0.6863, 4.2:0.6568, 4.4:0.6289, 4.6:0.6028,
              4.8:0.5785, 5.0:0.556, 5.2:0.5351, 5.4:0.5157, 5.6:0.4976,
              5.8:0.4809, 6.0:0.4652, 6.2:0.4506, 6.4:0.4368, 6.6:0.4239,
              6.8:0.4117, 7.0:0.4002, 7.2:0.3893, 7.4:0.3790, 7.6:0.3692,
              7.8:0.3599, 8.0:0.3511, 8.2:0.3426, 8.4:0.3346,8.6:0.3269,
              8.8:0.3196, 9.0:0.3126, 9.2:0.3058, 9.4:0.2994, 9.6:0.2932,
              9.8:0.2873, 10.0:0.2816, 10.5:0.2682, 11.0:0.2562, 11.5:0.2452,
              12.0:0.2350, 12.5:0.2257, 13.0:0.2170, 13.5:0.2090, 14.0:0.2016,
              14.5:0.1947, 15.0:0.1882, 16.0:0.1765, 17.0:0.1661, 18.0:0.1589,
              19.0:0.1487, 20.0:0.1413, 21.0:0.1346, 22.0:0.1285, 23.0:0.1229,
              24.0:0.1178, 25.0:0.1131, 26.0:0.1087, 28.0:0.1010, 30.0:0.942,
              32.0:0.0884, 34.0:0.0832, 36.0:0.0785, 38.0:0.0744, 40.0:0.0707,
              42.0:0.0673, 44.0:0.0643, 46.0:0.0615, 48.0:0.0589, 50.0:0.0566,
              60.0:0.0471, 70.0:0.0404, 80.0:0.0354, 90.0:0.0314, 100.0:0.0283
              })


def skinDepth(freq, conductivity, mur, epsr):
    return (1/(conductivity*sp.pi*freq*mur*mu0))**0.5

def inductCoax(freq, rInner, rOuter, cond):
    return 2*10**(-4)*sp.log((2*rOuter+ skinDepth(freq, cond, 1, 1))/(2*rInner+ skinDepth(freq, cond, 1, 1)))

def inductInnerCond(freq, rInner, lenCoax, cond, muPrime, muDouble):
    return 2*lenCoax*(sp.log(2*lenCoax/rInner)-1+(muDouble/4)*directXToT.get(2*sp.pi*rInner*(2*muPrime*mu0*freq/cond)**0.5))

def inductOuterCond(rOuter, lenCoax):
    return 2*lenCoax*(sp.log(2*lenCoax/rOuter)-1)

def inductCoaxLine(freq, rInner, rOuter, cond, muPrime):
##    print sp.log(rOuter/rInner), directXToT.get(2*sp.pi*rInner*(2*muPrime*freq*cond)**0.5)
    return 2*(sp.log(rOuter/rInner) + 0.25*directXToT.get(2*sp.pi*rInner*(2*mu0*muPrime*freq*cond)**0.5))

def fileInductRead(targetFile):
    fileRead=open(targetFile, 'r+')

    listData = fileRead.readlines()
    fileRead.close()
    listCurrents = []
    listStoredEnergyElectric=[]
    listStoredEnergyMagnetic=[]
    for entry in listData:
        if entry.startswith("Total current (integral J ds)  "):
            tidbit=entry.replace("Total current (integral J ds)                    = ","")
            listCurrents.append(float(tidbit.replace(" [A]", "")))
        elif entry.startswith("Stored energy/unit length (integral A.J/2 ds)"):
            tidbit=entry.replace("Stored energy/unit length (integral A.J/2 ds)    = ", "")
            listStoredEnergyElectric.append(float(tidbit.replace(" [J/mm]", ""))*10.0**3)
        elif entry.startswith("Stored energy/unit length (integral B.H/2 ds) "):
            tidbit=entry.replace("Stored energy/unit length (integral B.H/2 ds)    = ", "")
            listStoredEnergyMagnetic.append(float(tidbit.replace(" [J/mm]", ""))*10.0**3)
        else:
            pass

    tempCurrents = []
    tempStoredEnergyElectric=[]
    tempStoredEnergyMagnetic=[]

    for i in range(0,len(listStoredEnergyMagnetic)-1,2):
        tempCurrents.append(listCurrents[i]+listCurrents[i+1])
        tempStoredEnergyElectric.append((listStoredEnergyElectric[i]**2+listStoredEnergyElectric[i+1]**2)**0.5)
        tempStoredEnergyMagnetic.append((listStoredEnergyMagnetic[i]**2+listStoredEnergyMagnetic[i+1]**2)**0.5)


    arrCurrents=sp.array(tempCurrents)
    arrStoredEnergyElectric=sp.array(tempStoredEnergyElectric)
    arrStoredEnergyMagnetic=sp.array(tempStoredEnergyMagnetic)
    listInductances = []
##    print arrStoredEnergyMagnetic

    #Calculate inductances using U=0.5 I^2 L U and I multiplied by 4 for quarter geometry

    geoFact=4.0
    for i in range(0,len(arrCurrents)):
        listInductances.append([arrStoredEnergyElectric[i]*geoFact*2/(geoFact*arrCurrents[i])**2,arrStoredEnergyMagnetic[i]*4*2/(4*arrCurrents[i])**2])

    arrInductances=sp.array(listInductances)

    return arrInductances

    

####### Test of calling analysis of OPERA2D from python ########

####### A coaxial line ########

directTar = "E:/OperaModels/opera_logs/"

##### Inner Conductor #####
##### Region 1 #####


####### All Regions #####
fileTar = "PP_201306101039041601.lp"

innerInductAll = fileInductRead(directTar+fileTar)

fileTar = "PP_201306101015154041.lp"

innerInductInner = fileInductRead(directTar+fileTar)

##### Outer Conductor #####

##### All Regions #####


##### Region 1 Only #####
fileTar = "PP_201306101054268001.lp"

outerInductAll = fileInductRead(directTar+fileTar)



##### Region 1 Only #####
fileTar = "PP_201306101104051281.lp"

outerInductOuter = fileInductRead(directTar+fileTar)

##### Both Conductors #####

##### All Regions #####


##### Region 1 Only #####
fileTar = "PP_201306101113466561.lp"

mutualInductInner = fileInductRead(directTar+fileTar)


##### Region 1 Only #####
fileTar = "PP_201306101107425281.lp"

mutualInductOuter = fileInductRead(directTar+fileTar)


####### Region 1 Only #####
fileTar = "PP_201306101116005321.lp"
##
mutualInductBoth = fileInductRead(directTar+fileTar)

fileTar = "PP_201306101158121721.lp"

mutualInductAirGap = fileInductRead(directTar+fileTar)

fileTar = "PP_201306101200594041.lp"

mutualInductMetals = fileInductRead(directTar+fileTar)

print mu0/(2*sp.pi)*sp.log(20/5)

####### Analytical Calculations ########


freqList = sp.logspace(0, 9, 81)

selfInductInner = []
totalInductCoax = []
thing = []
for entry in freqList:
    thing.append([2*sp.pi*rIn*(2*muCopper*mu0*entry*condCopper)**0.5, directXToT.get(2*sp.pi*rIn*(2*muCopper*mu0*entry*condCopper)**0.5)])
    selfInductInner.append(inductInnerCond(entry, rIn, lenCable, condCopper, muCopper, muDoubleCopper))
    totalInductCoax.append(inductCoaxLine(entry, rIn, rOut, condCopper, muCopper))

selfInductOuter = inductOuterCond(rOut, lenCable)

##inductCoax =inductCoax(freqList, rIn, rOut, condCopper)
thing=sp.array(thing)


##pl.loglog()
pl.semilogx()
pl.plot(freqList, innerInductAll[-81:,1]*10.0**9, 'k-', label="Inner Inductance")
pl.plot(freqList, innerInductInner[-81:,1]*10.0**9, 'kx', label="Inner Inductance Inner Area Only")
pl.plot(freqList, outerInductAll[-81:,1]*10.0**9, 'b-', label="Outer Inductance")
pl.plot(freqList, outerInductOuter[-81:,1]*10.0**9, 'bx', label="Outer Inductance Outer Area Only")
##pl.plot(freqList, mutualInductInner[-81:,0]*10.0**9, 'rx', label="Mutual Inductance Inner Area Only")
##pl.plot(freqList, innerInductAll[-81:,0]*10.0**9-outerInductOuter[-81:,0]*10.0**9, 'g-')
##pl.plot(freqList, innerInductAll[-81:,0]*10.0**9-outerInductAll[-81:,0]*10.0**9, 'gx')
##pl.plot(freqList, innerInductAll[-81:,1]*10.0**9-mutualInductOuter[-81:,1]*10.0**9, 'g-')
##pl.plot(freqList, innerInductAll[-81:,1]*10.0**9-mutualInductOuter[-81:,1]*10.0**9, 'gx')
pl.plot(freqList, innerInductAll[-81:,1]*10.0**9-mutualInductInner[-81:,1]*10.0**9, 'g-')
pl.plot(freqList, innerInductAll[-81:,1]*10.0**9-mutualInductInner[-81:,1]*10.0**9, 'gx')
pl.plot(freqList, mutualInductInner[-81:,1]*10.0**9, 'r-')
##pl.plot(freqList, mutualInductMetals[-81:,0]*10.0**9, 'ro', label="Mutual Inductance Metals Only")
##pl.plot(freqList, mutualInductBoth[-81:,0]*10.0**9, 'go', label="Mutual Inductance Both Area Only")


##pl.plot(freqList, selfInductInner, label="Self Inductance Analytical")
##pl.plot(freqList, totalInductCoax, label="Total Inductance Analytical")
##pl.legend(loc="lower left")
pl.xlabel("Frequency (Hz)")
pl.ylabel("Inductance (nH)")
##pl.axis([10**0,10**9,0,500])
pl.show()
pl.clf()


############ A coaxial line with an external conductor ##############
##
inductInnerCond = fileInductRead(directTar+"PP_201306110948015361.lp")
inductOuterCond = fileInductRead(directTar+"PP_201306111042381081.lp")
inductBlockCond = fileInductRead(directTar+"PP_201306111044315681.lp")
inductBlockToInnerCond = fileInductRead(directTar+"PP_201306111059043561.lp")
inductBlockToInnerCondNoOuterCond = fileInductRead(directTar+"PP_201306111105335601.lp")
inductBlockToOuterCond = fileInductRead(directTar+"PP_201306111137262801.lp")
inductBlockToOuterCondNoInnerCond = fileInductRead(directTar+"PP_201306111128364521.lp")
inductInnerToOuterCond = fileInductRead(directTar+"PP_201306111143149161.lp")
inductInnerToOuterCondNoBlock = fileInductRead(directTar+"PP_201306111149360641.lp")
inductInnerToOuterCondInnerOnly = fileInductRead(directTar+"PP_201306111159504281.lp")
inductInnerToOuterCondOuterOnly = fileInductRead(directTar+"PP_201306111206529961.lp")

print mu0/(2*sp.pi)*sp.log(10/5)

##
##freqList = sp.logspace(0, 9, 81)
##
##pl.loglog()
pl.semilogx()
pl.plot(freqList, inductInnerCond[:,0]*10.0**9, label="Inner Cond")
pl.plot(freqList, inductOuterCond[:,0]*10.0**9, label="Outer Cond")
pl.plot(freqList, inductBlockCond[:,0]*10.0**9, label="Block Cond")
pl.plot(freqList, inductBlockToInnerCond[:,0]*10.0**9, 'r-', label="Block To Inner Cond")
pl.plot(freqList, inductBlockToInnerCondNoOuterCond[:,0]*10.0**9, 'rx', label="Block To Inner Cond Limited")
pl.plot(freqList, inductBlockToOuterCond[:,0]*10.0**9, 'k-', label="Block To Outer Cond")
pl.plot(freqList, inductBlockToOuterCondNoInnerCond[:,0]*10.0**9, 'kx', label="Block To Outer Cond Limited")
pl.plot(freqList, inductInnerToOuterCond[:,0]*10.0**9, 'b-', label="Inner To Outer Cond")
pl.plot(freqList, inductInnerToOuterCondNoBlock[:,0]*10.0**9, 'bx', label="Inner To Outer Cond Limited")
pl.plot(freqList, inductInnerToOuterCondInnerOnly[:,0]*10.0**9, 'bx', label="Inner To Outer Cond Inner Only")
##pl.plot(freqList, inductInnerToOuterCondOuterOnly[:,0]*10.0**9, 'bx', label="Inner To Outer Cond Outer Only")

pl.legend(loc="upper right")
pl.xlabel("Frequency (Hz)")
pl.ylabel("Inductance (nH)")
pl.axis([10**0,10**9,0,1000])
##pl.show()
pl.clf()


