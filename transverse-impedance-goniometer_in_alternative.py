import scipy as sp
import pylab as pl
import time, csv, sys, os
import matplotlib.pyplot as plt
import scipy.integrate as inte

########################## Imported Profile ##################################

start = time.time()

#######   SPS parameters for LHC type beam  #######

C = 299792458.0
circ=6911.0

def Z_bb(freq, data):
    return data[2]/complex(1, data[1]*(freq/data[0] - data[0]/freq))

def gauss_prof(dist, sigma, order):
    return sp.e**(-order*(dist/(2*sigma))**2)

def para_prof(dist, bunch_length):
    return 1-(2*dist/bunch_length)**2

def cos_prof(dist, bunch_length):
    return sp.cos(sp.pi/bunch_length*dist)**2

def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp

top_directory = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/hfssModel/testModes/"
dirList = [top_directory+direct for direct in os.listdir(top_directory)]

print dirList

print time.time()-start
