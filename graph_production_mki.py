import csv, os, sys
import scipy as sp
import pylab as pl


def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp


disp = 0.001
length = 2.88
f_rev = 3*10**8/27000.0
directory = "E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/full-device-impedances/" #Directory of data
largeInnerDiam = "largeInDiamFerr/longitudinal-impedance.csv"
largeInnerDiam15m = "largeInDiamFerr/longitudinal-impedance15m.csv"
cerrVac = "replacingCeraWithVac/longitudinal-impedance.csv"
without_metallisation = "without-metalisation/longitudinal-impedance.csv"
screen_conductors_15 = "with-15-screen-conductors/longitudinal-impedance.csv"
##screen_conductors_24 = "with-24-screen-conductors/longitudinal-impedance.csv"
screen_conductors_24 = "alternating-length-screen-cond/longitudinal-impedance.csv"
screen_conductors_15_long_9_short = "15-long-9-short-screen/longitudinal-impedance.csv"
screen_conductors_15_4hv = "15-and-4-hv-plate/longitudinal-impedance.csv"
screen_conductors_15_5hv = "15-and-5-hv-plate/longitudinal-impedance.csv"
screen_conductors_15_2hv = "17_screen_conductors/longitudinal-impedance.csv"
screen_conductors_none = "no-conductors/longitudinal-impedance.csv"
no_damping_ferrites = "no-damping-ferrites/longitudinal-impedance.csv"
no_screen = "no-screen/longitudinal-impedance.csv"
alt_screen_1 = "alt-screen-design/longitudinal-impedance.csv"
alt_screen_embedded_cerr = "embedded-in-tube/longitudinal-impedance.csv"
alt_screen_embedded_cerr_sec = "embedded-in-tube/longitudinal-impedance-second.csv"
enclosed = "24_conductors_enclosed_slots/longitudinal_impedance.csv"
alt_screen_no_cerr = "embedded-tube-vacuum/longitudinal-impedance.csv"
alt_screen_thick_cerr = "thick_ceramic_5.5mm/longitudinal-impedance.csv"
v_screen_24 = "v-screen-conductors-24/longitudinal-impedance.csv"
shortened_24 = "24_conductors_shortened_2mm/longitudinal-impedance.csv"
shortened_15 = "15_conductors_shortened_2mm/longitudinal-impedance.csv"
clustered_17 = "17_screen_conductors_together/longitudinal_impedance.csv"
clustered_19 = "19_conductors_together/longitudinal-impedance.csv"
alternating_length_19 = "19_conductors_alternating/longitudinal-impedance.csv"
alternating_length_24 = "24_conductors_alternating/longitudinal_impedance.csv"
alternating_v_24 = "24-conductors-alternating-then-tapered/longitudinal-impedance.csv"
alternating_length_24_step_1mm = "24_conductors_alternating_step_out_1mm/longitudinal-impedance.csv"
alternating_length_24_step_5mm = "24_conductors_alternating_step_out_5mm/longitudinal-impedance.csv"
alternating_length_24_step_2mm_1mm_off = "24_conductors_alternating_step_out_2mm_offset_1mm/longitudinal-impedance.csv"
alternating_length_24_step_2mm_1mm_off_v = "24_conductors_alternating_step_out_2mm_offset_1mm_v/longitudinal-impedance.csv"
alternating_length_24_step_2mm_1mm_off_v_overlap = "24_conductors_alternating_step_out_2mm_offset_1mm_v_overlapping_metallization/longitudinal-impedance.csv"
alternating_length_24_step_2mm_1mm_off_v_overlap_10mm = "24_conductors_v_2mm_step_10mm_either_side/longitudinal-impedance.csv"
alternating_length_24_step_2mm_1mm_off_v_overlap_20mm = "24_conductors_v_2mm_step_20mm_either_side/longitudinal-impedance.csv"
measurements_24 = "E:/PhD/1st_Year_09-10/Data/mki-heating/measurements/24-strips-measurements.csv"
measurements_15 = "E:/PhD/1st_Year_09-10/Data/mki-heating/measurements/15-strips-measurements.csv"
measurements_19 = "E:/PhD/1st_Year_09-10/Data/mki-heating/measurements/mki_measurements_mbarnes\MKI5_measurement_data-_4_6_2012/CentralTube-impedance-results.csv"
measurementsStepOut = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/31-05-13/tankNo13/resonator-impedance-results.csv"
measurements_cu_pipe = "E:/PhD/1st_Year_09-10/Data/mki-heating/measurements/mki_measurements_mbarnes\MKI5_measurement_data-_4_6_2012/neg_cu_tube-impedance-results.csv"

measure_stash_path = "E:/PhD/1st_Year_09-10/Data/mki-heating/measurements/mki_measurements_mbarnes/"

##dip_BL_file = "vertical.csv"
##dip_kroyer_file = "vertical_mine.csv"


list_files = [measure_stash_path+i for i in os.listdir(measure_stash_path)]
updated_file_list = []
for i in range(0,len(list_files)):
    if '.csv' in list_files[i]:
        updated_file_list.append(list_files[i])
    else:
        pass

datalargeInDiam = sp.array(extract_dat(directory+largeInnerDiam))
datalargeInDiam15m = sp.array(extract_dat(directory+largeInnerDiam15m))
dataCerrVac = sp.array(extract_dat(directory+cerrVac))
data_without_metalisation = sp.array(extract_dat(directory+without_metallisation))
data_screen_conductors_15 = sp.array(extract_dat(directory+screen_conductors_15))
data_screen_conductors_24 = sp.array(extract_dat(directory+screen_conductors_24))
data_screen_conductors_15_long_9_short = sp.array(extract_dat(directory+screen_conductors_15_long_9_short))
data_screen_conductors_15_4hv = sp.array(extract_dat(directory+screen_conductors_15_4hv))
data_screen_conductors_15_5hv = sp.array(extract_dat(directory+screen_conductors_15_5hv))
data_screen_conductors_15_2hv = sp.array(extract_dat(directory+screen_conductors_15_2hv))
data_screen_conductors_none = sp.array(extract_dat(directory+screen_conductors_none))
data_no_damping_ferrites = sp.array(extract_dat(directory+no_damping_ferrites))
data_no_screen = sp.array(extract_dat(directory+no_screen))
data_alt_screen_1 = sp.array(extract_dat(directory+alt_screen_1))
data_alt_screen_embedded_cerr = sp.array(extract_dat(directory+alt_screen_embedded_cerr))
data_alt_screen_embedded_cerr_sec = sp.array(extract_dat(directory+alt_screen_embedded_cerr_sec))
data_alt_screen_no_cerr = sp.array(extract_dat(directory+alt_screen_no_cerr))
data_alt_screen_thick_cerr = sp.array(extract_dat(directory+alt_screen_thick_cerr))
data_v_screen_24 = sp.array(extract_dat(directory+v_screen_24))
data_shortened_24 = sp.array(extract_dat(directory+shortened_24))
data_shortened_15 = sp.array(extract_dat(directory+shortened_15))
data_enclosed = sp.array(extract_dat(directory+enclosed))
data_17_together = sp.array(extract_dat(directory+clustered_17))
data_19_together = sp.array(extract_dat(directory+clustered_19))
data_19_alternating = sp.array(extract_dat(directory+alternating_length_19))
data_24_alternating = sp.array(extract_dat(directory+alternating_length_24))
data_24_alternating_v = sp.array(extract_dat(directory+alternating_v_24))
data_24_alternating_step_1mm = sp.array(extract_dat(directory+alternating_length_24_step_1mm))
data_24_alternating_step_5mm = sp.array(extract_dat(directory+alternating_length_24_step_5mm))
data_24_alternating_step_2mm_off = sp.array(extract_dat(directory+alternating_length_24_step_2mm_1mm_off))
data_24_alternating_step_2mm_off_v = sp.array(extract_dat(directory+alternating_length_24_step_2mm_1mm_off_v))
data_24_alternating_step_2mm_off_v_overlap = sp.array(extract_dat(directory+alternating_length_24_step_2mm_1mm_off_v_overlap))
data_24_alternating_step_2mm_off_v_overlap_10mm = sp.array(extract_dat(directory+alternating_length_24_step_2mm_1mm_off_v_overlap_10mm))
data_24_alternating_step_2mm_off_v_overlap_20mm = sp.array(extract_dat(directory+alternating_length_24_step_2mm_1mm_off_v_overlap_20mm))

directory = "E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/shortened_end_piece_impedance/"#Directory of data
stepOutCond = sp.array(extract_dat(directory+"150mm_metallization_56mm_tube_tapered_cond_100mm_overlap_stepoutCont/longitudinal-impedance.csv"))

strip_24_meas = []

input_file = open(measurements_24, 'r+')
tar = csv.reader(input_file, delimiter=",")

i=0
for row in tar:
    if i!=0:
        row = map(float, row)
        row[1] = float(row[1])*length 
##        n = float(row[0])*10**6/f_rev
##        row[1] = float(row[1])/0.003
##        row[3] = float(row[3])/0.003
        strip_24_meas.append(row)
    i+=1

input_file.close()
strip_24_meas = sp.array(strip_24_meas)

strip_15_meas = []

input_file = open(measurements_15, 'r+')
tar = csv.reader(input_file, delimiter=",")

i=0
for row in tar:
    if i!=0:
        row = map(float, row)
        row[0] = float(row[0])/10**6
        row[1] = float(row[1])*length 
##        row[1] = float(row[1])/0.003
##        row[3] = float(row[3])/0.003
        strip_15_meas.append(row)
    i+=1

input_file.close()
strip_15_meas = sp.array(strip_15_meas)

strip_19_meas = []

input_file = open(measurements_19, 'r+')
tar = csv.reader(input_file, delimiter=",")

i=0
for row in tar:
    if i!=0:
        row = map(float, row)
        row[0] = float(row[0])
##        row[1] = float(row[1])*length 
##        row[1] = float(row[1])/0.003
##        row[3] = float(row[3])/0.003
        strip_19_meas.append(row)
    i+=1

input_file.close()
strip_19_meas = sp.array(strip_19_meas)

strip_cu_meas = []

input_file = open(measurements_cu_pipe, 'r+')
tar = csv.reader(input_file, delimiter=",")

i=0
for row in tar:
    if i!=0:
        row = map(float, row)
##        row[1] = float(row[1])/0.003
##        row[3] = float(row[3])/0.003
        strip_cu_meas.append(row)
    i+=1

input_file.close()
strip_cu_meas = sp.array(strip_cu_meas)

measStepOut = []

input_file = open(measurementsStepOut, 'r+')
tar = csv.reader(input_file, delimiter=",")

i=0
for row in tar:
    if i!=0:
        row = map(float, row)
##        row[1] = float(row[1])/0.003
##        row[3] = float(row[3])/0.003
        measStepOut.append(row)
    i+=1

input_file.close()
measStepOut = sp.array(measStepOut)

measurement_repository = []
for data_file in updated_file_list:
    input_file = open(data_file, 'r+')
    tar = input_file.readlines()
    temp = []

    for row in tar:
            row = row.split(',')
            row = map(float, row)
##            row[0] = float(row[0])/10**6
            row[1] = float(row[1])*length
            temp.append(row)


    input_file.close()
    measurement_repository.append(temp)



    
##
##
##for i in range(20,4000, 20):
##    pl.axvline(i, 0, 200)
##pl.loglog()
##pl.semilogy()
##pl.plot(data_without_metalisation[:,0], data_without_metalisation[:, 1], 'k-', label= "No Metalisation")
pl.plot(data_screen_conductors_15[:,0], data_screen_conductors_15[:, 1], 'm-', label= "15 Conductors, together, open slots")
##pl.plot(data_screen_conductors_15[:,0], data_screen_conductors_15[:, 3], 'b--', label= "15 Conductive Strips $\Im{}m(Z)$")
##pl.plot(data_19_together[:,0], abs(data_19_together[:, 1]), 'g-', label= "19 Conductors, together, open slots")
##pl.plot(data_screen_conductors_24[:,0], abs(data_screen_conductors_24[:, 1]), 'k-', label= "24 Conductors, together, open slots")
##pl.plot(data_screen_conductors_24[:,0], (data_screen_conductors_24[:, 3]), 'r-', label= "24 Conductive Strips $\Im{}m(Z)$")
##pl.plot(data_screen_conductors_15_long_9_short[:,0], data_screen_conductors_15_long_9_short[:, 1], 'r--', label= "15 long, 9 short")
##pl.plot(data_screen_conductors_15_2hv[:,0], data_screen_conductors_15_2hv[:, 1], 'y-', label= "17 Conductors, 2 near HV plate")
##pl.plot(data_screen_conductors_15_4hv[:,0], data_screen_conductors_15_4hv[:, 1], 'b-', label= "19 Conductors, 4 near HV plate")
##pl.plot(data_screen_conductors_15_5hv[:,0], data_screen_conductors_15_5hv[:, 1], 'r-', label= "20 Conductors, 5 near HV plate")

##pl.plot(data_screen_conductors_none[:,0], data_screen_conductors_none[:, 1], 'b-', label= "No Conductive Strips")
##pl.plot(data_no_damping_ferrites[:,0], abs(data_no_damping_ferrites[:, 1]), 'b-', label= "No Damping Ferrites")
##pl.plot(data_no_screen[:,0], data_no_screen[:, 1], 'g-', label= "No Screen")
##pl.plot(data_alt_screen_1[:,0], abs(data_alt_screen_1[:, 1]), 'b-', label= "24 Conductors, Alternative Design")
##pl.plot(data_alt_screen_embedded_cerr[:,0], abs(data_alt_screen_embedded_cerr[:, 1]), 'r-', label= "Embedded in Ceramic $\Re{}e(Z)$")
##pl.plot(data_alt_screen_embedded_cerr_sec[:,0], abs(data_alt_screen_embedded_cerr_sec[:, 1]), 'r-', label= "24 Conductors, enclosed slots")
##pl.plot(data_alt_screen_embedded_cerr_sec[:,0], abs(data_alt_screen_embedded_cerr_sec[:, 2]), 'r--', label= "Embedded in Ceramic Sec $\Im{}m(Z)$")
##pl.plot(data_alt_screen_no_cerr[:,0], abs(data_alt_screen_no_cerr[:, 1]), 'b-', label= "Embedded no Ceramic $\Re{}e(Z)$")
##pl.plot(data_alt_screen_thick_cerr[:,0], abs(data_alt_screen_thick_cerr[:, 1]), 'g-', label= "24 Conductors, open slots, Thick Ceramic (5.5mm) $\Re{}e(Z)$")
##pl.plot(data_v_screen_24[:,0], abs(data_v_screen_24[:, 1]), 'm-', label= "24 Conductors, open slots a = 8.6mm $\Re{}e(Z)$")
##pl.plot(data_shortened_24[:,0], abs(data_shortened_24[:, 1]), 'm-', label= "24 Conductors, open slots, Tapered 2mm per cond")
##pl.plot(data_shortened_15[:,0], data_shortened_15[:, 1], 'k-', label= "15 Conductors, open slots, Tapered 2mm per cond")
##pl.plot(data_17_together[:,0], abs(data_17_together[:, 1]), 'y-', label= "17 Conductors, together, open slots a = 2mm $\Re{}e(Z)$")
##pl.plot(data_17_together[:,0], data_17_together[:, 1], 'k-', label= "17 Conductors, together, open slots a = 2mm $\Re{}e(Z)$")

##pl.plot(data_19_together[:,0], data_19_together[:, 1], 'k-', label= "19 Conductors, together, open slots a = 2mm $\Re{}e(Z)$")
##pl.plot(data_enclosed[:,0], data_enclosed[:, 1], 'm-', label= "24 conductors, enclosed slots $\Re{}e(Z)$")
##pl.plot(data_19_alternating[:,0], data_19_alternating[:, 1], 'b-', label= "19 conductors, open slots, Alternating 10mm per cond")
##pl.plot(data_24_alternating[:,0], abs(data_24_alternating[:, 1]), 'r-', label= "24 conductors, open slots, Alternating 10mm per cond")
##pl.plot(data_24_alternating_v[:,0], abs(data_24_alternating_v[:, 1]), 'k--', label= "24 conductors, open slots, Alternating and tapered")
##pl.plot(data_24_alternating_step_1mm[:,0], abs(data_24_alternating_step_1mm[:, 1]), 'm-', label= "24 conductors, open slots b = 10mm, step=1mm $\Re{}e(Z)$")
pl.plot(data_24_alternating_step_5mm[:,0], abs(data_24_alternating_step_5mm[:, 1]), 'b-', label= "24 conductors, open slots b = 20mm, step=5mm $\Re{}e(Z)$")
pl.plot(data_24_alternating_step_2mm_off[:,0], abs(data_24_alternating_step_2mm_off[:, 1]), 'm-', label= "24 conductors, open slots b = 10mm, step=2mm offset $\Re{}e(Z)$")
pl.plot(data_24_alternating_step_2mm_off_v[:,0], abs(data_24_alternating_step_2mm_off_v[:, 1]), 'b--', label= "24 conductors, open slots b = 10mm, step=2mm offset V $\Re{}e(Z)$")
pl.plot(data_24_alternating_step_2mm_off_v_overlap[:,0], abs(data_24_alternating_step_2mm_off_v_overlap[:, 1]), 'k--', label= "24 conductors, open slots b = 10mm, step=2mm offset V overlap $\Re{}e(Z)$")
pl.plot(data_24_alternating_step_2mm_off_v_overlap_10mm[:,0], abs(data_24_alternating_step_2mm_off_v_overlap_10mm[:, 1]), 'k--', label= "24 conductors, open slots a=3mm, step=2mm offset V overlap 10mm $\Re{}e(Z)$")
##pl.plot(data_24_alternating_step_2mm_off_v_overlap_20mm[:,0], abs(data_24_alternating_step_2mm_off_v_overlap_20mm[:, 1]), 'r--', label= "24 conductors, open slots, alternating and tapered, with external pipe")
##pl.plot(stepOutCond[:,0], stepOutCond[:, 1], 'k-', label= "MKI Final Design")
##pl.plot(datalargeInDiam[:,0], datalargeInDiam[:, 1], 'k-', label= "Inner Diameter 61mm Final Design")
##pl.plot(datalargeInDiam15m[:,0], datalargeInDiam15m[:, 1], 'r-', label= "Inner Diameter 61mm Final Design")
##pl.plot(dataCerrVac[:,0], dataCerrVac[:, 1], 'g-', label= "Replacing Ceramic with Vacuum")


##pl.semilogy()
##pl.plot(strip_24_meas[:,0]/10**3, strip_24_meas[:,1], 'r-', markersize=16.0,label = "24 Conductors, open slots, measurements $\Re{}e(Z)$")                    # plot fitted curve
##pl.plot(strip_15_meas[:,0]/10**3, strip_15_meas[:,1], 'gx', markersize=12.0,label = "15 Conductors, open slots, measurements$")                    # plot fitted curve
##pl.plot(strip_19_meas[:,0]/10**3, strip_19_meas[:,1], 'r-', markersize=12.0,label = "19 Conductors, open slots, measurements$")                    # plot fitted curve
##pl.plot(strip_cu_meas[:,0]/10**3, strip_cu_meas[:,1], 'b-', markersize=16.0,label = "NEG-Coated Cu Pipe, measurements $\Re{}e(Z)$")                    # plot fitted curve
##pl.plot(measStepOut[:,0]/10**3, measStepOut[:,1], 'b-', markersize=16.0,label = "24 Conductors, new design, open slots, measurements$")                    # plot fitted curve
##
####pl.plot(theory[:,0], theory[:,1], 'g-', markersize=8.0,label = "Real Longitudinal Impedance - Theory")                    # plot fitted curve
####pl.plot(theory[:,0], theory[:,2], 'g--', markersize=8.0,label = "Imaginary Longitudinal Impedance - Theory")                    # plot fitted curve
mark = 1
des_file = measure_stash_path+'mki29'+'-impedance-results.csv'
while updated_file_list[mark]!=des_file:
    mark+=1

measurement = sp.array(measurement_repository[mark])

##pl.plot(measurement[:,0]/10**3, measurement[:,1], 'r-',markersize=12.0, label="Measurement 24 staggered conductors")


pl.grid(linestyle="--", which = "major")
pl.xlabel("Frequency (GHz)", fontsize = 16)                  #Label axes
pl.ylabel("$\Re{}e (Z_{\parallel})$ $(\Omega)$", fontsize = 16)
##pl.ylabel("$\Im{}m (Z_{\parallel})$ $(\Omega)$",fontsize = 16)
##pl.title("", fontsize = 16)
pl.legend(loc="upper right")
pl.axis([0,2.5,0,200])
##pl.savefig(directory+"test"+".pdf")                  # Save fig for film
pl.show()
pl.clf()   
