import scipy as sp
import pylab as pl
import time, csv, sys, os
import matplotlib.pyplot as plt

########################## Imported Profile ##################################

start = time.time()

#######   SPS parameters for LHC type beam  #######

C = 299792458.0
circ=6911.0
n_bunches = 144
possible_bunches = 144
p_bunch = 1.15*10**11
Q_part = 1.6*10**-19
f_rev = 2*10**7

def gauss_prof(dist, sigma, order):
    return sp.e**(-order*(dist/sigma)**2)

def para_prof(dist, bunch_length):
    return 1-(2*dist/bunch_length)**2

def cos_prof(dist, bunch_length):
    return sp.cos(sp.pi/bunch_length*dist)**2

I_b = p_bunch*Q_part*f_rev
power_tot = I_b**2*n_bunches/possible_bunches

print I_b, power_tot

##top_directory = "E:/PhD/1st_Year_09-10/Data/UA9_goniometer/impedances/"
##measure_stash_path = "E:/PhD/1st_Year_09-10/Data/UA9_goniometer/impedances/"
top_directory = "/media/CORSAIR/impedances/"

in_data = top_directory+"in-ang-impedance/longitudinal-impedance.csv"
out_data = top_directory+"out-ang-impedance/longitudinal-impedance.csv"

data_in_ua9_long = []

input_file = open(in_data, 'r+')
tar = csv.reader(input_file, delimiter=",")

i=0
for row in tar:
    if i!=0:
        row = map(float, row)
        data_in_ua9_long.append(row)
    i+=1

input_file.close()
data_in_ua9_long = sp.array(data_in_ua9_long)


data_out_ua9_long = []

input_file = open(out_data, 'r+')
tar = csv.reader(input_file, delimiter=",")

i=0
for row in tar:
    if i!=0:
        row = map(float, row)
        data_out_ua9_long.append(row)
    i+=1

input_file.close()
data_out_ua9_long = sp.array(data_out_ua9_long)



##pl.semilogy()
##pl.plot(data_in_ua9_long[:,0], data_in_ua9_long[:, 1], 'r-', label= "Parked in Position $\Re{}e(Z$")
##pl.plot(data_in_ua9_long[:,0], data_in_ua9_long[:, 3], 'r--', label= "Parked in Position $\Im{}m(Z)$")
pl.plot(data_out_ua9_long[:,0], data_out_ua9_long[:, 1], 'b-', label= "Parked out Position $\Re{}e(Z)$")
##pl.plot(data_out_ua9_long[:,0], data_out_ua9_long[:, 3], 'b--', label= "Parked out Position $\Im{}m(Z)$")


pl.grid(linestyle="--", which = "major")
pl.xlabel("Frequency (GHz)",fontsize = 16)                  #Label axes
pl.ylabel("Real Longitudinal Impedance $(\Omega)$",fontsize = 16)
##pl.title("", fontsize = 16)
pl.legend(loc="upper left")
##pl.axis([0,2000,-1000,3500])
##pl.savefig(directory+"test"+".pdf")                  # Save fig for film
##pl.show()
pl.clf()   


in_data = top_directory+"in-ang-impedance/vertical-dipolar-impedance.csv"
out_data = top_directory+"out-ang-impedance/vertical-dipolar-impedance.csv"

data_in_ua9_vert = []

input_file = open(in_data, 'r+')
tar = csv.reader(input_file, delimiter="\n")

i=0
for row in tar:
    if i!=0:
        row = map(float, row[0].split())
        data_in_ua9_vert.append(row)
    i+=1

input_file.close()
data_in_ua9_vert = sp.array(data_in_ua9_vert)


data_out_ua9_vert = []

input_file = open(out_data, 'r+')
tar = csv.reader(input_file, delimiter="\n")

i=0
for row in tar:
    if i!=0:
        row = map(float, row[0].split())
        data_out_ua9_vert.append(row)
    i+=1

input_file.close()
data_out_ua9_vert = sp.array(data_out_ua9_vert)


##pl.semilogy()
pl.plot(data_in_ua9_vert[:,0], data_in_ua9_vert[:, 1], 'r-', label= "Parked in Position $\Re{}e(Z$")
pl.plot(data_in_ua9_vert[:,0], data_in_ua9_vert[:, 3], 'r--', label= "Parked in Position $\Im{}m(Z)$")
pl.plot(data_out_ua9_vert[:,0], data_out_ua9_vert[:, 1], 'b-', label= "Parked out Position $\Re{}e(Z)$")
pl.plot(data_out_ua9_vert[:,0], data_out_ua9_vert[:, 3], 'b--', label= "Parked out Position $\Im{}m(Z)$")


pl.grid(linestyle="--", which = "major")
pl.xlabel("Frequency (GHz)",fontsize = 16)                  #Label axes
pl.ylabel("Vertical Impedance $(\Omega)$",fontsize = 16)
##pl.title("", fontsize = 16)
pl.legend(loc="lower left")
##pl.axis([0,2000,-1000,3500])
##pl.savefig(directory+"test"+".pdf")                  # Save fig for film
##pl.show()
pl.clf()   



in_data = top_directory+"in-ang-impedance/horizontal-dipolar-impedance.csv"
out_data = top_directory+"out-ang-impedance/horizontal-dipolar-impedance.csv"

data_in_ua9_horz = []

input_file = open(in_data, 'r+')
tar = csv.reader(input_file, delimiter="\n")

i=0
for row in tar:
    if i!=0:
        row = map(float, row[0].split())
        data_in_ua9_horz.append(row)
    i+=1

input_file.close()
data_in_ua9_horz = sp.array(data_in_ua9_horz)


data_out_ua9_horz = []

input_file = open(out_data, 'r+')
tar = csv.reader(input_file, delimiter="\n")

i=0
for row in tar:
    if i!=0:
        row = map(float, row[0].split())
        data_out_ua9_horz.append(row)
    i+=1

input_file.close()
data_out_ua9_horz = sp.array(data_out_ua9_horz)


##pl.semilogy()
pl.plot(data_in_ua9_horz[:,0], data_in_ua9_horz[:, 1], 'r-', label= "Parked in Position $\Re{}e(Z$")
pl.plot(data_in_ua9_horz[:,0], data_in_ua9_horz[:, 3], 'r--', label= "Parked in Position $\Im{}m(Z)$")
pl.plot(data_out_ua9_horz[:,0], data_out_ua9_horz[:, 1], 'b-', label= "Parked out Position $\Re{}e(Z)$")
pl.plot(data_out_ua9_horz[:,0], data_out_ua9_horz[:, 3], 'b--', label= "Parked out Position $\Im{}m(Z)$")


pl.grid(linestyle="--", which = "major")
pl.xlabel("Frequency (GHz)",fontsize = 16)                  #Label axes
pl.ylabel("Horizontal Impedance $(\Omega)$",fontsize = 16)
##pl.title("", fontsize = 16)
pl.legend(loc="upper left")
##pl.axis([0,2000,-1000,3500])
##pl.savefig(directory+"test"+".pdf")                  # Save fig for film
##pl.show()
pl.clf()   

mark_length = 1.1*10**-9
time_domain_parabolic = []
time_domain_cos = []
time_domain_gauss = []
time_domain_gauss_trunc = []
data_points = 10000.0
sample_rate = 0.1/(mark_length/data_points)
bunch_length = 4.6*10**-9

## For 50ns spacing 22.726
if f_rev == 2*10**7:
    t=pl.r_[-22.726*mark_length:22.726*mark_length:1/sample_rate]
    n_bunches = 144

## For 25ns spacing
elif f_rev == 4*10**7:
    t=pl.r_[-11.357*mark_length:11.357*mark_length:1/sample_rate]
    n_bunch = 144
    ##print len(t)
omega = sample_rate

for i in t:
    if abs(i)<=bunch_length/2:
        time_domain_parabolic.append(para_prof(i,bunch_length))           ## Parabolic bunch profile
        time_domain_cos.append(cos_prof(i,bunch_length))  ## cos^2 profile
    else:
        time_domain_parabolic.append(0)
        time_domain_cos.append(0)
    time_domain_gauss.append(gauss_prof(i,bunch_length,30))           ## gaussian profile

        
time_domain_parabolic = sp.array(time_domain_parabolic)
time_domain_cos = sp.array(time_domain_cos)
time_domain_gauss = sp.array(time_domain_gauss)
N=len(t)
freq_domain_parabolic=sp.fft(time_domain_parabolic)
freq_domain_cos=sp.fft(time_domain_cos)
freq_domain_gauss=sp.fft(time_domain_gauss)
f = sample_rate*sp.r_[0:(N/2)]/N
n=len(f)

freq_domain_parabolic = freq_domain_parabolic[0:n]/freq_domain_parabolic[0]
freq_domain_cos = freq_domain_cos[0:n]/freq_domain_cos[0]
freq_domain_gauss = freq_domain_gauss[0:n]/freq_domain_gauss[0]

###### To dB scale    ########

freq_domain_parabolic = sp.real(20*sp.log(freq_domain_parabolic))
freq_domain_cos = sp.real(20*sp.log(freq_domain_cos))
freq_domain_gauss = sp.real(20*sp.log(freq_domain_gauss))

fig = plt.figure()
ax1 = fig.add_subplot(111)
pl.plot(f[:100]/10**9, freq_domain_parabolic[:100], 'b-', label="Parabolic")
pl.plot(f[:100]/10**9, freq_domain_cos[:100], 'r-', label="Cos$^{2}$")
pl.plot(f[:100]/10**9, freq_domain_gauss[:100], 'g-', label="Gaussian")
ax1.set_xlabel("Frequency (GHz)", fontsize = "16")
ax1.set_ylabel("S (dB)", fontsize = "16")
ax1.legend(loc = "upper right")
ax1.axis([0,2,-160,0])

ax2 = ax1.twinx()
pl.plot(data_in_ua9_long[:,0], abs(data_in_ua9_long[:,1]), 'k-', label="Parked In")
pl.plot(data_out_ua9_long[:,0], abs(data_out_ua9_long[:,1]), 'm-', label = "Parked Out")
ax2.set_ylabel("Real Longitudinal Impedance ($\Omega$)", fontsize = "16")
ax2.legend(loc="center left")
pl.show()
pl.clf()

heating_in_para = 0.0
heating_in_cos = 0.0
heating_in_gauss = 0.0
heating_out_para = 0.0
heating_out_cos = 0.0
heating_out_gauss = 0.0
heating_cumulative_out_cos = []

for i in range(0,len(f)):
    j = 0
    if f[i] < 2.1*10**9:
        while j<len(data_in_ua9_long) :
            if f[i] == 0.0:
                pass
            elif abs(f[i]/10**9-data_in_ua9_long[j,0])<0.0005:
                heating_in_para+=2*power_tot*abs(freq_domain_parabolic[i])**2*abs(data_in_ua9_long[j,1])
                heating_out_para+=2*power_tot*abs(freq_domain_parabolic[i])**2*abs(data_out_ua9_long[j,1])
                heating_in_cos+=2*power_tot*abs(freq_domain_cos[i])**2*abs(data_in_ua9_long[j,1])
                heating_out_cos+=2*power_tot*abs(freq_domain_cos[i])**2*abs(data_out_ua9_long[j,1])
                heating_cumulative_out_cos.append(2*power_tot*abs(freq_domain_cos[i])**2*abs(data_out_ua9_long[j,1]))
                heating_in_gauss+=2*power_tot*abs(freq_domain_gauss[i])**2*abs(data_in_ua9_long[j,1])
                heating_out_gauss+=2*power_tot*abs(freq_domain_gauss[i])**2*abs(data_out_ua9_long[j,1])
                pass
            j+=1
            

##print heating_in_para 
##print heating_in_cos 
##print heating_in_gauss
##print heating_out_para
##print heating_out_cos 
##print heating_out_gauss                

pl.plot(f[:100], heating_cumulative_out_cos)
##pl.show()
pl.clf()
