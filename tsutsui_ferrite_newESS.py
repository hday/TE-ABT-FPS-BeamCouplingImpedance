import sympy.mpmath as mpmath
import scipy as sp
import numpy, csv, time
from matplotlib import pyplot
import numbers
import pylab as pl
import scipy.integrate as inte
##import pyfftw

Z0 = 377.0
a = 0.05
b = 0.06
d = b+0.02
length = 1
disp = 1
e0 = 8.85*10**-12
eprime = 12.0
rho = 10**6
muprime = 460.0
tau = 1.0/(20.0*10**6)
er=12.0
mu0 = 4*sp.pi*10**-7

def k(freq):
    return 2*sp.pi*freq/(3*10**8)

def mur(freq):
    return (1 + (muprime/(1+1j*tau*freq)))

def kxn(n):
    return ((2*n + 1)*sp.pi)/(2*a)

def kyn(n,freq):
    return ((er*mur(freq) - 1)*k(freq)**2 - kxn(n)**2)**0.5

def sh(n):
    return mpmath.sinh(kxn(n)*b)

def ch(n):
    return mpmath.cosh(kxn(n)*b)

def tn(n,freq):
    return mpmath.tan(kyn(n,freq)*(b-d))

def ct(n,freq):
    return mpmath.cot(kyn(n,freq)*(b-d))

def FX(n,freq):
    return (kxn(n)/k(freq))*((1+(mur(freq)*er))*sh(n)*ch(n))

def FY(n,freq):
    return (kyn(n,freq)/k(freq))*(mur(freq)*(sh(n)**2)*tn(n,freq) - er*(ch(n)**2)*ct(n,freq))

def FY_2(n,freq):
    return (kyn(n,freq)/k(freq))*(mur(freq)*(ch(n)**2)*tn(n,freq) - er*(sh(n)**2)*ct(n,freq))

def ferrite_long(freq):
    return (1j*Z0)/(2*a) * (mpmath.nsum(lambda n: (((FX(n,freq) +FY(n,freq))/(er*mur(freq)-1)) - ((k(freq)*sh(n)*ch(n))/kxn(n)))**(-1), [0,10]))

def ferrite_dip_horz(freq):
    return (1j*Z0)/(2*a) * mpmath.nsum(lambda n: (((kxn(n)**2)/k(freq))*(((FX(n,freq) + FY(n,freq))/(er*mur(freq)-1) - (k(freq)*sh(n)*ch(n)/kxn(n))))**-1), [0, 10])

def ferrite_dip_vert(freq):
    return (1j*Z0)/(2*a) * mpmath.nsum(lambda n: (((kxn(n)**2)/k(freq))*(((FX(n,freq) + FY_2(n,freq))/(er*mur(freq)-1) - (k(freq)*sh(n)*ch(n)/kxn(n))))**-1), [0, 10])

def ferrite_quad_horz(freq):
    return (-1j*Z0)/(2*a) * mpmath.nsum(lambda n: (((kxn(n)**2)/k(freq))*(((FX(n,freq) + FY(n,freq))/(er*mur(freq)-1) - (k(freq)*sh(n)*ch(n)/kxn(n))))**-1), [0, 10])

def ferrite_quad_vert(freq):
    return (1j*Z0)/(2*a) * mpmath.nsum(lambda n: (((kxn(n)**2)/k(freq))*(((FX(n,freq) + FY(n,freq))/(er*mur(freq)-1) - (k(freq)*sh(n)*ch(n)/kxn(n))))**-1), [0, 10])

def heatCalc(Z, distribution, bLength, Ib):
    if distribution == "gaussian":
        dist = lambda bLength: sp.e**(2*sp.pi*Z[0]*bLength)**2
    elif distribution == "cos":
        dist = lambda bLength: sp.sin(2*sp.pi*Z[0]*bLength)/(2*sp.pi*Z[0]*bLength*(1-Z[0]*bLength)**2)**2
    else:
        print "Not a supported distrubution"
    return 2*Ib**2*dist(bLength)**2*Z[1]

def gaussProfFreq(freq, bLength):
    sigma = bLength/(4.0*2.0*sp.log(2.0))
    return sp.e**(-(2.0*sp.pi*freq*sigma)**2/2)

def cosProfFreq(freq, bunLength):
    return (sp.sin(sp.pi*freq*bunLength)/(sp.pi*freq*bunLength*(1-(freq*bunLength)**2)))**2

def paraProfFreq(freq, bunLength):                                     
    return (sp.sin(sp.pi*freq*bunLength)*((2*sp.pi*freq)**2*bunLength**2)+(8*sp.cos(sp.pi*freq*bunLength)*2*sp.pi*bunLength))/(2*sp.pi*freq)**2/bunLength**2
##    return sp.sin(sp.pi*freq*bunLength)/(sp.pi*freq)

##def gaussProfTime(dist, sigma, order):
####    return sp.e**(-(dist/sigma)**2/2)/(2*sp.pi)**0.5/sigma
##    return sp.e**(-order*(dist/sigma)**2/2)
####    return sp.e**(-(dist/sigma)**2/2)

def gaussProfTime(dist, bLength, order):
    sigma = bLength/(4.0*2.0*sp.log(2.0))
##    return sp.e**(-(dist/sigma)**2/2)/(2*sp.pi)**0.5/sigma
    return sp.e**(-(dist/sigma)**2/2)
##    return sp.e**(-(dist/sigma)**2/2)

def paraProfTime(dist, bunch_length):
    if bunch_length/2 <= dist:
        return 0
    elif bunch_length/2 >= dist:
        return 1-(2*dist/bunch_length)**2
    else:
        return 0

def cosProfTime(dist, bunch_length):
    if bunch_length/2 <= dist:
        return 0
    if bunch_length/2 >= dist:
##        print dist,bunch_length
        return sp.cos(sp.pi/bunch_length*dist)**2
    else:
        return 0

def linToLog(data):
    return 20*sp.log10(data)
                              
def logToLin(data):
    return 10**(data/20)

start = time.time()


temp_real = []
temp_imag = []


freqList = sp.linspace(10**3,20000*10**9, 20000)
freqListPlot = sp.linspace(10**3,20*10**9, 20000)


freq_list = []
for i in range(0,12,1):
    for j in range(1,100,1):
        freq_list.append(float((j/10.0)*10.0**i))

freq_list = list(set(freq_list))
freq_list.sort()
freq_list=sp.array(freq_list)
##freq_list=sp.logspace(1,10**10,10000)
impLong = []
impLongPlot = []
    
for i in range(0,len(freqList)):
    impLong.append([float(ferrite_long(freqList[i]).real), float(ferrite_long(freqList[i]).imag), ferrite_long(freqList[i])])
    impLongPlot.append([float(ferrite_long(freqListPlot[i]).real), float(ferrite_long(freqListPlot[i]).imag), ferrite_long(freqListPlot[i])])
    temp_real.append(mur(freqList[i]).real)
    temp_imag.append(mur(freqList[i]).imag)

impLong=sp.array(impLong)
impLongPlot=sp.array(impLongPlot)
temp_real = sp.array(temp_real)
temp_imag = sp.array(temp_imag)

pl.semilogx()
pl.plot(freqList/10**9, temp_real,"k-",label="$\mu_{r}^{'}$")
pl.plot(freqList/10**9, -temp_imag,"r--",label="$\mu_{r}^{''}$")
pl.xlabel("Frequency (GHz)", fontsize=18.0)
pl.ylabel("$\mu_{r}$", fontsize=18.0)
pl.axis([10**-3,10,0,500])
pl.legend(loc="upper right", prop={'size':18})
##pl.show()
pl.clf()

saveDir = "E:/PhD/1st_Year_09-10/Software/"

try:
    os.mkdir(saveDir+"ESSImpTsutsui")
except:
    pass

bunLen = 10*10**-12
##### ESS Spec

bunSpacing = 2.841*10.0**-9
trainSpacing = 8*bunSpacing
bunInTrain = 72
trainsInMachine = 4
blankBunSpaces = 562
beamCur = 62.5*10.0**-3
print 43450

bunProfGauss = cosProfFreq(freqList, bunLen)

freqRes = 1/(freqList[1]-freqList[0])
countF = len(freqList)
timeList = freqRes*sp.r_[0:(countF/2)]/countF

##pl.loglog()
pl.semilogx()
fig = pl.figure(1)
ax1=fig.add_subplot(111)
pl.plot(freqList/10**9, linToLog(bunProfGauss/sp.amax(bunProfGauss)))
pl.ylim(-90,0)
ax2=pl.twinx()
ax2.plot(freqList/10**9,impLong[:,0],"k-",)
pl.xlim(0,100)
##pl.show()
pl.clf()

peakPow = inte.simps(impLong[:,0]*beamCur**2)
fftTar = sp.transpose(impLong[:,2])

print peakPow
print peakPow*(14)
fftImpReal=sp.ifft(fftTar)
fftImpReal=fftImpReal[0:countF/2]
##print fftImpReal
##print sp.sum(sp.fft(impLong[:,0]))

##output = open('./ESSImpTsutsui/mki-long.csv', 'w+')
##for i in range(0,len(temp_real)):
##              output.write(str(freq_list[i])+","+str(temp_real[i])+','+str(temp_imag[i])+'\n')
##
##output.close()
##pl.clf()
##pl.loglog()

##pl.semilogx()
pl.plot(freqListPlot/10**9,impLongPlot[:,0], color='black')
pl.plot(freqListPlot/10**9,impLongPlot[:,1], color='red')
pl.xlabel("Frequency (GHz)")                  #Label axes
pl.ylabel("Impedance ($\Omega/m$)")
pl.xlim(0, 2.5)
pl.show()
pl.clf()
pl.savefig(saveDir+'ESSImpTsutsui/mki-long.png')

##print len(timeList), len(fftImpReal)
##pl.semilogx()
pl.plot(timeList, fftImpReal)
pl.show()
pl.clf()

##x= []
##temp_real = []
##temp_imag = []
##
##for i in freq_list:
##    temp_real.append(ferrite_dip_vert(i).real*length*disp)
##    temp_imag.append(ferrite_dip_vert(i).imag*length*disp)


##output = open('./ESSImpTsutsui/mki_vert_dip_data.csv', 'w+')
##for i in range(0,len(temp_real)):
##              output.write(str(freq_list[i])+","+str(temp_real[i])+','+str(temp_imag[i])+'\n')
##output.close()


##pl.clf()
##pl.semilogx()
##pl.plot(freq_list,temp_real, color='black')
##pl.plot(freq_list,temp_imag, color='red')
##pl.xlabel("Frequency (MHz)")                  #Label axes
##pl.ylabel("Impedance (Ohms/m/m)")
##pl.savefig(saveDir+'ESSImpTsutsui/mki_dip_vert_theo.png')
##
##x= []
##temp_real = []
##temp_imag = []
##
##for i in freq_list:
##    temp_real.append(ferrite_dip_horz(i).real*length*disp)
##    temp_imag.append(ferrite_dip_horz(i).imag*length*disp)


##output = open('./ESSImpTsutsui/mki_horz_dip_data.csv', 'w+')
##for i in range(0,len(temp_real)):
##              output.write(str(freq_list[i])+","+str(temp_real[i])+','+str(temp_imag[i])+'\n')
##output.close()

##pl.clf()
##pl.semilogx()
##pl.plot(freq_list,temp_real, color='black')
##pl.plot(freq_list,temp_imag, color='red')
##pl.xlabel("Frequency (MHz)")                  #Label axes
##pl.ylabel("Impedance (Ohms/m/m)")
##pl.savefig(saveDir+'ESSImpTsutsui/mki_dip_horz_theo.png')
##
##x= []
##temp_real = []
##temp_imag = []
##
##for i in freq_list:
##    temp_real.append(ferrite_quad_horz(i).real*length*disp)
##    temp_imag.append(ferrite_quad_horz(i).imag*length*disp)
##

##output = open('./ESSImpTsutsui/mki_horz_quad_data.csv', 'w+')
##for i in range(0,len(temp_real)):
##              output.write(str(freq_list[i])+","+str(temp_real[i])+','+str(temp_imag[i])+'\n')
##output.close()
##
##pl.clf()
##pl.semilogx()
##pl.plot(freq_list,temp_real, color='black')
##pl.plot(freq_list,temp_imag, color='red')
##pl.xlabel("Frequency (MHz)")                  #Label axes
##pl.ylabel("Impedance (Ohms/m/m)")
##pl.savefig(saveDir+'ESSImpTsutsui/mki_quad_horz_theo.png')
##
##x= []
##temp_real = []
##temp_imag = []
##
##for i in freq_list:
##    temp_real.append(ferrite_quad_vert(i).real*length*disp)
##    temp_imag.append(ferrite_quad_vert(i).imag*length*disp)
##

##output = open('./ESSImpTsutsui/mki_vert_quad_data.csv', 'w+')
##for i in range(0,len(temp_real)):
##              output.write(str(freq_list[i])+","+str(temp_real[i])+','+str(temp_imag[i])+'\n')
##output.close()

##
##pl.clf()
##pl.semilogx()
##pl.plot(freq_list,temp_real, color='black')
##pl.plot(freq_list,temp_imag, color='red')
##pl.xlabel("Frequency (MHz)")                  #Label axes
##pl.ylabel("Impedance (Ohms/m/m)")
##pl.savefig(saveDir+'ESSImpTsutsui/mki_quad_vert_theo.png')
##
##x= []
##temp_real = []
##temp_imag = []
##
##for i in freq_list:
##    temp_real.append((ferrite_dip_vert(i)+ferrite_quad_vert(i)).real*length*disp)
##    temp_imag.append((ferrite_dip_vert(i)+ferrite_quad_vert(i)).imag*length*disp)

##output = open('./ESSImpTsutsui/mki_vert_total_data.csv', 'w+')
##for i in range(0,len(temp_real)):
##              output.write(str(freq_list[i])+","+str(temp_real[i])+','+str(temp_imag[i])+'\n')
##
##output.close()

##
##
##pl.clf()
##pl.semilogx()
##pl.plot(freq_list,temp_real, color='black')
##pl.plot(freq_list,temp_imag, color='red')
##pl.xlabel("Frequency (MHz)")                  #Label axes
##pl.ylabel("Impedance (Ohms/m)")
##pl.savefig(saveDir+'ESSImpTsutsui/mki_total_vert.png')
##
##x= []
##temp_real = []
##temp_imag = []
##
##for i in freq_list:
##    temp_real.append((ferrite_dip_horz(i)+ferrite_quad_horz(i)).real*length*disp)
##    temp_imag.append((ferrite_dip_horz(i)+ferrite_quad_horz(i)).imag*length*disp)


##output = open('./ESSImpTsutsui/mki_horz_total_data.csv', 'w+')
##for i in range(0,len(temp_real)):
##              output.write(str(freq_list[i])+","+str(temp_real[i])+','+str(temp_imag[i])+'\n')
##
##output.close()
##pl.clf()
##pl.semilogx()
##pl.plot(freq_list,temp_real, color='black')
##pl.plot(freq_list,temp_imag, color='red')
##pl.xlabel("Frequency (MHz)")                  #Label axes
##pl.ylabel("Impedance (Ohms/m)")
##pl.savefig(saveDir+'ESSImpTsutsui/mki_total_horz.png')

print time.time() - start
