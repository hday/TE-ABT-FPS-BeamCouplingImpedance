import csv, os, sys, time
import scipy as sp
import pylab as pl
import scipy.optimize as op
import scipy.interpolate as interp

####### Define constants for calculations ##########

C = 299792458.0
Z0 = 377
lenMKP = 3.423+(2*0.035)+(2*0.005)
Zc = 320.0

######### Define importing, analysis functions #########

c=299792458.0
mu0=4*sp.pi*10.0**-7
eps0= 8.854187817*10.0**-12
dAper = 0.045
rWire = 0.0005
lWire = 3.551
lDUT = 2.45
resCopper = 17.24*10.0**-9
resOuter = 1/(3.3*10.0**7)
ZChar = 60*sp.log(dAper/rWire)

def importDatSimMeas(file_tar):
    tar = open(file_tar, 'r+')
    tempDat = tar.readlines()
    datStore = []
    for entry in tempDat[1:]:
        temp = map(float, entry.split(','))
        datStore.append(temp)

    return datStore

def analSingReSimMeas(data, freqList, lenDUT, lenTot, r_wire, apPipe):
    reStore = []
    temp = []
##    Z0 = 60*sp.log(1.27*apPipe/r_wire)
    Z0=50.0
    for i in range(0,len(data)):
        tempLin = []
        for j in range(1,len(data[0])):
            tempLin.append(-2*Z0/lenTot*sp.log(10**(data[i,j]/20))) 
        temp.append(tempLin)
    return temp

def fileImport(tarFile):
    readPart=open(tarFile, 'r+')
    datUnPro = readPart.readlines()
    readPart.close()
    freqDat=[]
    qLoaded=[]
    s21DB=[]
    for row in datUnPro[20:]:
        freqDat.append(float(row.split(",")[0]))
        qLoaded.append(float(row.split(",")[1]))
        s21DB.append(float(row.split(",")[2]))

    return sp.array(freqDat), sp.array(qLoaded), sp.array(s21DB)

def skinDepth(freq, cond):
    return (1/(sp.pi*freq*mu0*cond))**0.5

def resImpGet(targetFile):

    freqList, qTotal, transCoeff = fileImport(targetFile)

    linTransCoeff = 10.0**(transCoeff/10.0)
    coupCoeff = linTransCoeff/(1-linTransCoeff)
    qUnloaded=qTotal*(1+coupCoeff)

    measAttenuation = 8.686*sp.pi*freqList/(c*qUnloaded)
    skinD = skinDepth(freqList, 1/resCopper)
    skinCor = (rWire+skinD)/rWire

    attenCopper = skinCor*8.686*(sp.pi*eps0*resCopper*freqList)**0.5/(sp.log(dAper/rWire))*((1/rWire)+(1/dAper)*(resCopper/resOuter)**0.5)
    resLow = 4*lWire*resCopper/(sp.pi*rWire**2)
    resHigh = resLow*rWire/(4*skinD)*skinCor

    skinPipe = skinDepth(freqList, resOuter)
    zMeas = resHigh*(measAttenuation-attenCopper)/attenCopper

    return freqList, zMeas/lDUT

def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp

def sParaRead(fileTar, lenDUT, col):
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter=0.0
    last = 1e8
    freq_last = 0.0
    for row in inputData[5:]:
        row=map(float, row.split())
        phase = row[col+1]-counter
        if phase > last and (phase-last)>180 and abs(freq_last-row[0])>0.3e8:
            count+=1
            counter+=360
            phase-=360
            freq_last = row[0]
        elif phase>last and (phase-last)>180 and abs(freq_last-row[0])<0.3e8:
            phase+=360    

        s21Pec = 2*sp.pi*row[0]*lenDUT/C
        import_dat = [row[0], row[col], sp.radians(phase), -s21Pec]
        data.append(import_dat)
        last = phase

    return data


def impAnalysisSingle(fileTar, lenDUT, r_wire, r_pipe, Zc):
    ###### Analyses a S2P file for measurement of longitudinal impedance to impedance data ########
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter=0.0
    last = 1e8
    freq_last = 0.0
    for row in inputData[5:]:
        row=map(float, row.split())
        linDat = 10**(row[3]/20)
        s21Pec = 2*sp.pi*row[0]*lenDUT/C
        phase = row[4]-counter
        if phase > last and (phase-last)>180 and abs(freq_last-row[0])>0.3e8:
            count+=1
            counter+=360
            phase-=360
            freq_last = row[0]
        elif phase>last and (phase-last)>180 and abs(freq_last-row[0])<0.3e8:
            phase+=360


        import_dat = [row[0], -2*Zc*sp.log(linDat), -2*Zc*(sp.radians(phase)+s21Pec)]
##        import_dat = [row[0], -2*Zc*(sp.log(linDat)-sp.log(linDat)*(sp.radians(phase)+s21Pec)/lenDUT), -2*Zc*((sp.radians(phase)+s21Pec)+((sp.log(linDat))**2-(sp.radians(phase)+s21Pec)**2)/(2*lenDUT))]
        data.append(import_dat)
        last = phase

    return data

def impAnalysisTwo(fileTar, lenDut, r_wire, r_pipe, d_wire, Zc):
    ###### Analyses a S2P file for measurement of dipole impedance to impedance data ########
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter = 0.0
    last = 1e8

    for row in inputData[5:]:
        row=map(float, row.split())
        linDat = 10**(row[3]/20)
        s21Pec = 2*sp.pi*row[0]*lenDut/C
        phase = row[4]-counter
        if phase>last and (phase-last)>180:
            count+=1
            counter+=360
            pgase-=360
        import_dat = [row[0], -2*Zc*sp.log(linDat)*C/(2*sp.pi*row[0]*d_wire**2), -2*Zc*(sp.radians(phase)+s21Pec)*C/(2*sp.pi*row[0]*d_wire**2)]
##        import_dat = [row[0], -2*Zc*(sp.log(linDat)-sp.log(linDat)*(sp.radians(phase)+s21Pec)/lenDut)*C/(row[0]*d_wire**2), -2*Zc*((sp.radians(phase)+s21Pec)+((sp.log(linDat))**2-(sp.radians(phase)+s21Pec)**2)/(2*lenDut))*C/(row[0]*d_wire**2)]
        data.append(import_dat)
    return data

def impTransPara(dispWire, measImp, wireErr):
##### Takes an array of longitudinal measurements and returns the transverse impedance
    quadFunc = lambda p, x: p[0] + x * p[1] + x**2 * p[2]
    errFunc = lambda p, x, Z: (Z-quadFunc(p,x))

    totTrans = []
    longComp = []

    for i in range(0,len(measImp[0,:,1])):
        pinit = [1.0, -1.0, 1.0]
        ZerrRe = measImp[:,i,1]*wireErr
        pReal, pother = op.leastsq(errFunc, pinit, args=(dispWire, measImp[:,i,1]), full_output = 0)
        pinit = [1.0, -1.0, 1.0]
        ZerrIm = measImp[:,i,2]*wireErr
        pImag, pother = op.leastsq(errFunc, pinit, args=(dispWire, measImp[:,i,2]), full_output = 0)
        totTrans.append([pReal[2]*C/(2*sp.pi*measImp[0,i,0]), pImag[2]*C/(2*sp.pi*measImp[0,i,0])])
        longComp.append([pReal[0], pImag[0]])

    return longComp, totTrans

def quadImp(totalTrans, dipTrans, axis):
######## Returns the quadrupolar impedance from total transverse and dipolar data
    quadImp = []
    for i in range(0,len(totalTrans)):
        if axis == "horz":
            quadImp.append([totalTrans[i,1]+dipTrans[i,1],totalTrans[i,2]+dipTrans[i,2]])
        elif axis == "vert":
            quadImp.append([totalTrans[i,1]-dipTrans[i,1],totalTrans[i,2]-dipTrans[i,2]])
    return quadImp


def Z_bb(freq, data):
    return data[2]/complex(1, data[1]*(freq/data[0] - data[0]/freq))

def peakFitSimulations(data):
    lorentz_fit = lambda p, f: p[1]/(1 + (p[2]*(f/p[0] - p[0]/f))**2) ##### A lorentzian fitting function
    lorentz_errfunc = lambda p, f, y: (y-lorentz_fit(p, f))
    err_factor = 0.01

    peak_freq = []       

    lower_bound = 50
    upper_bound = len(data[:,0])
    x_val = data[lower_bound:upper_bound,0]
    y_val = data[lower_bound:upper_bound,1]

    ####### fit lorentzian data ###########

    current_max_y = 0.0
    current_max_x = 0
    for i in range(0, len(x_val)):
        if y_val[i] > current_max_y:
            current_max_y = y_val[i]
            current_max_x = i
        elif y_val[i] < current_max_y and i-current_max_x > 50 and y_val[current_max_x]>2:
            peak_freq.append([current_max_x, x_val[current_max_x], y_val[current_max_x]])
            current_max_y = 0
        else:
            pass
    fit_data = []
    old_upper = 100

    for res in peak_freq:
        pinit = [0.2, 10.0, 10.0]
        lower_bound = res[0]-50
        upper_bound = res[0]+50
        ###### Check to ensure no under or overrun
        if lower_bound < 0:
            lower_bound = 0
        if upper_bound > (len(data)-1):
            upper_bound = (len(data)-1)
    ##    ####### Make sure no overlap with other peaks
    ##    if lower_bound < old_upper and len(fit_data) > 0:
    ##        lower_bound = old_upper-1
        x_val = data[lower_bound:upper_bound,0]
        y_val = data[lower_bound:upper_bound,1]
        pfinal, pcov = op.leastsq(lorentz_errfunc, pinit, args = (x_val, y_val))   
        fit_data.append(pfinal)
        old_upper = upper_bound

    return fit_data, peak_freq

def peakFitMeasurements(data):
    lorentz_fit = lambda p, f: p[1]/(1 + (p[2]*(f/p[0] - p[0]/f))**2) ##### A lorentzian fitting function
    lorentz_errfunc = lambda p, f, y: (y-lorentz_fit(p, f))
    err_factor = 0.01

    peak_freq = []       

    lower_bound = 0
    upper_bound = len(data[:,0])
    x_val = data[lower_bound:upper_bound,0]
    y_val = data[lower_bound:upper_bound,1]

    ####### fit lorentzian data ###########

    current_max_y = 0.0
    current_max_x = 0
    for i in range(0, len(x_val)):
        if y_val[i] > current_max_y:
            current_max_y = y_val[i]
            current_max_x = i
        elif y_val[i] < current_max_y and i-current_max_x > 2 and y_val[current_max_x]>3:
            peak_freq.append([current_max_x, x_val[current_max_x], y_val[current_max_x]])
            current_max_y = 0
        else:
            pass
    fit_data = []
    old_upper = 100

    for res in peak_freq:
        pinit = [res[1], 4.0, data[res[0],1]]
        lower_bound = res[0]-4
        upper_bound = res[0]+3
        ###### Check to ensure no under or overrun
        if lower_bound < 0:
            lower_bound = 0
        if upper_bound > (len(data)-1):
            upper_bound = (len(data)-1)
    ##    ####### Make sure no overlap with other peaks
    ##    if lower_bound < old_upper and len(fit_data) > 0:
    ##        lower_bound = old_upper-1
        x_val = data[lower_bound:upper_bound,0]
        y_val = data[lower_bound:upper_bound,1]
        pfinal, pcov = op.leastsq(lorentz_errfunc, pinit, args = (x_val, y_val))   
        fit_data.append(pfinal)
        old_upper = upper_bound

    return fit_data, peak_freq

def gaussProf(freq, bLength):
    sigma = bLength/(4.0*2.0*sp.log(2.0))
    return sp.e**(-(2.0*sp.pi*freq*sigma)**2)

def cosProf(freq, bunLength):
    return (sp.sin(sp.pi*freq*bunLength)/(sp.pi*freq*bunLength*(1-(freq*bunLength)**2)))**2
                                                    

def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp

def peak_fit(data):
    lorentz_fit = lambda p, f: p[1]/(1 + (p[2]*(f/p[0] - p[0]/f))**2) ##### A lorentzian fitting function
    lorentz_errfunc = lambda p, f, y: (y-lorentz_fit(p, f))
    err_factor = 0.01

    peak_freq = []       

    lower_bound = 50
    upper_bound = len(data[:,0])
    x_val = data[lower_bound:upper_bound,0]
    y_val = data[lower_bound:upper_bound,1]

    ####### fit lorentzian data ###########

    current_max_y = 0.0
    current_max_x = 0
    for i in range(0, len(x_val)):
        if y_val[i] > current_max_y:
            current_max_y = y_val[i]
            current_max_x = i
        elif y_val[i] < current_max_y and i-current_max_x > 50 and y_val[current_max_x]>5:
            peak_freq.append([current_max_x, x_val[current_max_x], y_val[current_max_x]])
            current_max_y = 0
        else:
            pass
    fit_data = []
    old_upper = 100

    for res in peak_freq:
        pinit = [0.2, 10.0, 10.0]
        lower_bound = res[0]-50
        upper_bound = res[0]+50
        ###### Check to ensure no under or overrun
        if lower_bound < 0:
            lower_bound = 0
        if upper_bound > (len(data)-1):
            upper_bound = (len(data)-1)
    ##    ####### Make sure no overlap with other peaks
    ##    if lower_bound < old_upper and len(fit_data) > 0:
    ##        lower_bound = old_upper-1
        x_val = data[lower_bound:upper_bound,0]
        y_val = data[lower_bound:upper_bound,1]
        pfinal, pcov = op.leastsq(lorentz_errfunc, pinit, args = (x_val, y_val))   
        fit_data.append(pfinal)
        old_upper = upper_bound

    return fit_data, peak_freq

def heatCalc(Z, distribution, bLength, Ib):
    if distribution == "gaussian":
        dist = lambda bLength: sp.e**(2*sp.pi*Z[0]*bLength)**2
    elif distribution == "cos":
        dist = lambda bLength: sp.sin(2*sp.pi*Z[0]*bLength)/(2*sp.pi*Z[0]*bLength*(1-Z[0]*bLength)**2)**2
    else:
        print "Not a supported distrubution"
    return 2*Ib**2*dist(bLength)**2*Z[1]

def gaussProfFreq(freq, bLength):
    sigma = bLength/(4.0*2.0*sp.log(2.0))
    return sp.e**(-(2.0*sp.pi*freq*sigma)**2)

def cosProfFreq(freq, bunLength):
    return (sp.sin(sp.pi*freq*bunLength)/(sp.pi*freq*bunLength*(1-(freq*bunLength)**2)))**2

def paraProfFreq(freq, bunLength):                                     
    return (sp.sin(sp.pi*freq*bunLength)*((2*sp.pi*freq)**2*bunLength**2)+(8*sp.cos(sp.pi*freq*bunLength)*2*sp.pi*bunLength))/(2*sp.pi*freq)**2/bunLength**2
##    return sp.sin(sp.pi*freq*bunLength)/(sp.pi*freq)

def gaussProfTime(dist, sigma, order):
    return sp.e**(-order*(dist/sigma)**2)

def paraProfTime(dist, bunch_length):
    if bunch_length/2 <= dist:
        return 0
    elif bunch_length/2 >= dist:
        return 1-(2*dist/bunch_length)**2
    else:
        return 0

def cosProfTime(dist, bunch_length):
    if bunch_length/2 <= dist:
        return 0
    if bunch_length/2 >= dist:
##        print dist,bunch_length
        return sp.cos(sp.pi/bunch_length*dist)**2
    else:
        return 0

####### Import data for measurement. MKI Tank in this case #######

measurementsStepOut = sp.array(extract_dat("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/31-05-13/tankNo13/resonator-impedance-results.csv"))

resList, peakData = peakFitMeasurements(measurementsStepOut)

##print measurementsStepOut[:,0], measurementsStepOut[:,1]
SplineFitTank1 = interp.InterpolatedUnivariateSpline(measurementsStepOut[:,0], measurementsStepOut[:,1])

freqList = sp.linspace(1,2000,2000)

splineFitDataTank1 = SplineFitTank1(freqList)
freqListHeating = sp.linspace(20,2000,2000/20)
splitFitHeatingTank1 = SplineFitTank1(freqListHeating)

####### Calculation beam power spectrum using FFT of a simulated bunch train signal

f_rev = C/27000.0
nBunches = 1380
qPart = 1.6*10.0**-19
nPart=1.6*10.0**11
bCur = nPart*qPart*nBunches*f_rev
bLength=1.001*10.0**-9
testImp = [250*10**6, 10.0, 10000]
timeRes = 0.02*10.0**-9
bunRange = 50

print bCur

bunSpacing = 50*10.0**-9
trainSpacing = 150*10.0**-9
bunInTrain = 144
trainsInMachine = 10

spaceCount = ((trainsInMachine*bunInTrain*bunSpacing)+(trainSpacing*(trainsInMachine+250))+2*bunSpacing)/timeRes
ampListTime = [0 for i in range(0,int(spaceCount))]
timing = []
for i in range(0,len(ampListTime)):
    timing.append(i*timeRes)

bunLimit = int((bLength)/timeRes)

for i in range(0,trainsInMachine):
    for j in range(0,bunInTrain):
        midPoint = int(((j*bunSpacing)+(i*trainsInMachine*trainSpacing)+(i*bunInTrain*bunSpacing)+bLength)/timeRes)
##        print midPoint, bunLimit, i
        for k in range(midPoint-bunLimit, midPoint+bunLimit):
##            print k
##            ampListTime[k] = gaussProfTime(abs(k-midPoint)*timeRes, bLength, 20)
            ampListTime[k] = cosProfTime(abs(k-midPoint)*timeRes, bLength)
##            ampListTime[k] = paraProfTime(abs(k-midPoint)*timeRes, bLength)

pl.plot(timing, ampListTime)
pl.axis([0,timing[-1], -0.5,1.5])
pl.xlabel("Time (s)", fontsize=16.0)
pl.ylabel("Amplitude", fontsize=16.0)
##pl.show()
pl.clf()

N = len(timing)
f = 1/timeRes*sp.r_[0:(N/2)]/N
n= len(f)
ampListFreq = sp.fft(ampListTime)
ampListFreq = ampListFreq[0:n]/sp.amax(ampListFreq)
ampListFreqdB = 20*sp.log10(ampListFreq)

##print len(f), len(ampListFreq)

pl.plot(f/10**9, ampListFreqdB)
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("S (dB)", fontsize=16.0)
pl.axis([0,2.5,-60,0])
##pl.show()
pl.clf()

###### Calculation of power loss due to beam spectrum

count=10
cumHeatingBeamSpect = []
wander = []
sumHeatingBeamSpect = []
powerBeamSpectTot = 0.0
while f[count]<2.0*10**9:
    powerBeamSpectTot+=((f[1]-f[0])*2/(nBunches*2*sp.pi)*bCur**2*abs(ampListFreq[count])**2*SplineFitTank1(f[count]/(10**6)))
    cumHeatingBeamSpect.append((f[1]-f[0])*2/(nBunches*2*sp.pi)*bCur**2*abs(ampListFreq[count])**2*SplineFitTank1(f[count]/(10**6)))
    wander.append(abs(ampListFreq[count])**2*SplineFitTank1(f[count]/(10**6)))
    sumHeatingBeamSpect.append(powerBeamSpectTot)
    count+=1



wander=sp.array(wander)
cumHeatingBeamSpect=sp.array(cumHeatingBeamSpect)
sumHeatingBeamSpect=sp.array(sumHeatingBeamSpect)

###### Calculation of power loss due to bunch spectrum at beam harmonics

heatingTotalCosTank1 = 0.0
heatingFreqTank1=[]
cumHeatingBunch = []
wanderT =[]
spectInt = []
for i in range(0,len(splitFitHeatingTank1)):
    heatingTotalCosTank1+=2*splitFitHeatingTank1[i]*(bCur**2)*cosProfFreq(freqListHeating[i]*10.0**6, bLength)
    heatingFreqTank1.append(2*splitFitHeatingTank1[i]*(bCur**2)*cosProfFreq(freqListHeating[i]*10.0**6, bLength))
    wanderT.append(splitFitHeatingTank1[i]*cosProfFreq(freqListHeating[i]*10.0**6, bLength))
    cumHeatingBunch.append(heatingTotalCosTank1)
    spectInt.append(cosProfFreq(freqListHeating[i]*10.0**6, bLength))

spectInt=sp.array(spectInt)
heatingFreqTank1=sp.array(heatingFreqTank1)
cumHeatingBunch=sp.array(cumHeatingBunch)
wanderT=sp.array(wanderT)
linScaleBunchProfile = sp.array(cosProfFreq(freqList*10**6, bLength))
logScaleBunchProfile = 10*sp.log10(linScaleBunchProfile)

pl.plot(f/10**9, abs(ampListFreq))
pl.plot(freqList/10**3, linScaleBunchProfile**0.5)
pl.axis([0,2,0,1])
##pl.show()
pl.clf()

###### Calculation of power loss by fitting to resonant values


peakLossCalculation = 0.0
for peak in peakData:
    print peak
    peakLossCalculation+=2*peak[2]*(bCur**2)*cosProfFreq(peak[1]*10.0**6, bLength)
    

print powerBeamSpectTot                                                                      
print heatingTotalCosTank1
print peakLossCalculation

fig, ax1 = pl.subplots()

ax1.plot(f/10**6, ampListFreqdB, label="Beam Power Spectrum")
ax1.plot(freqList, logScaleBunchProfile, label="Bunch Power Spectrum")
pl.xlabel("Frequency (MHz)", fontsize=16.0)
pl.ylabel("Normalised Power (dB)", fontsize=16.0)
pl.axis([0,2000,-60,0])
pl.legend(loc="upper left")
ax2=ax1.twinx()

ax2.plot(freqList, splineFitDataTank1, "k-", linewidth=2,label="MKI11-T13-MC03")



pl.ylabel("$\Re{}e(Z_{\parallel})$ ($\Omega$)", fontsize=16.0)
pl.legend(loc="upper right")
pl.axis([0,2000,0,60])
pl.show()
pl.clf()


##pl.semilogy()
##pl.plot(f[10:count]/10**6, cumHeatingBeamSpect, label="Power Loss from beam spectrum")
##pl.plot(freqListHeating, heatingFreqTank1, label="Power loss from beam harmonics on a bunch spectrum")
##pl.xlabel("Frequency (MHz)", fontsize=16.0)
##pl.ylabel("Power Loss (W)", fontsize=16.0)
##pl.legend(loc="lower right")
##pl.show()
##pl.clf()
##print wander.sum()
##print wanderT.sum()
##pl.plot(f[10:count]/10**6, wander)
##pl.plot(freqListHeating, wanderT)
##pl.show()
##pl.clf()
##pl.plot(f[10:count]/10**6, sumHeatingBeamSpect, label="Cumulative power Loss from beam spectrum")
##pl.plot(freqListHeating, cumHeatingBunch, label="Cumulative power loss from beam harmonics on a bunch spectrum")
##pl.xlabel("Frequency (MHz)", fontsize=16.0)
##pl.ylabel("Power Loss (W)", fontsize=16.0)
##pl.legend(loc="upper left")
##pl.show()
##pl.clf()
