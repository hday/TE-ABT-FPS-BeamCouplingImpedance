import csv, os, sys, math
import scipy as sp
import pylab as pl
from scipy import optimize as op

fitfunc = lambda p, x: p[2] + p[1]*x + p[0]*x**2
errfunc = lambda p, x, y, err: (y-fitfunc(p, x))/err

linfitfunc = lambda p, x: p[0] + p[1]*x
linerrfunc = lambda p, x, y, err: (y-linfitfunc(p, x))/err

distance_acc = 0.000001
imp_err = distance_acc/0.003

C = 3.0*10**8
Z0=377.0
lenTot = 0.1
rSep = 0.004
rWire = 0.0002
apDev = 0.005
##print cos_phi_bt
##print cos_phi_at
##print sp.arccos(cos_phi_bt)
##print sp.arccos(cos_phi_at)
length_total = 0.05
length_imp = 0.05
wire_sep = 0.003

def importDat(file_tar):
    tar = open(file_tar, 'r+')
    tempDat = tar.readlines()
    datStore = []
    for entry in tempDat[1:]:
        temp = map(float, entry.split(','))
        datStore.append(temp)

    return datStore


def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp

def analDipRe(data, freqList, lenDUT, lenTot, r_wire, r_sep, apPipe):
    reStore = []
    temp = []
##    Z0 = 120*math.acosh(r_sep/(2*r_wire))
    Z0=50.0
    for i in range(0,len(data)):
        temp.append(-2*Z0*C/(2*sp.pi*freqList[i]*r_sep**2)*sp.log(10**(data[i,1]/20))/lenTot) 
    return temp

def analDipIm(data, freqList, lenDUT, lenTot, r_wire, r_sep, apPipe):
    reStore = []
    temp = []
##    Z0 = 120*math.acosh(r_sep/(2*r_wire))
    Z0=50.0
    for i in range(0,len(data)):
##        print sp.radians(data[i,1]), (2*sp.pi*freq_list[i]*lenTot/C)
        temp.append(-2*Z0*C/(2*sp.pi*freqList[i]*r_sep**2*lenTot)*(sp.radians(data[i,1])+(2*sp.pi*freq_list[i]*lenTot/C))) 
    pl.plot(freqList, sp.radians(data[:,1]))
##    pl.show()
    pl.clf()
    return temp

def analSingRe(data, freqList, lenDUT, lenTot, r_wire, apPipe):
    reStore = []
    temp = []
##    Z0 = 60*sp.log(1.27*apPipe/r_wire)
    Z0=50.0
    for i in range(0,len(data)):
        tempLin = []
        for j in range(1,len(data[0])):
            tempLin.append(-2*Z0/lenTot*sp.log(10**(data[i,j]/20))) 
        temp.append(tempLin)
    return temp

def analSingIm(data, freqList, lenDUT, lenTot, r_wire, apPipe):
    reStore = []
    temp = []
##    Z0 = 60*sp.log(1.27*apPipe/r_wire)
    Z0=50.0
    for i in range(0,len(data)):
        tempLin = []
        for j in range(1,len(data[0])):
            tempLin.append(-2*Z0/lenTot*(sp.radians(data[i,j])+(2*sp.pi*freq_list[i]*lenTot/C))) 
        temp.append(tempLin)
    return temp


freq_list = []
for i in range(6,10,1):
    for j in range(1,11,1):
        freq_list.append(float((j)*10.0**i))

freq_list = list(set(freq_list))
freq_list.sort()


######### Measurements ########

directory = "E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/full-device-impedances/" #Directory of data
screen_conductors_15 = "with-15-screen-conductors/longitudinal-impedance.csv"
alternating_length_19 = "19_conductors_alternating/longitudinal-impedance.csv"

dataScreenSims15 = sp.array(extract_dat(directory+screen_conductors_15))
data_19_alternating = sp.array(extract_dat(directory+alternating_length_19))


######### Simulations #########



##directory = "E:/PhD/1st_Year_09-10/Data/frequency-dom-test/data/project_dimensions_Jun_2012/tmpAlt/results_c_core_low_mesh/" #Directory of data
directory = "E:/PhD/1st_Year_09-10/Data/mkiSimulatedWireMeas/singleWire24Conductors/" #Directory of data

realSingleWireHorz24Cond = importDat(directory+"singleWireDisplacedX1mmdB.csv")
realSingleWireHorzDat24Cond = sp.array(analSingRe(sp.array(realSingleWireHorz24Cond), freq_list, lenTot, lenTot, rWire, apDev))
realSingleWireVert24Cond = importDat(directory+"singleWireDisplacedY2mmdB.csv")
realSingleWireVertDat24Cond = sp.array(analSingRe(sp.array(realSingleWireVert24Cond), freq_list, lenTot, lenTot, rWire, apDev))
realSingleWireOnAxis24Cond = importDat(directory+"singleWireOnAxisdB.csv")
realSingleWireOnAxisDat24Cond = sp.array(analSingRe(sp.array(realSingleWireVert24Cond), freq_list, lenTot, lenTot, rWire, apDev))

temp = []
for entry in realSingleWireHorz24Cond:
    temp.append(entry[0])
freq_list=sp.array(temp)

directory = "E:/PhD/1st_Year_09-10/Data/mkiSimulatedWireMeas/" #Directory of data


realTwoWireHorz24Cond = importDat(directory+"24CondDipolarHorzdBLowFreq.csv")
realTwoWireHorzDat24Cond = sp.array(analDipRe(sp.array(realTwoWireHorz24Cond), freq_list, lenTot, lenTot, rWire, rSep ,apDev))
realTwoWireVert24Cond = importDat(directory+"24CondDipolarVertdBLowFreq.csv")
realTwoWireVertDat24Cond = sp.array(analDipRe(sp.array(realTwoWireVert24Cond), freq_list*10.0**6, lenTot, lenTot, rWire, rSep ,apDev))
realTwoWireHorz24 = importDat(directory+"TwoWireHorz24CondLargerVolLowFreqdb.csv")
realTwoWireHorz24Dat = sp.array(analDipRe(sp.array(realTwoWireHorz24), freq_list*10.0**6, lenTot, lenTot, rWire, rSep ,apDev))


##pl.semilogy()
##pl.loglog()
##pl.axis([10**6,10**10,-1.5*10**3,1.5*10**3])
pl.plot(freq_list/10**3, realSingleWireHorzDat24Cond, label="24 Screen Conductors with ferrite yoke Horz")
pl.plot(freq_list/10**3, realSingleWireOnAxisDat24Cond, label="24 Screen Conductors with ferrite yoke On axis")
pl.plot(freq_list/10**3, realSingleWireVertDat24Cond, label="24 Screen Conductors with ferrite yoke Vert")

pl.xlabel("Frequency (MHz)", fontsize="16.0")
pl.ylabel("$(Z_{\parallel}) (\Omega)$", fontsize="16.0")
pl.legend(loc="upper right")
##pl.axis([0.00,0.050,0,1500])
pl.show()

pl.clf()

pl.semilogy()
##pl.loglog()
##pl.axis([10**6,10**10,-1.5*10**3,1.5*10**3])
##pl.plot(freq_list, realTwoWireHorzDat24Cond, label="24 Screen Conductors with ferrite yoke Horizontal")
pl.plot(freq_list, realTwoWireVertDat24Cond, label="24 Screen Conductors with ferrite yoke Vertical")
pl.plot(freq_list, realTwoWireHorz24Dat, label="24 Screen Conductors with ferrite yoke Horizontal")
pl.plot(freq_list, realSingleWireHorzDat24Cond/(2.0*10.0**-3), label="24 Screen Conductors with ferrite yoke Horz")
pl.xlabel("Frequency (MHz)", fontsize="16.0")
pl.ylabel("$(Z_{\perp}^{dip}) (\Omega{}/m)$", fontsize="16.0")
pl.legend(loc="upper right")
##pl.show()

pl.clf()
