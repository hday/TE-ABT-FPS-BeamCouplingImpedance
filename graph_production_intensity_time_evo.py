import csv, time
import scipy as sp
import pylab as pl

start = time.time()

directory = "E:/PhD/1st_Year_09-10/Software/headtail/data_dump/hdtl-data/" #Directory of data
list_bt_no_sc = [directory+"hdtl-bt-no-sc.NumPar."+str(i)+"_prt.dat" for i in range(1,31)]
list_bt_sc = [directory+"hdtl-bt-sc.NumPar."+str(i)+"_prt.dat" for i in range(1,31)]
list_bt_sc_no_bb =  [directory+"hdtl-bt-no-BB.NumPar."+str(i)+"_prt.dat" for i in range(1,31)]
list_at_no_sc = [directory+"hdtl-at-no-sc.NumPar."+str(i)+"_prt.dat" for i in range(1,31)]
list_at_sc = [directory+"hdtl-at-sc.NumPar."+str(i)+"_prt.dat" for i in range(1,31)]
list_at_sc_no_bb =  [directory+"hdtl-at-no-BB.NumPar."+str(i)+"_prt.dat" for i in range(1,201)]

file_name = "hdtl-at-BB-10-SC-0.NumPar.100_prt.dat"
file_tar = directory+file_name


value_desired = 9
big_bugger =[]
##data_list_1 = []
##for data_raw in list_bt_no_sc:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    append_tar = []
##    for row in linelist:
##        temp = map(float, row.split())
##        append_tar.append([temp[0],temp[value_desired]])
##    data_list_1.append(append_tar)
##    
##bt_no_sc = sp.array(data_list_1)
##
##
##
##data_list_1 = []
##for data_raw in list_bt_sc:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    append_tar = []
##    for row in linelist:
##        temp = map(float, row.split())
##        append_tar.append([temp[0],temp[value_desired]])
##    data_list_1.append(append_tar)
##    
##bt_sc = sp.array(data_list_1)
##    
##
##
##
##data_list_1 = []
##for data_raw in list_bt_sc_no_bb:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    append_tar = []
##    for row in linelist:
##        temp = map(float, row.split())
##        append_tar.append([temp[0],temp[value_desired]])
##    data_list_1.append(append_tar)
##
##
##bt_no_bb=sp.array(data_list_1)
##
##
##
##data_list_1 = []
##for data_raw in list_at_no_sc:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    append_tar = []
##    for row in linelist:
##        temp = map(float, row.split())
##        append_tar.append([temp[0],temp[value_desired]])
##    data_list_1.append(append_tar)
##
##    
##at_no_sc = sp.array(data_list_1)
##
##
##
##data_list_1 = []
##for data_raw in list_at_sc:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    append_tar = []
##    for row in linelist:
##        temp = map(float, row.split())
##        append_tar.append([temp[0],temp[value_desired]])
##    data_list_1.append(append_tar)
##
##    
##at_sc = sp.array(data_list_1)
##    
##
##
##
##data_list_1 = []
##for data_raw in list_at_sc_no_bb:
##    input_file = open(data_raw, 'r+')
##    linelist = input_file.readlines()
##    input_file.close()
##    append_tar = []
##    for row in linelist:
##        temp = map(float, row.split())
##        append_tar.append([temp[0],temp[value_desired]])
##    data_list_1.append(append_tar)
##
##
##at_no_bb=sp.array(data_list_1)


input_file = open(file_tar, 'r+')
linelist = input_file.readlines()
input_file.close()
append_tar = []
for row in linelist:
    temp = map(float, row.split())
    append_tar.append([temp[0],temp[value_desired]])

data_arr = sp.array(append_tar)

x_values = sp.array(range(1,31))


##pl.plot(bt_no_sc[14,:,0], bt_no_sc[14,:,1], 'k-', label = "Below Transition - NoSC/BB")                    # plot fitted curve
##pl.plot(bt_sc[14,:,0], bt_sc[14,:,1],'k:', label = "Below Transition - SC/BB")
##pl.plot(bt_no_bb[14,:,0], bt_no_bb[14,:,1],'k-.', label = "Below Transition - SC/noBB")
##pl.plot(at_no_sc[14,:,0], at_no_sc[14,:,1], 'r-', label = "Above Transition - NoSC/BB")                    # plot fitted curve
##pl.plot(at_sc[14,:,0], at_sc[14,:,1],'r:', label = "Above Transition - SC/BB")
##pl.plot(at_no_bb[14,:,0], at_no_bb[14,:,1],'r-.', label = "Above Transition - SC/noBB")
##pl.plot(at_no_bb[50,:,0], at_no_bb[50,:,1],'b-.', label = "Above Transition - SC/noBB")
##pl.plot(at_no_bb[100,:,0], at_no_bb[100,:,1],'k-.', label = "Above Transition - SC/noBB")
##pl.plot(at_no_bb[140,:,0], at_no_bb[140,:,1],'y-.', label = "Above Transition - SC/noBB")

pl.plot(data_arr[:,0], data_arr[:,1])

name_str = file_name.rstrip('.dat')+"_time_evo"


pl.grid(linestyle="--", which = "major")
pl.xlabel("Time (ms)",fontsize = 16)                  #Label axes
if value_desired == 9:
    pl.ylabel("RMS Bunch Lenth ($\sigma_{z}$) (m)",fontsize = 16)
elif value_desired == 10:
    pl.ylabel("Momentum Spread $\delta{}p/p$",fontsize = 16)
elif value_desired == 20 or 13 or 16:
    pl.ylabel("RMS Longitudinal Emittance $\epsilon_{l}$ (eVs)",fontsize = 16)
pl.title("", fontsize = 16)
##pl.legend(loc = "upper left")
##pl.axis([0,5,0.12,100])
if value_desired == 9:
    pl.savefig(directory+"rms_bunch_length_time_evo"+name_str+".pdf")
    pl.savefig(directory+"rms_bunch_length_time_evo"+name_str+".eps")
    pl.savefig(directory+"rms_bunch_length_time_evo"+name_str+".png")
elif value_desired == 10:
    pl.savefig(directory+"rms_momentum_spred_time_evo"+name_str+".pdf")
    pl.savefig(directory+"rms_momentum_spred_time_evo"+name_str+".eps")
    pl.savefig(directory+"rms_momentum_spred_time_evo"+name_str+".png")
elif value_desired == 20 or 13 or 16:
    pl.savefig(directory+"rms_longitudinal_emittance_time_evo"+name_str+".pdf")
    pl.savefig(directory+"rms_longitudinal_emittance_time_evo"+name_str+".eps")
    pl.savefig(directory+"rms_longitudinal_emittance_time_evo"+name_str+".png")

pl.show()
pl.clf() 

print time.time() - start
