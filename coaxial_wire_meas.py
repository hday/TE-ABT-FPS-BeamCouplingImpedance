import csv, time, os, sys
import scipy as sp
import pylab as pl
import scipy.optimize as op

####### Define constants for calculations ##########

C = 3.0*10**8
Z0 = 377
lenMKP = 3.423+(2*0.035)+(2*0.005)
Zc = 320.0

######### Define importing, analysis functions #########

def sParaRead(fileTar, lenDUT, col):
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter=0.0
    last = 1e8
    freq_last = 0.0
    for row in inputData[5:]:
        row=map(float, row.split())
        phase = row[col+1]-counter
        if phase > last and (phase-last)>180 and abs(freq_last-row[0])>0.3e8:
            count+=1
            counter+=360
            phase-=360
            freq_last = row[0]
        elif phase>last and (phase-last)>180 and abs(freq_last-row[0])<0.3e8:
            phase+=360    

        s21Pec = 2*sp.pi*row[0]*lenDUT/C
        import_dat = [row[0], row[col], sp.radians(phase), -s21Pec]
        data.append(import_dat)
        last = phase

    return data


def impAnalysisSingle(fileTar, lenDUT, r_wire, r_pipe, Zc):
    ###### Analyses a S2P file for measurement of longitudinal impedance to impedance data ########
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter=0.0
    last = 1e8
    freq_last = 0.0
    for row in inputData[5:]:
        row=map(float, row.split())
        linDat = 10**(row[3]/20)
        s21Pec = 2*sp.pi*row[0]*lenDUT/C
        phase = row[4]-counter
        if phase > last and (phase-last)>180 and abs(freq_last-row[0])>0.3e8:
            count+=1
            counter+=360
            phase-=360
            freq_last = row[0]
        elif phase>last and (phase-last)>180 and abs(freq_last-row[0])<0.3e8:
            phase+=360


        import_dat = [row[0], -2*Zc*sp.log(linDat), -2*Zc*(sp.radians(phase)+s21Pec)]
##        import_dat = [row[0], -2*Zc*(sp.log(linDat)-sp.log(linDat)*(sp.radians(phase)+s21Pec)/lenDUT), -2*Zc*((sp.radians(phase)+s21Pec)+((sp.log(linDat))**2-(sp.radians(phase)+s21Pec)**2)/(2*lenDUT))]
        data.append(import_dat)
        last = phase

    return data

def impAnalysisTwo(fileTar, lenDut, r_wire, r_pipe, d_wire, Zc):
    ###### Analyses a S2P file for measurement of dipole impedance to impedance data ########
    data = []
    inputFile = open(fileTar, 'r+')
    inputData = inputFile.readlines()
    inputFile.close()
    count = 0
    counter = 0.0
    last = 1e8

    for row in inputData[5:]:
        row=map(float, row.split())
        linDat = 10**(row[3]/20)
        s21Pec = 2*sp.pi*row[0]*lenDut/C
        phase = row[4]-counter
        if phase>last and (phase-last)>180:
            count+=1
            counter+=360
            pgase-=360
        import_dat = [row[0], -2*Zc*sp.log(linDat)*C/(2*sp.pi*row[0]*d_wire**2), -2*Zc*(sp.radians(phase)+s21Pec)*C/(2*sp.pi*row[0]*d_wire**2)]
##        import_dat = [row[0], -2*Zc*(sp.log(linDat)-sp.log(linDat)*(sp.radians(phase)+s21Pec)/lenDut)*C/(row[0]*d_wire**2), -2*Zc*((sp.radians(phase)+s21Pec)+((sp.log(linDat))**2-(sp.radians(phase)+s21Pec)**2)/(2*lenDut))*C/(row[0]*d_wire**2)]
        data.append(import_dat)
    return data

def impTransPara(dispWire, measImp, wireErr):
##### Takes an array of longitudinal measurements and returns the transverse impedance
    quadFunc = lambda p, x: p[0] + x * p[1] + x**2 * p[2]
    errFunc = lambda p, x, Z: (Z-quadFunc(p,x))

    totTrans = []
    longComp = []

    for i in range(0,len(measImp[0,:,1])):
        pinit = [1.0, -1.0, 1.0]
        ZerrRe = measImp[:,i,1]*wireErr
        pReal, pother = op.leastsq(errFunc, pinit, args=(dispWire, measImp[:,i,1]), full_output = 0)
        pinit = [1.0, -1.0, 1.0]
        ZerrIm = measImp[:,i,2]*wireErr
        pImag, pother = op.leastsq(errFunc, pinit, args=(dispWire, measImp[:,i,2]), full_output = 0)
        totTrans.append([pReal[2]*C/(2*sp.pi*measImp[0,i,0]), pImag[2]*C/(2*sp.pi*measImp[0,i,0])])
        longComp.append([pReal[0], pImag[0]])

    return longComp, totTrans

def quadImp(totalTrans, dipTrans, axis):
######## Returns the quadrupolar impedance from total transverse and dipolar data
    quadImp = []
    for i in range(0,len(totalTrans)):
        if axis == "horz":
            quadImp.append([totalTrans[i,1]+dipTrans[i,1],totalTrans[i,2]+dipTrans[i,2]])
        elif axis == "vert":
            quadImp.append([totalTrans[i,1]-dipTrans[i,1],totalTrans[i,2]-dipTrans[i,2]])
    return quadImp


fileListy = ["E:/PhD/1st_year_09-10/Data/MKP/hugo_olav/22-06-12/"+str(i)+"mm_y/S21_NO_GATING.S2P" for i in range(-9,12,3)]

count= 0
datay=[]
for tarF in fileListy:
    if count == 0:
        dataSy=sParaRead(tarF, lenMKP, 3)
        count+=1
    datay.append(impAnalysisSingle(tarF, lenMKP, 0.5*10**-3, 0.027, Zc))

datay = sp.array(datay)
dataSy = sp.array(dataSy)

fileListx = ["E:/PhD/1st_year_09-10/Data/MKP/hugo_olav/22-06-12/"+str(i)+"mm_x/S21_NO_GATING.S2P" for i in range(-9,12,3)]

count= 0
datax=[]
for tarF in fileListx:
    if count == 0:
        dataSx=sParaRead(tarF, lenMKP, 3)
        count+=1
    datax.append(impAnalysisSingle(tarF, lenMKP, 0.5*10**-3, 0.027, Zc))

datax = sp.array(datax)
dataSx = sp.array(dataSx)

for i in range(0,len(datay)):
    pl.plot(datay[i,:,0]/10**9, datay[i,:,1], label = str(fileListy[i]))
##    pl.plot(data[i,:,0]/10**9, abs(data[i,:,2]), label = str(fileList[i]))

pl.axis([0,2, 0, 10000])
pl.xlabel("Frequency (GHz)", fontsize="16")
pl.ylabel("$Z_{\parallel{}} (\Omega)$", fontsize="16")
pl.legend(loc="lower right")
pl.show()
pl.clf()

pl.plot(dataSy[:,0], dataSy[:,1], label = "S21 Mag")
pl.plot(dataSy[:,0], dataSy[:,2], label = "S21 Phase")
pl.plot(dataSy[:,0], dataSy[:,3], label = "PEC Phase")
pl.xlabel("Frequency (GHz)", fontsize="16")
pl.ylabel("$Z_{\parallel{}} (\Omega)$", fontsize="16")
pl.legend(loc='lower right')
##pl.axis([0,2,10**1,10**5])
##pl.show()
pl.clf()

wireDisp = sp.linspace(-0.006, 0.006, 5)
longImpy, totalTransImpy = impTransPara(wireDisp, datay, 0.001)
longImpy = sp.array(longImpy)
totalTransImpy = sp.array(totalTransImpy)

wireDisp = sp.linspace(-0.006, 0.006, 5)
longImpx, totalTransImpx = impTransPara(wireDisp, datax, 0.001)
longImpx = sp.array(longImpx)
totalTransImpx = sp.array(totalTransImpx)

pl.plot(datay[4,:,0]/10**9, longImpy[:,0], label="$\Re{}e Z_{\parallel, y}$ Fit")
##pl.plot(datay[4,:,0]/10**9, longImpy[:,1], label="$\Im{}m Z_{\parallel, y}$ Fit")
pl.plot(datax[4,:,0]/10**9, longImpx[:,0], label="$\Re{}e Z_{\parallel, x}$ Fit")
##pl.plot(datax[4,:,0]/10**9, longImpx[:,1], label="$\Im{}m Z_{\parallel, x}$ Fit")
##pl.plot(datay[4,:,0]/10**9, datay[4,:,1], label="$\Re{}e Z_{\parallel}$ Meas")
##pl.plot(datay[4,:,0]/10**9, datay[4,:,2], label="$\Im{}m Z_{\parallel}$ Meas")
pl.xlabel("Frequency (GHz)", fontsize="16")
pl.ylabel("$Z_{\parallel{}} (\Omega)$", fontsize="16")
pl.legend(loc='lower right')
##pl.axis([0,0.5,-1000,10000])
##pl.show()
pl.clf()

##pl.plot(datay[4,:,0]/10**9, totalTransImpy[:,0], 'k-', label="$\Re{}e Total Z_{\perp{}, y}$ Fit")
##pl.plot(datay[4,:,0]/10**9, totalTransImpy[:,1], 'r-',label="$\Im{}m Total Z_{\perp, y}$ Fit")
pl.plot(datax[4,:,0]/10**9, totalTransImpx[:,0], 'k--', label="$\Re{}e Total Z_{\perp, x}$ Fit")
##pl.plot(datax[4,:,0]/10**9, totalTransImpx[:,1], 'r--', label="$\Im{}m Total Z_{\perp, x}$ Fit")
pl.xlabel("Frequency (GHz)", fontsize="16")
pl.ylabel("$Z_{\parallel{}} (\Omega{}/m)$", fontsize="16")
pl.legend(loc='lower right')
pl.axis([0,2,-10**6,10**6])
##pl.show()
pl.clf()

fileDatDipy = "E:/PhD/1st_year_09-10/Data/MKP/hugo_olav/19-06-12/mkp-two-wire-vertical/S21_TRANSMISSION_GATING.S2P"
fileDatDipx = "E:/PhD/1st_year_09-10/Data/MKP/hugo_olav/20-06-12/mkp-two-wire-horizontal/S21_TRANSMISSION_NO_GATING.S2P"

datYDip = impAnalysisTwo(fileDatDipy, lenMKP, 0.5*10**-3, 0.027, 7*10**-3, Zc)
datXDip = impAnalysisTwo(fileDatDipx, lenMKP, 0.5*10**-3, 0.027, 7*10**-3, Zc)
datYDip = sp.array(datYDip)
datXDip = sp.array(datXDip)

##pl.semilogy()
pl.plot(datXDip[:,0]/10**9, datXDip[:,1], 'k-', label="$\Re{}e Dip Z_{\perp{}, x}$")
##pl.plot(datXDip[:,0]/10**9, datXDip[:,2], 'r-', label="$\Im{}m Dip Z_{\perp{}, x}$")
pl.plot(datYDip[:,0]/10**9, datYDip[:,1], 'k--', label="$\Re{}e Dip Z_{\perp{}, y}$")
##pl.plot(datYDip[:,0]/10**9, datYDip[:,2], 'r--', label="$\Im{}m Dip Z_{\perp{}, y}$")
pl.xlabel("Frequency (GHz)", fontsize="16")
pl.ylabel("$Z_{\parallel{}} (\Omega{}/m)$", fontsize="16")
pl.legend(loc='lower right')
pl.axis([0,2,0,2*10**6])
##pl.show()
pl.clf()
