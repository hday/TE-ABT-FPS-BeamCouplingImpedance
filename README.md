Library of functions, analysis files and associated code to analyse beam impedance simulations, measurements and related issues such as beam induced heating and longitudinal beam profiles.

Library functions stored in library impedance; sub-directories
impedance - contains functions for reading data files, measurement analysis and power loss calculations
profiles - contains functions for longitudinal beam distributions (gaussian, cos^2, parabolic)

Other files are a variety examples and use. Some key and clear examples of each application are listed below:

coaxial wire beam impedance measurement (single wire on axis) Done
coaxial wire beam impedance measurement (two wires on axis)
coaxial wire beam impedance measurement (displaced single wire) Done
coaxial wire beam impedance measurement (resonant coaxial method) Done

beam impedance simulations:
Importing impedance simulation data
Calculating power loss
Calculating power loss distributions from loss maps - Done

Beam profiles:
Generating beam profiles in frequency and time domain - Done
Bunch train beam profiles - Done
