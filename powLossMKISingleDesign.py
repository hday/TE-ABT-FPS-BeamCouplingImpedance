import csv, time, os, sys
import scipy as sp
import pylab as pl
import scipy.optimize as op
import scipy.interpolate as interp
import scipy.integrate as integrate
import scipy.fftpack as fftPack
import mpl_toolkits.mplot3d as ToolKit3d
import impedance.impedance as imp
import impedance.powlossprof as prof

####### Define constants for calculations ##########

C = 299792458.0
Z0 = 377
lenMKP = 3.423+(2*0.035)+(2*0.005)
Zc = 320.0

######### Define importing, analysis functions #########

c=299792458.0
mu0=4*sp.pi*10.0**-7
eps0= 8.854187817*10.0**-12
dAper = 0.045
rWire = 0.0005
lWire = 3.551
lDUT = 2.45
resCopper = 17.24*10.0**-9
resOuter = 1/(3.3*10.0**7)
ZChar = 60*sp.log(dAper/rWire)

def minValIndex(arrMark, minValue=10.0):
    return sp.array([i for i in range(0,len(arrMark)) if arrMark[i]>minValue]) 

start=time.time()

length = 2.88
f_rev = C/27000.0
nBunches = 2808
qPart = 1.6*10.0**-19
nPart=1.15*10.0**11
bCur = nPart*qPart*nBunches*f_rev
bLength=1.001*10.0**-9
testImp = [250*10**6, 10.0, 10000]
bunchCur=nPart*qPart*f_rev
print bCur, bunchCur

##### Directory definiton #####

directoryRings60mmDiam = "E:/PhD/1st_Year_09-10/Data//LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/70mmOverLapPostLS1/"
##directoryRings60mmDiam = "E:/PhD/1st_Year_09-10/Data//LHC-MKI-data/NewDesign/ImpedanceSimulationsCST2013/POSTLS1Overlap117mm/powLossTo2GHz/powLossDens/FerriteCollar20mm/"

##### Impedance Input #####

impLong = imp.CSTtoFuncComp(imp.extract_dat2013(directoryRings60mmDiam+"longitudinal-impedance-real.txt"))
##impLong = imp.CSTtoFuncComp(imp.extract_dat2013(directoryRings60mmDiam+"longitudinal-impedance-real-dft.txt"))
temp = []
for i in range(0,len(impLong)-1):
    temp.append([impLong[i][0],impLong[i][1]])
impLong=sp.array(temp)
powLossTot, freqPowLoss = prof.heatingValGauss(impLong, bCur, 25.0*10.0**-9, bLength)
print powLossTot/2.45

pl.plot(freqPowLoss[:,0], freqPowLoss[:,1])
pl.show()
pl.clf()

##### Loss Distribution Input #####

lossTestf0423 = directoryRings60mmDiam+"f0423.txt"
lossTestf0838 = directoryRings60mmDiam+"f0838.txt"
lossTestf1239 = directoryRings60mmDiam+"f1239.txt"
lossTestf1652 = directoryRings60mmDiam+"f1652.txt"
lossTestf1977 = directoryRings60mmDiam+"f1977.txt"

##lossTestf0423 = directoryRings60mmDiam+"f04385.txt"
##lossTestf0838 = directoryRings60mmDiam+"f0860.txt"
##lossTestf1239 = directoryRings60mmDiam+"f1264.txt"
##lossTestf1652 = directoryRings60mmDiam+"f1661.txt"
##lossTestf1977 = directoryRings60mmDiam+"f2029.txt"

datTestRings60mmDiamf0423=sp.array(imp.extract_dat2013LossMap(lossTestf0423))
##print len(datTestSidePlatef0423)
(x,y,z), fieldsPlotRings60mmDiamf0423, lensRings60mmDiamf0423 = imp.meshGridCSTFieldMap(datTestRings60mmDiamf0423)
lossSumOnZRings60mmDiamf0423 = imp.cumSumLoss3DInZ(fieldsPlotRings60mmDiamf0423, lensRings60mmDiamf0423[2])
lossOnZRings60mmDiamf0423 = imp.sumLoss3DInZ(fieldsPlotRings60mmDiamf0423, lensRings60mmDiamf0423[2])
lossSliceZRings60mmDiamf0423 = imp.cumSumLoss3DInZDiff(fieldsPlotRings60mmDiamf0423, "X")

fieldsPlotRings60mmDiamf0423=None
lensRings60mmDiamf0423 = None

datTestRings60mmDiamf0838=sp.array(imp.extract_dat2013LossMap(lossTestf0838))
(x,y,z), fieldsPlotRings60mmDiamf0838, lensRings60mmDiamf0838 = imp.meshGridCSTFieldMap(datTestRings60mmDiamf0838)
lossSumOnZRings60mmDiamf0838 = imp.cumSumLoss3DInZ(fieldsPlotRings60mmDiamf0838, lensRings60mmDiamf0838[2])
lossOnZRings60mmDiamf0838 = imp.sumLoss3DInZ(fieldsPlotRings60mmDiamf0838, lensRings60mmDiamf0838[2])
lossSliceZRings60mmDiamf0838 = imp.cumSumLoss3DInZDiff(fieldsPlotRings60mmDiamf0838, "X")

fieldsPlotRings60mmDiamf0838=None
lensRings60mmDiamf0838 = None
(x,y,z)=(None,None,None)

datTestRings60mmDiamf1239=sp.array(imp.extract_dat2013LossMap(lossTestf1239))
(x,y,z), fieldsPlotRings60mmDiamf1239, lensRings60mmDiamf1239 = imp.meshGridCSTFieldMap(datTestRings60mmDiamf1239)
lossSumOnZRings60mmDiamf1239 = imp.cumSumLoss3DInZ(fieldsPlotRings60mmDiamf1239, lensRings60mmDiamf1239[2])
lossOnZRings60mmDiamf1239 = imp.sumLoss3DInZ(fieldsPlotRings60mmDiamf1239, lensRings60mmDiamf1239[2])
lossSliceZRings60mmDiamf1239 = imp.cumSumLoss3DInZDiff(fieldsPlotRings60mmDiamf1239, "X")

fieldsPlotRings60mmDiamf1239=None
lensRings60mmDiamf1239 = None
(x,y,z)=(None,None,None)

datTestRings60mmDiamf1652=sp.array(imp.extract_dat2013LossMap(lossTestf1652))
(x,y,z), fieldsPlotRings60mmDiamf1652, lensRings60mmDiamf1652 = imp.meshGridCSTFieldMap(datTestRings60mmDiamf1652)
lossSumOnZRings60mmDiamf1652 = imp.cumSumLoss3DInZ(fieldsPlotRings60mmDiamf1652, lensRings60mmDiamf1652[2])
lossOnZRings60mmDiamf1652 = imp.sumLoss3DInZ(fieldsPlotRings60mmDiamf1652, lensRings60mmDiamf1652[2])
lossSliceZRings60mmDiamf1652 = imp.cumSumLoss3DInZDiff(fieldsPlotRings60mmDiamf1652, "X")

fieldsPlotRings60mmDiamf1652=None
lensSRings60mmDiamf1652 = None
(x,y,z)=(None,None,None)

datTestRings60mmDiamf1977=sp.array(imp.extract_dat2013LossMap(lossTestf1977))
(x,y,z), fieldsPlotRings60mmDiamf1977, lensRings60mmDiamf1977 = imp.meshGridCSTFieldMap(datTestRings60mmDiamf1977)
lossSumOnZRings60mmDiamf1977 = imp.cumSumLoss3DInZ(fieldsPlotRings60mmDiamf1977, lensRings60mmDiamf1977[2])
lossOnZRings60mmDiamf1977 = imp.sumLoss3DInZ(fieldsPlotRings60mmDiamf1977, lensRings60mmDiamf1977[2])
lossSliceZRings60mmDiamf1977 = imp.cumSumLoss3DInZDiff(fieldsPlotRings60mmDiamf1977, "X")

fieldsPlotRings60mmDiamf1977=None
lensRings60mmDiamf1977 = None


tmp = []
bins = 44
offset = 0
test = len(lossSumOnZRings60mmDiamf1977)/bins
tmpNormRings60mmDiam = []
tmpNormZRings60mmDiam=[]

tmpZ=[]
for i in range(0,bins):
    for j in range(0,test):
        tmpZ.append(offset+test*i)

for i in range(0,bins):
   for j in range(0,test):
        tmpNormZRings60mmDiam.append(test*i)

##print test, len(tmpNormZSidePlate)
for i in range(0,len(tmpNormZRings60mmDiam)):
    if i+test<len(tmpNormZRings60mmDiam)-1:
####        print i+test, len(lossOnZ)-1, tmpZ[i], len(tmpZ), len(tmpNormZ)
####        print tmpZ[i+test]
        tmpNormRings60mmDiam.append(sp.ma.average(lossOnZRings60mmDiamf0423[tmpZ[i]:tmpZ[i+test]]))
    else:
        tmpNormRings60mmDiam.append(sp.ma.average(lossOnZRings60mmDiamf0423[tmpZ[i]:-1]))
tmpNormRings60mmDiam=sp.array(tmpNormRings60mmDiam)

###Losses Ferrite Rings 60mm Rad ###

normRings60mmDiamf0423 = (lossOnZRings60mmDiamf0423/lossSumOnZRings60mmDiamf0423[-1])*prof.gaussProf(0.423*10**9, bLength)**2
normRings60mmDiamf0838 = (lossOnZRings60mmDiamf0838/lossSumOnZRings60mmDiamf0838[-1])*prof.gaussProf(0.838*10**9, bLength)**2
normRings60mmDiamf1239 = (lossOnZRings60mmDiamf1239/lossSumOnZRings60mmDiamf1239[-1])*prof.gaussProf(1.239*10**9, bLength)**2
normRings60mmDiamf1652 = (lossOnZRings60mmDiamf1652/lossSumOnZRings60mmDiamf1652[-1])*prof.gaussProf(1.652*10**9, bLength)**2
normRings60mmDiamf1977 = (lossOnZRings60mmDiamf1977/lossSumOnZRings60mmDiamf1977[-1])*prof.gaussProf(1.977*10**9, bLength)**2

meanValRings60mmDiam = []
for i in range(0,len(normRings60mmDiamf0423)):
    meanValRings60mmDiam.append(sp.mean(normRings60mmDiamf0423[i]+normRings60mmDiamf0838[i]+normRings60mmDiamf1239[i]+normRings60mmDiamf1652[i]+normRings60mmDiamf1977[i]))
##    print sp.mean(normf0423[i]+normf0838[i]+normf1239[i]+normf1652[i]+normf1977[i]), normf1977[i]
meanValRings60mmDiam = sp.array(meanValRings60mmDiam)
meanSumRings60mmDiam = []
for i in range(0,len(meanValRings60mmDiam)):
    meanSumRings60mmDiam.append(sp.sum(meanValRings60mmDiam[:i]))
meanSumRings60mmDiam=sp.array(meanSumRings60mmDiam)/meanSumRings60mmDiam[-1]

powLossMKIAvg = powLossTot/2.45

listZerosRings60mmDiam = minValIndex(lossOnZRings60mmDiamf0423,10**3)
##print listZerosSidePlate[0][1]
currentLowerBound = 0
currentUpperBound = 1
binnedLossesRings60mmDiam = list(sp.zeros(len(lossOnZRings60mmDiamf0423)))
for i in range(0,len(listZerosRings60mmDiam)-1):
##    print listZeros[0][currentUpperBound]
    while listZerosRings60mmDiam[currentUpperBound]==(listZerosRings60mmDiam[currentUpperBound+1]-1) and (currentUpperBound<len(listZerosRings60mmDiam)-2):
        currentUpperBound+=1
##    print sp.mean(lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]), lossOnZ[listZeros[0][currentLowerBound]:listZeros[0][currentUpperBound]]
    binnedLossesRings60mmDiam[listZerosRings60mmDiam[i]]=sp.mean(meanValRings60mmDiam[listZerosRings60mmDiam[currentLowerBound]:listZerosRings60mmDiam[currentUpperBound]])
    if listZerosRings60mmDiam[i]==listZerosRings60mmDiam[currentUpperBound]:
        currentLowerBound=currentUpperBound+1
        currentUpperBound=currentLowerBound+1
    
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)
binnedLossesRings60mmDiam=sp.array(binnedLossesRings60mmDiam)*powLossMKIAvg*2.45/((z[0,0,1]-z[0,0,0])/1000)
##binnedLossesRingsAir=sp.array(binnedLossesRingsAir)*powLossMKIAvg*1000
        
##print z[0,0]
lossCalcRings60mmDiam = tmpNormRings60mmDiam/lossSumOnZRings60mmDiamf0423[-1]*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)
lossConstRings60mmDiam = lossOnZRings60mmDiamf0423/lossSumOnZRings60mmDiamf0423[-1]*powLossMKIAvg/((z[0,0,1]-z[0,0,0])/1000)

### End Ferrite Rings 60mm Rad ###

meshtime=time.time()-start
print meshtime

print sp.mean(binnedLossesRings60mmDiam)
print sp.sum(meanValRings60mmDiam*powLossMKIAvg*2.45)


pl.plot(z[0,0],binnedLossesRings60mmDiam, "k-",label="Losses averaged to single components, Post LS1")
pl.xlabel("Displacement from centre of magnet (mm)", fontsize=24.0)
pl.ylabel("Power Loss (W/m)", fontsize=24.0)
##pl.ylabel("Power Loss (a.u)", fontsize=16.0)
pl.legend(loc="upper right")
pl.show()
pl.clf()

pl.plot(z[0,0],meanSumRings60mmDiam)
pl.show()
pl.clf()
