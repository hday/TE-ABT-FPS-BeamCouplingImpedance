import csv, os, sys
import scipy as sp
import pylab as pl
import scipy.optimize as op
import scipy.interpolate as interp
import impedance.impedance as imp
import impedance.powlossprof as prof

C = 299792458.0

##def extract_dat(file_name):
##    data = open(file_name, "r+")
##    tar = csv.reader(data, delimiter=",")
##    temp=[]
##    for row in tar:
##        row = map(float, row)
##        temp.append(row)
##    data.close()
##
##    return temp
##
##def peak_fit(data):
##    lorentz_fit = lambda p, f: p[1]/(1 + (p[2]*(f/p[0] - p[0]/f))**2) ##### A lorentzian fitting function
##    lorentz_errfunc = lambda p, f, y: (y-lorentz_fit(p, f))
##    err_factor = 0.01
##
##    peak_freq = []       
##
##    lower_bound = 50
##    upper_bound = len(data[:,0])
##    x_val = data[lower_bound:upper_bound,0]
##    y_val = data[lower_bound:upper_bound,1]
##
##    ####### fit lorentzian data ###########
##
##    current_max_y = 0.0
##    current_max_x = 0
##    for i in range(0, len(x_val)):
##        if y_val[i] > current_max_y:
##            current_max_y = y_val[i]
##            current_max_x = i
##        elif y_val[i] < current_max_y and i-current_max_x > 50 and y_val[current_max_x]>5:
##            peak_freq.append([current_max_x, x_val[current_max_x], y_val[current_max_x]])
##            current_max_y = 0
##        else:
##            pass
##    fit_data = []
##    old_upper = 100
##
##    for res in peak_freq:
##        pinit = [0.2, 10.0, 10.0]
##        lower_bound = res[0]-50
##        upper_bound = res[0]+50
##        ###### Check to ensure no under or overrun
##        if lower_bound < 0:
##            lower_bound = 0
##        if upper_bound > (len(data)-1):
##            upper_bound = (len(data)-1)
##    ##    ####### Make sure no overlap with other peaks
##    ##    if lower_bound < old_upper and len(fit_data) > 0:
##    ##        lower_bound = old_upper-1
##        x_val = data[lower_bound:upper_bound,0]
##        y_val = data[lower_bound:upper_bound,1]
##        pfinal, pcov = op.leastsq(lorentz_errfunc, pinit, args = (x_val, y_val))   
##        fit_data.append(pfinal)
##        old_upper = upper_bound
##
##    return fit_data, peak_freq
##
##def heatCalc(Z, distribution, bLength, Ib):
##    if distribution == "gaussian":
##        dist = lambda bLength: sp.e**(2*sp.pi*Z[0]*bLength)**2
##    elif distribution == "cos":
##        dist = lambda bLength: sp.sin(2*sp.pi*Z[0]*bLength)/(2*sp.pi*Z[0]*bLength*(1-Z[0]*bLength)**2)**2
##    else:
##        print "Not a supported distrubution"
##    return 2*Ib**2*dist(bLength)**2*Z[1]
##
##def gaussProfFreq(freq, bLength):
##    sigma = bLength/(4.0*2.0*sp.log(2.0))
##    return sp.e**(-(2.0*sp.pi*freq*sigma)**2/2)
##
##def cosProfFreq(freq, bunLength):
##    return (sp.sin(sp.pi*freq*bunLength)/(sp.pi*freq*bunLength*(1-(freq*bunLength)**2)))**2
##
##def paraProfFreq(freq, bunLength):                                     
##    return (sp.sin(sp.pi*freq*bunLength)*((2*sp.pi*freq)**2*bunLength**2)+(8*sp.cos(sp.pi*freq*bunLength)*2*sp.pi*bunLength))/(2*sp.pi*freq)**2/bunLength**2
##    return sp.sin(sp.pi*freq*bunLength)/(sp.pi*freq)

##def gaussProfTime(dist, sigma, order):
####    return sp.e**(-(dist/sigma)**2/2)/(2*sp.pi)**0.5/sigma
##    return sp.e**(-order*(dist/sigma)**2/2)
####    return sp.e**(-(dist/sigma)**2/2)

##def gaussProfTime(dist, bLength, order):
##    sigma = bLength/(4.0*2.0*sp.log(2.0))
####    return sp.e**(-(dist/sigma)**2/2)/(2*sp.pi)**0.5/sigma
##    return sp.e**(-(dist/sigma)**2/2)
####    return sp.e**(-(dist/sigma)**2/2)
##
##def paraProfTime(dist, bunch_length):
##    if bunch_length/2 <= dist:
##        return 0
##    elif bunch_length/2 >= dist:
##        return 1-(2*dist/bunch_length)**2
##    else:
##        return 0
##
##def cosProfTime(dist, bunch_length):
##    if bunch_length/2 <= dist:
##        return 0
##    if bunch_length/2 >= dist:
####        print dist,bunch_length
##        return sp.cos(sp.pi/bunch_length*dist)**2
##    else:
##        return 0
##
##def linToLog(data):
##    return 20*sp.log10(data)
##                              
##def logToLin(data):
##    return 10**(data/20)

disp = 0.001
length = 2.88
f_rev = 3*10**8/27000.0
nBunches = 2808
qPart = 1.6*10.0**-19
nPart=1.15*10.0**11
bCur = nPart*qPart*nBunches*f_rev
bLength=4*0.2/(3.0*10.0**8)
##bLength=0.67*10.0**-9
testImp = [250*10**6, 10.0, 10000]
timeRes = 0.08*10.0**-9
bunRange = 50

print bLength

####### 25ns Spec
##
bunSpacing = 24.97*10.0**-9
trainSpacing = 8*bunSpacing
bunInTrain = 72
trainsInMachine = 4
blankBunSpaces = 562
print 43450
##### 50ns Spec

##bunSpacing = 2*24.97*10.0**-9
##trainSpacing = 224.6*10.0**-9
##bunInTrain = 36
##trainsInMachine = 4
##blankBunSpaces = 287
##print 43450
##### 25ns Spec LHC
##
##bunSpacing = 24.97*10.0**-9
##trainSpacing = 8*bunSpacing
##bunInTrain = 288
##trainsInMachine = 10
##blankBunSpaces = 290
##print 11245
##### 50ns Spec LHC

##bunSpacing = 2*24.97*10.0**-9
##trainSpacing = 2*224.6*10.0**-9
##bunInTrain = 144
##trainsInMachine = 10
##blankBunSpaces = 165
##print 11245
##### 25ns Spec FCC

##bunSpacing = 24.97*10.0**-9
##trainSpacing = 12*bunSpacing
##bunInTrain = 144
##trainsInMachine = 90
##blankBunSpaces = 610
print 100000/C

##### ESS Spec

##bunSpacing = 2.841*10.0**-9
##trainSpacing = 8*bunSpacing
##bunInTrain = 72
##trainsInMachine = 4
##blankBunSpaces = 562
##beamCur = 62.5*10.0**-3
print 43450

print bunInTrain*trainsInMachine

print "Bunch Harmonic:"+str(1/bunSpacing)
print "Train Harmonic:"+str(1/trainSpacing)
print "Buch Harmonic:"+str(1/bunSpacing/trainsInMachine/bunInTrain)

spaceCount = ((trainsInMachine*bunInTrain*bunSpacing)+(trainSpacing*(trainsInMachine+1))+(bunInTrain*trainsInMachine*bLength)+bunSpacing*blankBunSpaces)/timeRes
ampListTime = [0 for i in range(0,int(spaceCount))]
print spaceCount
timing = []
for i in range(0,len(ampListTime)):
    timing.append(i*timeRes)

bunLimit = int((bLength)/timeRes)

for i in range(0,trainsInMachine):
    for j in range(0,bunInTrain):
        midPoint = int(((j*bunSpacing)+(i*trainsInMachine*trainSpacing)+(i*bunInTrain*bunSpacing)+bLength)/timeRes)
##        print midPoint, bunLimit, i
        for k in range(midPoint-bunLimit, midPoint+bunLimit):
            try:
                ampListTime[k] = prof.gaussProfTime(abs(k-midPoint)*timeRes, bLength, 31)
            except:
##                print i, j, j
                pass
##            ampListTime[k] = prof.cosProfTime(abs(k-midPoint)*timeRes, bLength)
##            ampListTime[k] = prof.paraProfTime(abs(k-midPoint)*timeRes, bLength)

pl.plot(timing, ampListTime)
pl.axis([0,timing[-1], -0.5,1.5])
pl.xlabel("Time (s)", fontsize=16.0)
pl.ylabel("Amplitude", fontsize=16.0)
pl.show()
pl.clf()

N = len(timing)
f = 1/timeRes*sp.r_[0:(N/2)]/N
print sp.r_[0:(N/2)]
n= len(f)
ampListFreq = sp.fft(ampListTime)
ampListFreq = ampListFreq[0:n]/sp.amax(ampListFreq)
ampListFreqLog = imp.linToLog(ampListFreq)
temp = abs(ampListFreq)

freqListHeating = sp.linspace(40,2000,2000/40)
##interpFreqLin = interp.InterpolatedUnivariateSpline(freqListHeating/10**6, temp)
##plotHeating = interpFreqLin(freqListHeating)

print f[1]-f[0]
##print len(f), len(ampListFreq)

freqList = sp.linspace(10**3,2*10**9,10**6)
gausProfSPS = prof.gaussProfFreq(freqList, 0.8/(3*10**8))

pl.plot(f/10**9, ampListFreqLog)
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("S (dB)", fontsize=16.0)
##pl.axis([0.03,0.405,-60,0])
pl.xlim(0.0,2.0)
pl.ylim(-60,0)
pl.show()
pl.clf()

pl.plot(f/10**9, abs(ampListFreq))
##pl.plot(freqListHeating/10**3, plotHeating, "bx")
pl.plot(freqList/10**9, gausProfSPS)
for i in range(1,2):
    pl.axvline(1/bunSpacing/10**9)
    for j in range(-10,10):
        pl.axvline((1/bunSpacing+j/trainSpacing/trainsInMachine)/10**9)
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("S (au)", fontsize=16.0)
##pl.axis([0.395,0.405,-60,0])
pl.xlim(0.03,0.05)
pl.ylim(0,1.0)
##pl.show()
pl.clf()

freqList = []
for j in range(1,30001):
    freqList.append(j*(10.0**5))

freqList = list(set(freqList))
freqList.sort()


