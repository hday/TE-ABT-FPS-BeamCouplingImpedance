import sympy.mpmath as mpmath
import scipy, numpy, csv, time
from matplotlib import pyplot
import numbers
import pylab as pl

Z0 = 377.0
a = 0.025
b = 0.005
d = b+0.005
length = 1
disp = 1
e0 = 8.85*10**-12
eprime = 1.0
rho = 7*10**4
muprime = 1.0
tau = 1.0/(20.0*10**6)
eprime= 1.0
mu0 = 4*scipy.pi*10**-7
mur = 1.0

def k(freq):
    return 2*scipy.pi*freq/(3*10**8)

def er(freq):
    return eprime*complex(1,-rho/(2*scipy.pi*freq*eprime*e0))

##def mur(freq):
##    return (1 + (muprime/(1+1j*tau*freq)))

def kxn(n):
    return ((2*n + 1)*scipy.pi)/(2*a)

def kyn(n,freq):
    return ((er(freq)*mur - 1)*k(freq)**2 - kxn(n)**2)**0.5

def sh(n):
    return mpmath.sinh(kxn(n)*b)

def ch(n):
    return mpmath.cosh(kxn(n)*b)

def tn(n,freq):
    return mpmath.tan(kyn(n,freq)*(b-d))

def ct(n,freq):
    return mpmath.cot(kyn(n,freq)*(b-d))

def FX(n,freq):
    return (kxn(n)/k(freq))*((1+(mur*er(freq)))*sh(n)*ch(n))

def FY(n,freq):
    return (kyn(n,freq)/k(freq))*(mur*(sh(n)**2)*tn(n,freq) - er(freq)*(ch(n)**2)*ct(n,freq))

def FY_2(n,freq):
    return (kyn(n,freq)/k(freq))*(mur*(ch(n)**2)*tn(n,freq) - er(freq)*(sh(n)**2)*ct(n,freq))

def ferrite_long(freq):
    return (1j*Z0)/(2*a) * (mpmath.nsum(lambda n: (((FX(n,freq) +FY(n,freq))/(er(freq)*mur-1)) - ((k(freq)*sh(n)*ch(n))/kxn(n)))**(-1), [0,10]))

def ferrite_dip_horz(freq):
    return (1j*Z0)/(2*a) * mpmath.nsum(lambda n: (((kxn(n)**2)/k(freq))*(((FX(n,freq) + FY(n,freq))/(er(freq)*mur-1) - (k(freq)*sh(n)*ch(n)/kxn(n))))**-1), [0, 10])

def ferrite_dip_vert(freq):
    return (1j*Z0)/(2*a) * mpmath.nsum(lambda n: (((kxn(n)**2)/k(freq))*(((FX(n,freq) + FY_2(n,freq))/(er(freq)*mur-1) - (k(freq)*sh(n)*ch(n)/kxn(n))))**-1), [0, 10])

def ferrite_quad_horz(freq):
    return (-1j*Z0)/(2*a) * mpmath.nsum(lambda n: (((kxn(n)**2)/k(freq))*(((FX(n,freq) + FY(n,freq))/(er(freq)*mur-1) - (k(freq)*sh(n)*ch(n)/kxn(n))))**-1), [0, 10])

def ferrite_quad_vert(freq):
    return (1j*Z0)/(2*a) * mpmath.nsum(lambda n: (((kxn(n)**2)/k(freq))*(((FX(n,freq) + FY(n,freq))/(er(freq)*mur-1) - (k(freq)*sh(n)*ch(n)/kxn(n))))**-1), [0, 10])


start = time.time()

x= []
temp_real = []
temp_imag = []

freq_list = []
for i in range(0,9,1):
    for j in range(1,200,1):
        freq_list.append(float((j/10.0)*10.0**i))

freq_list = list(set(freq_list))
freq_list.sort()

for i in freq_list:
    temp_real.append(ferrite_long(i).real*length)
    temp_imag.append(ferrite_long(i).imag*length)
##    x.append(float(i)/10.0**9)

output = open('phase_1_moly_60mm_long.csv', 'w+')
for i in range(0,len(temp_real)):
              output.write(str(freq_list[i])+','+str(temp_real[i])+','+str(temp_imag[i])+'\n')

output.close()

pl.loglog(freq_list, temp_real, 'k-', label = "Real Longitudinal")
pl.loglog(freq_list, temp_imag, 'r-', label = "Imaginary Longitudinal")


pl.xlabel("Frequency (Hz)", fontsize=16)                  #Label axes
pl.ylabel("Impedance ($\Omega/m$)", fontsize=16)
pl.legend()
pl.savefig('phase_1_moly_60mm_long.pdf')
pl.clf()


temp_real = []
temp_imag = []

for i in freq_list:
    temp_real.append(ferrite_dip_vert(i).real*length*disp)
    temp_imag.append(ferrite_dip_vert(i).imag*length*disp)


output = open('phase_1_moly_60mm_dip_vert.csv', 'w+')
for i in range(0,len(temp_real)):
              output.write(str(freq_list[i])+','+str(temp_real[i])+','+str(temp_imag[i])+'\n')
output.close()

pl.loglog(freq_list, temp_real, 'k-', label = "Real Longitudinal")
pl.loglog(freq_list, temp_imag, 'r-', label = "Imaginary Longitudinal")

pl.xlabel("Frequency (Hz)", fontsize=16)                  #Label axes
pl.ylabel("Impedance ($\Omega/m^{2}$)", fontsize=16)
pl.legend()
pl.savefig('phase_1_moly_60mm_dip_vert.pdf')
pl.clf()


temp_real = []
temp_imag = []

for i in freq_list:
    temp_real.append(ferrite_dip_horz(i).real*length*disp)
    temp_imag.append(ferrite_dip_horz(i).imag*length*disp)


output = open('phase_1_moly_60mm_dip_horz.csv', 'w+')
for i in range(0,len(temp_real)):
              output.write(str(freq_list[i])+','+str(temp_real[i])+','+str(temp_imag[i])+'\n')
output.close()

pl.loglog(freq_list, temp_real, 'k-', label = "Real Longitudinal")
pl.loglog(freq_list, temp_imag, 'r-', label = "Imaginary Longitudinal")


pl.xlabel("Frequency (Hz)", fontsize=16)                  #Label axes
pl.ylabel("Impedance ($\Omega/m^{2}$)", fontsize=16)
pl.legend()
pl.savefig('phase_1_moly_60mm_dip_horz.pdf')
pl.clf()


temp_real = []
temp_imag = []

for i in freq_list:
    temp_real.append(abs(ferrite_quad_horz(i).real)*length*disp)
    temp_imag.append(abs(ferrite_quad_horz(i).imag)*length*disp)


output = open('phase_1_moly_60mm_quad_horz.csv', 'w+')
for i in range(0,len(temp_real)):
              output.write(str(freq_list[i])+','+str(temp_real[i])+','+str(temp_imag[i])+'\n')
output.close()

pl.loglog(freq_list, temp_real, 'k-', label = "Real Longitudinal")
pl.loglog(freq_list, temp_imag, 'r-', label = "Imaginary Longitudinal")


pl.xlabel("Frequency (Hz)", fontsize=16)                  #Label axes
pl.ylabel("Impedance ($\Omega/m^{2}$)", fontsize=16)
pl.legend()
pl.savefig('phase_1_moly_60mm_quad_horz.pdf')
pl.clf()

temp_real = []
temp_imag = []

for i in freq_list:
    temp_real.append(ferrite_quad_vert(i).real*length*disp)
    temp_imag.append(ferrite_quad_vert(i).imag*length*disp)


output = open('phase_1_moly_60mm_quad_vert.csv', 'w+')
for i in range(0,len(temp_real)):
              output.write(str(freq_list[i])+','+str(temp_real[i])+','+str(temp_imag[i])+'\n')
output.close()
pl.loglog(freq_list, temp_real, 'k-', label = "Real Longitudinal")
pl.loglog(freq_list, temp_imag, 'r-', label = "Imaginary Longitudinal")


pl.xlabel("Frequency (Hz)", fontsize=16)                  #Label axes
pl.ylabel("Impedance ($\Omega/m^{2}$)", fontsize=16)
pl.legend()
pl.savefig('phase_1_moly_60mm_quad_vert.pdf')
pl.clf()


temp_real = []
temp_imag = []

for i in freq_list:
    temp_real.append((ferrite_dip_vert(i)+ferrite_quad_vert(i)).real*length*disp)
    temp_imag.append((ferrite_dip_vert(i)+ferrite_quad_vert(i)).imag*length*disp)

output = open('phase_1_moly_60mm_vert_total.csv', 'w+')
for i in range(0,len(temp_real)):
              output.write(str(freq_list[i])+','+str(temp_real[i])+','+str(temp_imag[i])+'\n')

output.close()

pl.loglog(freq_list, temp_real, 'k-', label = "Real Longitudinal")
pl.loglog(freq_list, temp_imag, 'r-', label = "Imaginary Longitudinal")

pl.xlabel("Frequency (Hz)", fontsize=16)                  #Label axes
pl.ylabel("Impedance ($\Omega/m^{2}$)", fontsize=16)
pl.legend()
pl.savefig('phase_1_moly_60mm_vert_total.pdf')
pl.clf()


temp_real = []
temp_imag = []

for i in freq_list:
    temp_real.append(abs((ferrite_dip_horz(i).real+ferrite_quad_horz(i)).real)*length*disp)
    temp_imag.append(abs((ferrite_dip_horz(i).imag+ferrite_quad_horz(i)).imag)*length*disp)


output = open('phase_1_moly_60mm_horz_total.csv', 'w+')
for i in range(0,len(temp_real)):
              output.write(str(freq_list[i])+','+str(temp_real[i])+','+str(temp_imag[i])+'\n')

output.close()

pl.loglog(freq_list, temp_real, 'k-', label = "Real Longitudinal")
pl.loglog(freq_list, temp_imag, 'r-', label = "Imaginary Longitudinal")


pl.xlabel("Frequency (Hz)", fontsize=16)                  #Label axes
pl.ylabel("Impedance ($\Omega/m^{2}$)", fontsize=16)
pl.legend()
pl.savefig('phase_1_moly_60mm_horz_total.pdf')
pl.clf()

print time.time() - start
