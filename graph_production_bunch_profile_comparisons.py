import csv, os
import scipy as sp
import pylab as pl
import images2swf
import Image, time

start = time.time()

directory = "E:/PhD/1st_Year_09-10/Software/headtail/data_dump/hdtl-data/" #Directory of data
name = "hdtl-bt-BB-0-SC-1"
data_1 = directory+"hdtl-at-BB-10-SC-20.NumPar.130_bunchds.dat"
data_2 = directory+"hdtl-at-BB-10-SC-20.NumPar.140_bunchds.dat"
data_3 = directory+"hdtl-at-BB-10-SC-20.NumPar.150_bunchds.dat"

    
input_file = open(data_1, 'r+')
tar = input_file.readlines()
input_file.close()
while '\n' in tar:
    tar.remove('\n')
for i in range(0,len(tar)):
    tar[i] = tar[i].rstrip('\n')
    tar[i] = tar[i].split()
sep_parts = []
sep_parts.append(tar[-128:])
data_list_1=sp.array(sep_parts)

input_file = open(data_2, 'r+')
tar = input_file.readlines()
input_file.close()
while '\n' in tar:
    tar.remove('\n')
for i in range(0,len(tar)):
    tar[i] = tar[i].rstrip('\n')
    tar[i] = tar[i].split()
sep_parts = []
sep_parts.append(tar[-128:])
data_list_2=sp.array(sep_parts)

input_file = open(data_3, 'r+')
tar = input_file.readlines()
input_file.close()
while '\n' in tar:
    tar.remove('\n')
for i in range(0,len(tar)):
    tar[i] = tar[i].rstrip('\n')
    tar[i] = tar[i].split()
sep_parts = []
sep_parts.append(tar[-128:])
data_list_3=sp.array(sep_parts)


pl.plot(data_list_1[0,:,0], data_list_1[0,:,1], 'k-')
pl.plot(data_list_2[0,:,0], data_list_2[0,:,1], 'r-')
pl.plot(data_list_3[0,:,0], data_list_3[0,:,1], 'm-')
pl.grid(linestyle="--", which = "major")
pl.xlabel("$\sigma_{z}$",fontsize = 16)                  #Label axes
pl.ylabel("Occupation",fontsize = 16)
##pl.legend(loc = "upper left")
##    pl.axis([-2,2,0,1000000])
##pl.savefig(plot_dir+str(i)+".png")                  # Save fig for film
pl.show()
pl.clf()


##os.chdir(plot_dir)    
##os.system("del *.png")
    

print time.time()-start



