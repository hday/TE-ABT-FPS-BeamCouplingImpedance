import csv, time, os, sys
import scipy as sp
import pylab as pl
import scipy.optimize as op
import scipy.interpolate as interp
from scipy import signal as sg

####### Define constants for calculations ##########

C = 299792458.0
Z0 = 377
lenMKP = 3.423+(2*0.035)+(2*0.005)
Zc = 320.0

def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp

def eigenmodeGen(freqFile, qFile, rOverQFile):
    inputFreq=open(freqFile, "r+")
    datFreq=inputFreq.readlines()
    inputFreq.close()
    outputFreq=[]
    for line in datFreq[-20:-1]:
        temp=(line[25:].rstrip()).strip()
        outputFreq.append(float(temp))
    inputQ=open(qFile, "r+")
    datQ=inputQ.readlines()
    inputQ.close()
    outputQ=[]
    for line in datQ[-20:-1]:
        temp=(line[25:].rstrip()).strip()
        outputQ.append(float(temp))
    inputrOverQ=open(rOverQFile, "r+")
    datrOverQ=inputrOverQ.readlines()
    inputrOverQ.close()
    outputrOverQ=[]
    for line in datrOverQ[-20:-1]:
        temp=(line[25:].rstrip()).strip()
        outputrOverQ.append(float(temp))
    resDat=[]
    for i in range(0,len(outputFreq)):
        resDat.append([outputFreq[i], outputQ[i], outputrOverQ[i]])

    return resDat

def Z_bb(freq, data):
    return data[2]*data[1]/complex(1, data[1]*(freq/data[0] - data[0]/freq))

def gaussProf(freq, bLength):
    sigma = bLength/(4.0*2.0*sp.log(2.0))
    return sp.e**(-(2.0*sp.pi*freq*sigma)**2*(2**0.5))

def cosProf(freq, bunLength):
    return (sp.sin(sp.pi*freq*bunLength)/(sp.pi*freq*bunLength*(1-(freq*bunLength)**2)))**2


def readS21FromVNA(inputFile):
    tar=open(inputFile, "r+")
    inputData=tar.readlines()
    tar.close()
    outputData=[]
    for line in inputData[6:]:
        temp=line.split("\t")
        outputData.append(map(float,temp))
    return outputData

def logToLin(data):
    return 10**(data/20)

def linToLog(data):
    return 20*sp.log10(data)

def lumpImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return 2*Zc*(1-dataNorm)/dataNorm

def logImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return -2*Zc*sp.log(dataNorm)

targetDir = "E:/PhD/1st_Year_09-10/Data/MKIRFFingers/impedanceSimulations/"
impNoRFFingers = sp.array(extract_dat(targetDir+"longitudinalImpedanceCavityNoFingers.csv"))
impConnecteRFFingers10m = sp.array(extract_dat(targetDir+"longitudinalImpedanceRFFingers57mmAttached.csv"))
impConnecteRFFingers30m = sp.array(extract_dat(targetDir+"longitudinalImpedanceRFFingers57mmAttached30mWake.csv"))
imp1BrokenRFFinger5deg = sp.array(extract_dat(targetDir+"longitudinalImpedanceRFFingers57mm1Detached30mWake.csv"))
imp2BrokenRFFinger5deg = sp.array(extract_dat(targetDir+"longitudinalImpedanceRFFingers57mm2Detached10mWake.csv"))
imp3BrokenRFFinger5deg = sp.array(extract_dat(targetDir+"longitudinalImpedanceRFFingers57mm3Detached10mWake.csv"))
imp1BrokenRFFinger1deg = sp.array(extract_dat(targetDir+"longitudinalImpedanceRFFingers57mm1Detached1Degree10mWake.csv"))
imp2BrokenRFFinger1deg = sp.array(extract_dat(targetDir+"longitudinalImpedanceRFFingers57mm2Detached1Degree10mWake.csv"))
imp3BrokenRFFinger1deg = sp.array(extract_dat(targetDir+"longitudinalImpedanceRFFingers57mm3Detached1Degree10mWake.csv"))
imp1BrokenRFFinger1degSpring = sp.array(extract_dat(targetDir+"longitudinalImpedanceRFFingers57mm1Detached1Degree10mWakeLumpedCircuit.csv"))

resDirectory = "E:/PhD/1st_Year_09-10/Data/MKIRFFingers/eigenmode/"
res1Fing1Deg=eigenmodeGen(resDirectory+"1FingersSeperated1DegreeHighMeshDensity/frequencies.txt",
                          resDirectory+"1FingersSeperated1DegreeHighMeshDensity/q_1.txt",
                          resDirectory+"1FingersSeperated1DegreeHighMeshDensity/rOverQ.txt")
res3Fing1Deg=eigenmodeGen(resDirectory+"3FingersSeperated1Degree/frequencies.txt",
                          resDirectory+"3FingersSeperated1Degree/q_1.txt",
                          resDirectory+"3FingersSeperated1Degree/rOverQ.txt")
res1Fing5Deg=eigenmodeGen(resDirectory+"1FingersSeperated5DegreeHighMeshDensity/frequencies.txt",
                          resDirectory+"1FingersSeperated5DegreeHighMeshDensity/q_1.txt",
                          resDirectory+"1FingersSeperated5DegreeHighMeshDensity/rOverQ.txt")
res3Fing1Deg=eigenmodeGen(resDirectory+"3FingersSeperated1Degree/frequencies.txt",
                          resDirectory+"3FingersSeperated1Degree/q_1.txt",
                          resDirectory+"3FingersSeperated1Degree/rOverQ.txt")

##freqRangeRes[sp.linspace(10**6,2*10**9,2000):


targetDirWireMeasOne = "E:/PhD/1st_Year_09-10/Data/MKIRFFingers/Measurements/21-10-13/rffingersmki/ownCalculation/"
allFingersTouchMeas=sp.array(readS21FromVNA(targetDirWireMeasOne+"S21ALLFINGERSTOUCHING.S2P"))
AFingerIsolatedLipAndSpring=sp.array(readS21FromVNA(targetDirWireMeasOne+"S21ONEFINGERISOLATEDSPRINGISOLATEDATLIP.S2P"))
AFingerNotTouchSpring=sp.array(readS21FromVNA(targetDirWireMeasOne+"S21ONEFINGEROUTOFSPRING.S2P"))
AFingerNotTouchSpringIsolatedLip=sp.array(readS21FromVNA(targetDirWireMeasOne+"S21ONEFINGEROUTOFSPRINGISOLATEDATLIP.S2P"))
AFingerTouchSpringIsolatedLip=sp.array(readS21FromVNA(targetDirWireMeasOne+"S21ONEFINGERINSPRINGISOLATEDATLIP.S2P"))

targetDirWireMeasTwo = "E:/PhD/1st_Year_09-10/Data/MKIRFFingers/Measurements/21-10-13/rffingersmki/ownCalculation2Fingers/"
allFingersTouchMeasTwo=sp.array(readS21FromVNA(targetDirWireMeasTwo+"S212FINGERSALLCONNECTED.S2P"))
TwoFingerIsolatedLipAndSpring=sp.array(readS21FromVNA(targetDirWireMeasTwo+"S212FINGERSISOLATEDSPRINGISOLATEDATLIP.S2P"))
TwoFingerNotTouchSpring=sp.array(readS21FromVNA(targetDirWireMeasTwo+"S212FINGERSOUTOFSPRING.S2P"))
TwoFingerNotTouchSpringIsolatedLip=sp.array(readS21FromVNA(targetDirWireMeasTwo+"S212FINGERSOUTOFSPRINGISOLATEDATLIP.S2P"))
TwoFingerTouchSpringIsolatedLip=sp.array(readS21FromVNA(targetDirWireMeasTwo+"S212FINGERSINSPRINGISOLATEDATLIP.S2P"))

####### Plotting Simulations ########

##pl.semilogy()
##pl.plot(impNoRFFingers[:,0], abs(impNoRFFingers[:,1]), label = "No RF Fingers")
##pl.plot(impConnecteRFFingers10m[:,0], abs(impConnecteRFFingers10m[:,1]), label = "RF Fingers Good Contact 10m Wake")
pl.plot(impConnecteRFFingers30m[:,0], abs(impConnecteRFFingers30m[:,1]), label = "RF Fingers Good Contact 30m Wake")
##pl.plot(imp1BrokenRFFinger5deg[:,0], abs(imp1BrokenRFFinger5deg[:,1]), label = "RF Fingers 1 bad contact $ \\alpha =5^{\circ{}} $")
##pl.plot(imp2BrokenRFFinger5deg[:,0], abs(imp2BrokenRFFinger5deg[:,1]), label = "RF Fingers 2 bad contact $ \\alpha =5^{\circ{}} $")
##pl.plot(imp3BrokenRFFinger5deg[:,0], abs(imp3BrokenRFFinger5deg[:,1]), label = "RF Fingers 3 bad contact $ \\alpha =5^{\circ{}} $")
pl.plot(imp1BrokenRFFinger1deg[:,0], abs(imp1BrokenRFFinger1deg[:,1]), label = "RF Fingers 1 bad contact $ \\alpha =1^{\circ{}} $")
##pl.plot(imp2BrokenRFFinger1deg[:,0], abs(imp2BrokenRFFinger1deg[:,1]), label = "RF Fingers 2 bad contact $ \\alpha =1^{\circ{}} $")
##pl.plot(imp3BrokenRFFinger1deg[:,0], abs(imp3BrokenRFFinger1deg[:,1]), label = "RF Fingers 3 bad contact $ \\alpha =1^{\circ{}} $")
pl.plot(imp1BrokenRFFinger1degSpring[:,0], abs(imp1BrokenRFFinger1degSpring[:,1]), label = "RF Fingers 1 bad contact Spring $ \\alpha =1^{\circ{}} $")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$\Re{}e (Z_{\parallel})$ ($\Omega$)", fontsize=16.0)
pl.legend(loc="upper right")
pl.axis([0,2,0,3])
##pl.show()
pl.clf()

####### Plotting Measurements ########

complexAllFingersTouch = []
complexAFingerIsolatedLipAndSpring = []
complexAFingerNotTouchSpring = []
complexAFingerNotTouchSpringIsolatedLip = []
complexAFingerTouchSpringIsolatedLip = []
for i in range(0,len(allFingersTouchMeas[:,3])):
    complexAllFingersTouch.append(complex(allFingersTouchMeas[i,3]*sp.cos(allFingersTouchMeas[i,4]),allFingersTouchMeas[i,3]*sp.sin(allFingersTouchMeas[i,4])))
    complexAFingerIsolatedLipAndSpring.append(complex(linToLog(AFingerIsolatedLipAndSpring[i,3])*sp.cos(AFingerIsolatedLipAndSpring[i,4]),linToLog(AFingerIsolatedLipAndSpring[i,3])*sp.sin(AFingerIsolatedLipAndSpring[i,4])))
    complexAFingerNotTouchSpring.append(complex((AFingerNotTouchSpring[i,3])*sp.cos(AFingerNotTouchSpring[i,4]),(AFingerNotTouchSpring[i,3])*sp.sin(AFingerNotTouchSpring[i,4])))
    complexAFingerNotTouchSpringIsolatedLip.append(complex(linToLog(AFingerNotTouchSpringIsolatedLip[i,3])*sp.cos(AFingerNotTouchSpringIsolatedLip[i,4]),linToLog(AFingerNotTouchSpringIsolatedLip[i,3])*sp.sin(AFingerNotTouchSpringIsolatedLip[i,4])))
    complexAFingerTouchSpringIsolatedLip.append(complex(linToLog(AFingerTouchSpringIsolatedLip[i,3])*sp.cos(AFingerTouchSpringIsolatedLip[i,4]),linToLog(AFingerTouchSpringIsolatedLip[i,3])*sp.sin(AFingerTouchSpringIsolatedLip[i,4])))
            
complexAllFingersTouch=sp.array(complexAllFingersTouch)
complexAFingerIsolatedLipAndSpring=sp.array(complexAFingerIsolatedLipAndSpring)
complexAFingerNotTouchSpring=sp.array(complexAFingerNotTouchSpring)
complexAFingerNotTouchSpringIsolatedLip=sp.array(complexAFingerNotTouchSpringIsolatedLip)
complexAFingerTouchSpringIsolatedLip=sp.array(complexAFingerTouchSpringIsolatedLip)

complexAllFingersTouchTwo = []
complexTwoFingerIsolatedLipAndSpring = []
complexTwoFingerNotTouchSpring = []
complexTwoFingerNotTouchSpringIsolatedLip = []
complexTwoFingerTouchSpringIsolatedLip = []
for i in range(0,len(allFingersTouchMeas[:,3])):
    complexAllFingersTouchTwo.append(complex(allFingersTouchMeasTwo[i,3]*sp.cos(allFingersTouchMeasTwo[i,4]),allFingersTouchMeas[i,3]*sp.sin(allFingersTouchMeasTwo[i,4])))
    complexTwoFingerIsolatedLipAndSpring.append(complex(linToLog(TwoFingerIsolatedLipAndSpring[i,3])*sp.cos(TwoFingerIsolatedLipAndSpring[i,4]),linToLog(TwoFingerIsolatedLipAndSpring[i,3])*sp.sin(TwoFingerIsolatedLipAndSpring[i,4])))
    complexTwoFingerNotTouchSpring.append(complex((TwoFingerNotTouchSpring[i,3])*sp.cos(TwoFingerNotTouchSpring[i,4]),(TwoFingerNotTouchSpring[i,3])*sp.sin(TwoFingerNotTouchSpring[i,4])))
    complexTwoFingerNotTouchSpringIsolatedLip.append(complex(linToLog(TwoFingerNotTouchSpringIsolatedLip[i,3])*sp.cos(TwoFingerNotTouchSpringIsolatedLip[i,4]),linToLog(TwoFingerNotTouchSpringIsolatedLip[i,3])*sp.sin(TwoFingerNotTouchSpringIsolatedLip[i,4])))
    complexTwoFingerTouchSpringIsolatedLip.append(complex(linToLog(TwoFingerTouchSpringIsolatedLip[i,3])*sp.cos(TwoFingerTouchSpringIsolatedLip[i,4]),linToLog(TwoFingerTouchSpringIsolatedLip[i,3])*sp.sin(TwoFingerTouchSpringIsolatedLip[i,4])))
            
complexAllFingersTouchTwo=sp.array(complexAllFingersTouchTwo)
complexTwoFingerIsolatedLipAndSpring=sp.array(complexTwoFingerIsolatedLipAndSpring)
complexTwoFingerNotTouchSpring=sp.array(complexTwoFingerNotTouchSpring)
complexTwoFingerNotTouchSpringIsolatedLip=sp.array(complexTwoFingerNotTouchSpringIsolatedLip)
complexTwoFingerTouchSpringIsolatedLip=sp.array(complexTwoFingerTouchSpringIsolatedLip)

pl.plot(allFingersTouchMeas[:,0]/10**9, (allFingersTouchMeas[:,3]), label="All Fingers Touch")
pl.plot(AFingerIsolatedLipAndSpring[:,0]/10**9, linToLog(AFingerIsolatedLipAndSpring[:,3]), label="1 Finger Isolated Spr/Lip")
pl.plot(AFingerNotTouchSpring[:,0]/10**9, (AFingerNotTouchSpring[:,3]), label="1 Finger Free Spr")
pl.plot(AFingerNotTouchSpringIsolatedLip[:,0]/10**9, linToLog(AFingerNotTouchSpringIsolatedLip[:,3]), label="1 Finger Isolated Lip, free spring")
pl.plot(AFingerTouchSpringIsolatedLip[:,0]/10**9, linToLog(AFingerTouchSpringIsolatedLip[:,3]), label="All Fingers Touch Spring, isolated lip")
##pl.plot(allFingersTouchMeasTwo[:,0]/10**9, linToLog(allFingersTouchMeasTwo[:,3]), label="All Fingers Touch")
##pl.plot(TwoFingerIsolatedLipAndSpring[:,0]/10**9, linToLog(TwoFingerIsolatedLipAndSpring[:,3]), label="2 Finger Isolated Spr/Lip")
##pl.plot(TwoFingerNotTouchSpring[:,0]/10**9, linToLog(TwoFingerNotTouchSpring[:,3]), label="2 Finger Free Spr")
##pl.plot(TwoFingerNotTouchSpringIsolatedLip[:,0]/10**9, linToLog(TwoFingerNotTouchSpringIsolatedLip[:,3]), label="2 Finger Isolated Lip, free spring")
##pl.plot(TwoFingerTouchSpringIsolatedLip[:,0]/10**9, linToLog(TwoFingerTouchSpringIsolatedLip[:,3]), label="All Fingers Touch Spring, 2 isolated lip")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$S_{21}$ (dB)", fontsize=16.0)
pl.legend(loc="upper left")
##pl.show()
pl.clf()

lumpedOneFingerIsolatedLipAndSpring = lumpImpFormula(AFingerIsolatedLipAndSpring[:,3], logToLin(allFingersTouchMeas[:,3]), 300)
lumpedOneFingerNotTouchSpring = lumpImpFormula(logToLin(AFingerNotTouchSpring[:,3]), logToLin(allFingersTouchMeas[:,3]), 300)
lumpedOneFingerNotTouchSpringIsolatedLip = lumpImpFormula(AFingerNotTouchSpringIsolatedLip[:,3], logToLin(allFingersTouchMeas[:,3]), 300)
lumpedOneFingerTouchSpringIsolatedLip = lumpImpFormula(AFingerTouchSpringIsolatedLip[:,3], logToLin(allFingersTouchMeas[:,3]), 300)
lumpedTwoFingerIsolatedLipAndSpring = lumpImpFormula(TwoFingerIsolatedLipAndSpring[:,3], (allFingersTouchMeasTwo[:,3]), 300)
lumpedTwoFingerNotTouchSpring = lumpImpFormula(TwoFingerNotTouchSpring[:,3], (allFingersTouchMeasTwo[:,3]), 300)
lumpedTwoFingerNotTouchSpringIsolatedLip = lumpImpFormula(TwoFingerNotTouchSpringIsolatedLip[:,3], (allFingersTouchMeasTwo[:,3]), 300)
lumpedTwoFingerTouchSpringIsolatedLip = lumpImpFormula(TwoFingerTouchSpringIsolatedLip[:,3], (allFingersTouchMeasTwo[:,3]), 300)

distOneFingerIsolatedLipAndSpring = logImpFormula(AFingerIsolatedLipAndSpring[:,3], logToLin(allFingersTouchMeas[:,3]), 300)
distOneFingerNotTouchSpring = logImpFormula(logToLin(AFingerNotTouchSpring[:,3]), logToLin(allFingersTouchMeas[:,3]), 300)
distOneFingerNotTouchSpringIsolatedLip = logImpFormula(AFingerNotTouchSpringIsolatedLip[:,3], logToLin(allFingersTouchMeas[:,3]), 300)
distOneFingerTouchSpringIsolatedLip = logImpFormula(AFingerTouchSpringIsolatedLip[:,3], logToLin(allFingersTouchMeas[:,3]), 300)
distTwoFingerIsolatedLipAndSpring = logImpFormula(TwoFingerIsolatedLipAndSpring[:,3], (allFingersTouchMeasTwo[:,3]), 300)
distTwoFingerNotTouchSpring = logImpFormula(TwoFingerNotTouchSpring[:,3], (allFingersTouchMeasTwo[:,3]), 300)
distTwoFingerNotTouchSpringIsolatedLip = logImpFormula(TwoFingerNotTouchSpringIsolatedLip[:,3], (allFingersTouchMeasTwo[:,3]), 300)
distTwoFingerTouchSpringIsolatedLip = logImpFormula(TwoFingerTouchSpringIsolatedLip[:,3], (allFingersTouchMeasTwo[:,3]), 300)


##pl.plot(allFingersTouchMeas[:,0]/10**9, lumpedOneFingerIsolatedLipAndSpring, label="1 Finger unbound by spring, isolated at lip and at spring")
##pl.plot(allFingersTouchMeas[:,0]/10**9, lumpedOneFingerNotTouchSpring, label="1 Finger unbound by spring, no isolation")
##pl.plot(allFingersTouchMeas[:,0]/10**9, lumpedOneFingerNotTouchSpringIsolatedLip, label="1 Finger unbound by spring, isolated at lip")
##pl.plot(allFingersTouchMeas[:,0]/10**9, lumpedOneFingerTouchSpringIsolatedLip, label="1 Finger unbound by spring but touching, isolated at lip")
##pl.plot(allFingersTouchMeas[:,0]/10**9, lumpedTwoFingerIsolatedLipAndSpring , label="2 Fingers unbound by spring, isolated at lip and at spring")
##pl.plot(allFingersTouchMeas[:,0]/10**9, lumpedTwoFingerNotTouchSpring, label="2 Fingers unbound by spring, no isolation")
##pl.plot(allFingersTouchMeas[:,0]/10**9, lumpedTwoFingerNotTouchSpringIsolatedLip, label="2 Fingers unbound by spring, isolated at lip")
##pl.plot(allFingersTouchMeas[:,0]/10**9, lumpedTwoFingerTouchSpringIsolatedLip, label="2 Fingers unbound by spring but touching, isolated at lip")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$Z_{\parallel}$ Lumped Formula ($\Omega$)", fontsize=16.0)
pl.legend(loc="lower left")
pl.axis([0.7,0.9,-10,30])
##pl.show()
pl.clf()

##pl.plot(allFingersTouchMeas[:,0]/10**9, distOneFingerIsolatedLipAndSpring, label="1 Finger unbound by spring, isolated at lip and at spring")
##pl.plot(allFingersTouchMeas[:,0]/10**9, distOneFingerNotTouchSpring, label="1 Finger unbound by spring, no isolation")
pl.plot(allFingersTouchMeas[:,0]/10**9, distOneFingerNotTouchSpringIsolatedLip, label="1 Finger unbound by spring, isolated at lip")
##pl.plot(allFingersTouchMeas[:,0]/10**9, distOneFingerTouchSpringIsolatedLip, label="1 Finger unbound by spring but touching, isolated at lip")
##pl.plot(allFingersTouchMeas[:,0]/10**9, distTwoFingerIsolatedLipAndSpring , label="2 Fingers unbound by spring, isolated at lip and at spring")
##pl.plot(allFingersTouchMeas[:,0]/10**9, distTwoFingerNotTouchSpring, label="2 Fingers unbound by spring, no isolation")
pl.plot(allFingersTouchMeas[:,0]/10**9, distTwoFingerNotTouchSpringIsolatedLip, label="2 Fingers unbound by spring, isolated at lip")
##pl.plot(allFingersTouchMeas[:,0]/10**9, distTwoFingerTouchSpringIsolatedLip, label="2 Fingers unbound by spring but touching, isolated at lip")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$Z_{\parallel}$ Distributed Formula ($\Omega$)", fontsize=16.0)
pl.legend(loc="lower left")
pl.axis([0.7,0.9,-20,60])
pl.show()
pl.clf()

freqList=sp.linspace(1,5000*10**6,50000)
gaussList = gaussProf(freqList, 1.201*10.0**-9)

##pl.semilogy()
pl.plot(freqList/10**9, linToLog(gaussList**0.5))
pl.axis([0,3.5,-60,0])
##pl.show()
pl.clf()

fRev=(3.0*10**8)/27000
Nb=1.6*10.0**11
nBunches=1380
e=1.6*10.0**-19
print (fRev*Nb*nBunches*e)

powerLoss1FingIsoLipSpring = 2*(fRev*Nb*nBunches*e)**2*0.032*24
powerLoss1FingIsoLipFreeSpring = 2*(fRev*Nb*nBunches*e)**2*0.032*21.4
powerLoss2FingIsoLipSpring = 2*(fRev*Nb*nBunches*e)**2*0.032*45
powerLoss2FingLipFreeSpring = 2*(fRev*Nb*nBunches*e)**2*0.032*56

powerLoss1Fing1Deg = 2*(fRev*Nb*nBunches*e)**2*gaussProf(520.0*10**6,10.0**-9)**2*3
powerLoss3Fing1Deg = 2*(fRev*Nb*nBunches*e)**2*gaussProf(600.0*10**6,10.0**-9)**2*6.5
powerLoss1Fing5Deg = 2*(fRev*Nb*nBunches*e)**2*gaussProf(876.0*10**6,10.0**-9)**2*5
powerLoss3Fing5Deg = 2*(fRev*Nb*nBunches*e)**2*gaussProf(780.0*10**6,10.0**-9)**2*13
powerLoss1Fing1DegSpring = 2*(fRev*Nb*nBunches*e)**2*gaussProf(500.0*10**6,10*.0*-9)**2*2.25
print powerLoss1FingIsoLipSpring, powerLoss1FingIsoLipFreeSpring, powerLoss2FingIsoLipSpring ,powerLoss2FingLipFreeSpring
print powerLoss1Fing1Deg, powerLoss3Fing1Deg, powerLoss1Fing5Deg , powerLoss3Fing5Deg, powerLoss1Fing1DegSpring
