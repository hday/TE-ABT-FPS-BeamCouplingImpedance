import scipy as sp
import scipy.optimize as op
import pylab as pl
import time, csv, os, sys

dirHead="E:/PhD/1st_Year_09-10/Data/CLICStripline/singleWireDisplace/"

def gaussProf(freq, bLength):
    sigma = bLength/(4.0*2.0*sp.log(2.0))
    return sp.e**(-(2.0*sp.pi*freq*sigma)**2)

def cosProf(freq, bunLength):
    return (sp.sin(sp.pi*freq*bunLength)/(sp.pi*freq*bunLength*(1-(freq*bunLength)**2)))**2
                                                    
def logToLin(data):
    return 10**(data/20)

def linToLog(data):
    return 20*sp.log10(data)

def lumpImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return 2*Zc*(1-dataNorm)/dataNorm

def logImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return -2*Zc*sp.log(dataNorm)

def readS21FromVNA(inputFile):
    tar=open(inputFile, "r+")
    inputData=tar.readlines()
    tar.close()
    outputData=[]
    for line in inputData[6:]:
        temp=line.split("\t")
        outputData.append(map(float,temp))
    return outputData

def fitQuadraticSquaresImp(sParaData, dispWire):
    impLongDat=[]
    impTransDat=[]
    quadFunc = lambda p, x: p[0] + x*p[1] + x**2*p[2]
    errFunc = lambda p, x, z: (z-quadFunc(p,x))
    try:
        os.mkdir("E:/PhD/1st_Year_09-10/Data/CLICStripline/singleWireDisplace/plots")
    except:
        pass
    for i in range(0,len(sParaData[0])):
        fitDataReal = sp.array(logImpFormula(logToLin(sParaData[:,i,3]),1,235))
        initVal = [-1.0,1.0,1.0]
        pReal, pFunc = op.leastsq(errFunc, initVal, args=(dispWire, fitDataReal), full_output=0)
        impLongDat.append([sParaData[0,i,0], pReal[0]])
        totTrans = (pReal[2]*C)/(2*sp.pi*sParaData[0,i,0])
        impTransDat.append([sParaData[0,i,0], totTrans])
        dispPlot = sp.linspace(dispWire[0]-0.005,dispWire[-1]+0.005,100)
##        pl.plot(dispWire,fitDataReal)
##        pl.plot(dispPlot, quadFunc(pReal,dispPlot))
##        pl.savefig("E:/PhD/1st_Year_09-10/Data/CLICStripline/singleWireDisplace/plots/plotImp"+str(i)+".png")
##        pl.clf()
        
    return sp.array(impLongDat), sp.array(impTransDat)

def impTwoWire(sParaData):
    impData=logImpFormula(logToLin(sParaData[:,3]),1,180)
    normImp=[]
    for i in range(0,len(sParaData)):
        normImp.append([sParaData[i,0],(impData[i]*C)/(2*sp.pi*sParaData[i,0]*0.014**2)-(logImpFormula(logToLin(sParaData[0,3]),1,180)*C)/(2*sp.pi*sParaData[i,0]*0.014**2)])
    return sp.array(normImp)

C = 299792458.0
mu0=4*sp.pi*10.0**-7
eps0= 8.854187817*10.0**-12
dAper = 0.045
rWire = 0.0005

fileList = ["E:/PhD/1st_Year_09-10/Data/CLICStripline/singleWireDisplace/POINT"+str(i)+".S2P" for i in range(0,5,1)]
fileListBis = ["E:/PhD/1st_Year_09-10/Data/CLICStripline/singleWireDisplace/POINT"+str(i)+"BIS.S2P" for i in range(4,0,-1)]

fileList = fileListBis+fileList

dataTemp = []
for fileEn in fileList:
    dataTemp.append(readS21FromVNA(fileEn))

dataTransFact = sp.array(dataTemp)

fileLoadList = ["E:/PhD/1st_Year_09-10/Data/CLICStripline/singleWireDisplace/POINT"+str(i)+"LOAD.S2P" for i in range(0,5,1)]
fileLoadListBis = ["E:/PhD/1st_Year_09-10/Data/CLICStripline/singleWireDisplace/POINT"+str(i)+"BISLOAD.S2P" for i in range(4,0,-1)]

fileLoadList = fileLoadListBis+fileLoadList

dataLoadTemp = []
for fileEn in fileLoadList:
    dataLoadTemp.append(readS21FromVNA(fileEn))

dataTransLoadFact = sp.array(dataLoadTemp)


fileTwoHorz = "E:/PhD/1st_Year_09-10/Data/CLICStripline/singleWireDisplace/transversetwowires/horizontalplane/TWOWIRESIDEAL.S2P"
fileTwoVert = "E:/PhD/1st_Year_09-10/Data/CLICStripline/singleWireDisplace/transversetwowires/verticalplane/TWOWIRESIDEALVERT.S2P"

datTwoHorz = sp.array(readS21FromVNA(fileTwoHorz))
datTwoVert = sp.array(readS21FromVNA(fileTwoVert))

impTwoHorz=impTwoWire(datTwoHorz)
impTwoVert=impTwoWire(datTwoVert)

wireDisplacements=sp.array([-0.02,-0.015,-0.01,-0.005,0,0.005,0.01,0.015,0.02])

longitudinalCLIC, totTransCLIC = fitQuadraticSquaresImp(dataTransFact, wireDisplacements)

longitudinalCLICLoad, totTransCLICLoad = fitQuadraticSquaresImp(dataTransLoadFact, wireDisplacements)

for i in range(0,len(dataTransFact)):
    pl.plot(dataTransFact[i,:,0],dataTransFact[i,:,3])
pl.xlabel("Frequency (Hz)", fontsize=16.0)
pl.ylabel("S$_{21}$ (dB)", fontsize=16.0)
##pl.show()
pl.clf()

for i in range(0,len(dataTransFact)):
    pl.plot(dataTransFact[i,:,0],dataTransFact[i,:,4])
##pl.show()
pl.clf()


##pl.plot(longitudinalCLIC[:,0]/10**9,longitudinalCLIC[:,1]-longitudinalCLIC[0,1], label="Constant Term from fitting No Load")
pl.plot(dataTransFact[4,:,0]/10**9,logImpFormula(logToLin(dataTransFact[4,:,3]),1,180)-logImpFormula(logToLin(dataTransFact[4,0,3]),1,180), label="Measurement at 0mm displacement No Load")
##pl.plot(longitudinalCLICLoad[:,0]/10**9,longitudinalCLICLoad[:,1]-longitudinalCLICLoad[0,1], label="Constant Term from fitting Load")
pl.plot(dataTransLoadFact[4,:,0]/10**9,logImpFormula(logToLin(dataTransLoadFact[4,:,3]),1,235)-logImpFormula(logToLin(dataTransFact[4,0,3]),1,235), label="Measurement at 0mm displacement Load")
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("Z$_{\parallel}$ ($\Omega$)", fontsize=16.0)
pl.legend(loc="upper left")
pl.xlim(0,1.0)
pl.ylim(0,300)
pl.show()
pl.clf()

pl.plot(totTransCLIC[:,0]/10**9,totTransCLIC[:,1]/10**3)
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("Z$_{\perp}$$^{Total}$ (k$\Omega$/m)", fontsize=16.0)
##pl.show()
pl.clf()

pl.plot(datTwoHorz[:,0]/10**9,datTwoHorz[:,3])
pl.plot(datTwoVert[:,0]/10**9,datTwoVert[:,3])
pl.xlabel("Frequency (Hz)", fontsize=16.0)
pl.ylabel("S$_{21}$ (dB)", fontsize=16.0)
##pl.show()
pl.clf()

##pl.semilogy()
##pl.plot(totTransCLIC[:,0]/10**9,totTransCLIC[:,1]/10**3)
pl.plot(totTransCLICLoad[:,0]/10**9,totTransCLICLoad[:,1]/10**3)
pl.plot(impTwoHorz[:,0]/10**9,impTwoHorz[:,1]/10**3)
pl.plot(totTransCLICLoad[:,0]/10**9,(totTransCLICLoad[:,1]-impTwoHorz[:,1])/10**3)
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("Z$_{\perp}$$^{Dip}$ (k$\Omega$/m)", fontsize=16.0)
##pl.show()
pl.clf()


pl.plot(impTwoVert[:,0]/10**9,impTwoVert[:,1]/10**3)
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("Z$_{\perp}$$^{Dip}$ (k$\Omega$/m)", fontsize=16.0)
##pl.show()
pl.clf()
