import csv
import scipy as sp
import pylab as pl

directory = "C:/Users/hday/Documents/PhD/1st_Year_09-10/Software/headtail/data_dump/comp_wake_meth/" #Directory of data
list_bt_no_sc = [directory+"hdtl-bt-no-sc.NumPar."+str(i)+".cfg_prt.dat" for i in range(1,31)]
list_bt_sc = [directory+"hdtl-bt-sc.NumPar."+str(i)+".cfg_prt.dat" for i in range(1,31)]
list_bt_sc_no_bb =  [directory+"hdtl-bt-sc-no-BB.NumPar."+str(i)+".cfg_prt.dat" for i in range(1,31)]
list_at_no_sc = [directory+"hdtl-at-no-sc.NumPar."+str(i)+".cfg_prt.dat" for i in range(1,31)]
list_at_sc = [directory+"hdtl-at-sc.NumPar."+str(i)+".cfg_prt.dat" for i in range(1,31)]
list_at_sc_no_bb =  [directory+"hdtl-at-sc-no-BB.NumPar."+str(i)+".cfg_prt.dat" for i in range(1,31)]


value_desired = 9
big_bugger =[]
data_list_1 = []
for data_raw in list_bt_no_sc:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    data_list_1.append(append_tar[value_desired])
    
big_bugger.append(data_list_1)



data_list_1 = []
for data_raw in list_bt_sc:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    data_list_1.append(append_tar[value_desired])
    
big_bugger.append(data_list_1)
    



data_list_1 = []
for data_raw in list_bt_sc_no_bb:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    data_list_1.append(append_tar[value_desired])


big_bugger.append(data_list_1)



data_list_1 = []
for data_raw in list_at_no_sc:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    data_list_1.append(append_tar[value_desired])
    
big_bugger.append(data_list_1)



data_list_1 = []
for data_raw in list_at_sc:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    data_list_1.append(append_tar[value_desired])
    
big_bugger.append(data_list_1)
    



data_list_1 = []
for data_raw in list_at_sc_no_bb:
    input_file = open(data_raw, 'r+')
    linelist = input_file.readlines()
    input_file.close()
    row=linelist[-1]
    append_tar = map(float, row.split())
    data_list_1.append(append_tar[value_desired])


big_bugger.append(data_list_1)



big_bugger = sp.array(big_bugger)

x_values = sp.array(range(1,31))

pl.plot(x_values, big_bugger[0,:], 'k-', label = "Below Transition - NoSC/BB")                    # plot fitted curve
pl.plot(x_values, big_bugger[1,:],'k:', label = "Below Transition - SC/BB")
pl.plot(x_values, big_bugger[2,:],'k-.', label = "Below Transition - SC/noBB")
pl.plot(x_values, big_bugger[3,:], 'r-', label = "Above Transition - NoSC/BB")                    # plot fitted curve
pl.plot(x_values, big_bugger[4,:],'r:', label = "Above Transition - SC/BB")
pl.plot(x_values, big_bugger[5,:],'r-.', label = "Above Transition - SC/noBB")



pl.grid(linestyle="--", which = "major")
pl.xlabel("Bunch Intensity ($10^{10}$)",fontsize = 16)                  #Label axes
pl.ylabel("RMS Bunch Length $\sigma_{z}$ $(m)$",fontsize = 16)
pl.title("", fontsize = 16)
pl.legend(loc = "upper left")
##pl.axis([0,5,0.19,0.2])
pl.savefig(directory+"bt-at-rms-bunch-length"+".pdf")                  # Save fig for film
pl.show()
pl.clf()   

##polycoeffs = sp.polyfit(x_values,big_bugger[2,:],1)
##print polycoeffs
##yfit=sp.polyval(polycoeffs,x_values)
##pl.plot(x_values, big_bugger[2,:])
##pl.plot(x_values, yfit)
##pl.show()
##pl.savefig(directory+"moo.pdf")
##pl.clf()
##        

