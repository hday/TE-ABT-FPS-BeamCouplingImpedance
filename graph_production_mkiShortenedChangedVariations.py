import csv, os, sys
import scipy as sp
import pylab as pl
import scipy.optimize as op

C = 299792458.0

def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp

def peak_fit(data):
    lorentz_fit = lambda p, f: p[1]/(1 + (p[2]*(f/p[0] - p[0]/f))**2) ##### A lorentzian fitting function
    lorentz_errfunc = lambda p, f, y: (y-lorentz_fit(p, f))
    err_factor = 0.01

    peak_freq = []       

    lower_bound = 50
    upper_bound = len(data[:,0])
    x_val = data[lower_bound:upper_bound,0]
    y_val = data[lower_bound:upper_bound,1]

    ####### fit lorentzian data ###########

    current_max_y = 0.0
    current_max_x = 0
    for i in range(0, len(x_val)):
        if y_val[i] > current_max_y:
            current_max_y = y_val[i]
            current_max_x = i
        elif y_val[i] < current_max_y and i-current_max_x > 50 and y_val[current_max_x]>5:
            peak_freq.append([current_max_x, x_val[current_max_x], y_val[current_max_x]])
            current_max_y = 0
        else:
            pass
    fit_data = []
    old_upper = 100

    for res in peak_freq:
        pinit = [0.2, 10.0, 10.0]
        lower_bound = res[0]-50
        upper_bound = res[0]+50
        ###### Check to ensure no under or overrun
        if lower_bound < 0:
            lower_bound = 0
        if upper_bound > (len(data)-1):
            upper_bound = (len(data)-1)
    ##    ####### Make sure no overlap with other peaks
    ##    if lower_bound < old_upper and len(fit_data) > 0:
    ##        lower_bound = old_upper-1
        x_val = data[lower_bound:upper_bound,0]
        y_val = data[lower_bound:upper_bound,1]
        pfinal, pcov = op.leastsq(lorentz_errfunc, pinit, args = (x_val, y_val))   
        fit_data.append(pfinal)
        old_upper = upper_bound

    return fit_data, peak_freq

def heatCalc(Z, distribution, bLength, Ib):
    if distribution == "gaussian":
        dist = lambda bLength: sp.e**(2*sp.pi*Z[0]*bLength)**2
    elif distribution == "cos":
        dist = lambda bLength: sp.sin(2*sp.pi*Z[0]*bLength)/(2*sp.pi*Z[0]*bLength*(1-Z[0]*bLength)**2)**2
    else:
        print "Not a supported distrubution"
    return 2*Ib**2*dist(bLength)**2*Z[1]

def gaussProf(freq, bLength):
    sigma = bLength/(4.0*2.0*sp.log(2.0))
    return sp.e**(-(2.0*sp.pi*freq*sigma)**2)

def cosProf(freq, bunLength):
    return (sp.sin(sp.pi*freq*bunLength)/(sp.pi*freq*bunLength*(1-(freq*bunLength)**2)))**2
                                                    

disp = 0.001
length = 2.88
f_rev = 3*10**8/27000.0
nBunches = 1380
qPart = 1.6*10.0**-19
nPart=1.6*10.0**11
bCur = nPart*qPart*nBunches*f_rev
bLength=1.001*10.0**-9
testImp = [250*10**6, 10.0, 10000]
print bCur

resFreqs = [C/((10**0.5)*2*(i+1.25*0.007)) for i in sp.linspace(0.08,0.12,5)]
print resFreqs
directory = "E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/shortened_end_piece_impedance/"#Directory of data
##listDir = [directory+i for i in os.listdir(directory)]
listDir = [directory+str(i)+"mm_metallization_56mm_tube/" for i in range(130, 180,10)]

fileNam = "/longitudinal-impedance.csv"
lossGauss = []
lossCos = []
lossGaussRes = []
lossCosRes = []

plotList=[]

for tarFile in listDir:
    tmp = sp.array(extract_dat(tarFile+fileNam))
##    tmp = sp.array(extract_dat(tarFile))
    resDat, peakCount = peak_fit(tmp)
    powLossGauss=0.0
    powLossCos=0.0
    powLossResGauss = 0.0
    powLossResCos = 0.0
    for count in range(20,len(tmp),20):
        gaussAdd = 2*(bCur**2)*tmp[count,1]*(gaussProf(tmp[count,0]*10.0**9,bLength))**2
        powLossGauss+=gaussAdd
##        print tmp[count,0]*10.0**9
##        print cosProf(tmp[count,0]*10.0**9, bLength)
        cosAdd = 2*(bCur**2)*tmp[count,1]*(cosProf(tmp[count,0]*10.0**9,bLength))**2
        powLossCos+=cosAdd
    plotList.append([tmp[:,0],tmp[:,1],tmp[:,3]])
    lossGauss.append(powLossGauss)
    lossCos.append(powLossCos)
    for entry in peakCount:
        powLossResGauss+=2*(bCur**2)*entry[2]*(gaussProf(entry[1]*10.0**9,bLength))**2
        powLossResCos+=2*(bCur**2)*entry[2]*(cosProf(entry[1]*10.0**9,bLength))**2
    lossGaussRes.append(powLossResGauss)
    lossCosRes.append(powLossResCos)

##tmp = sp.array(extract_dat(listDir[-1]+"/longitudinal-impedance15m.csv"))
##tmp = sp.array(extract_dat(tarFile))
##resDat, peakCount = peak_fit(tmp)
##powLossGauss=0.0
##powLossCos=0.0
##powLossResGauss = 0.0
##powLossResCos = 0.0
##for count in range(40,len(tmp),40):
##    gaussAdd = 2*(bCur**2)*tmp[count,1]*(gaussProf(tmp[count,0]*10.0**9,bLength))**2
##    powLossGauss+=gaussAdd
####        print tmp[count,0]*10.0**9
####        print cosProf(tmp[count,0]*10.0**9, bLength)
##    cosAdd = 2*(bCur**2)*tmp[count,1]*(cosProf(tmp[count,0]*10.0**9,bLength))**2
##    powLossCos+=cosAdd
##plotList.append([tmp[:,0],tmp[:,1],tmp[:,3]])
##lossGauss.append(powLossGauss)
##lossCos.append(powLossCos)
##for entry in peakCount:
##    powLossResGauss+=2*(bCur**2)*entry[2]*(gaussProf(entry[1]*10.0**9,bLength))**2
##    powLossResCos+=2*(bCur**2)*entry[2]*(cosProf(entry[1]*10.0**9,bLength))**2
##lossGaussRes.append(powLossResGauss)
##lossCosRes.append(powLossResCos)

plotList=sp.array(plotList)

freqList = []
for i in range(6,10):
    for j in range(1,11):
        freqList.append(j*(10.0**i))

freqList = list(set(freqList))
freqList.sort()

beamCurGauss = []
beamCurCos = []

print lossGauss
print lossGaussRes
print lossCos
print lossCosRes
##resDat=sp.array(resDat)
##print len(resDat)
##print resDat
##print peakCount

directory = "E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/full-device-impedances/" #Directory of data
without_metallisation = "without-metalisation/longitudinal-impedance.csv"
screen_conductors_15 = "with-15-screen-conductors/longitudinal-impedance.csv"
alternating_length_19 = "19_conductors_alternating/longitudinal-impedance.csv"

data_screen_conductors_15 = sp.array(extract_dat(directory+screen_conductors_15))
data_19_alternating = sp.array(extract_dat(directory+alternating_length_19))

print plotList[1,:][1]

##pl.semilogx()
pl.plot(plotList[0,0,:], plotList[0,1,:], 'k-', label="80mm overlap")
pl.plot(plotList[1,0,:], plotList[1,1,:], 'r-', label="90mm overlap")
pl.plot(plotList[2,0,:], plotList[2,1,:], 'b-', label="100mm overlap")
pl.plot(plotList[3,0,:], plotList[3,1,:], 'm-', label="110mm overlap")
pl.plot(plotList[4,0,:], plotList[4,1,:], 'g-', label="120mm overlap")
for freq in resFreqs:
    pl.axvline(freq/10**9)
pl.xlabel("Frequency (GHz)", fontsize="16.0")
pl.ylabel("$\Re{}e (Z_{\parallel})$ ($\Omega$)", fontsize="16.0")
pl.legend(loc="upper left")
pl.grid(True)
pl.axis([0,0.6,0,60])
pl.show()
pl.clf()

pl.plot(plotList[0,0,:], plotList[0,2,:], 'k-', label="24 Screen Conductors (Equal Length)")
pl.plot(plotList[1,0,:], plotList[1,2,:], 'r-', label="24 Screen Conductors (Alternative Design) Fig. 8.30")
pl.plot(plotList[2,0,:], plotList[2,2,:], 'b-', label="24 Screen Conductors (Enclosed Conductors) Fig. 8.29")
pl.plot(plotList[3,0,:], plotList[3,2,:], 'm-', label="24 Screen Conductors (Alternating Length 20mm) Fig. 8.26")
pl.plot(plotList[4,0,:], plotList[4,2,:], 'g-', label="24 Screen Conductors (Tapered Length) Fig. 8.27")
pl.plot(plotList[5,0,:], plotList[5,2,:], 'k--', label="24 Screen Conductors (Tapered and Alternating) Fig. 8.28")
pl.plot(plotList[6,0,:], plotList[6,2,:], 'r--', label="24 Screen Conductors (Final Design) Fig. 8.32")
##pl.plot(plotList[6,0,:], plotList[6,2,:], 'r--', label="24 Screen Conductors (Final Design)")
pl.plot(plotList[6,0,:], plotList[7,2,:], 'k-', label="24 Screen Conductors (Final Design longer overlap)")
pl.xlabel("Frequency (GHz)", fontsize="16.0")
pl.ylabel("$\Im{}m (Z_{\parallel})$ ($\Omega$)", fontsize="16.0")
pl.legend(loc="upper right")
pl.grid(True)
pl.axis([0,2.5,0,1400])
##pl.show()
pl.clf()


pl.plot(lossGauss, label="Broadband heating, gaussian profile")
pl.plot(lossGaussRes, label="Resonance heating, gaussian profile")
##pl.show()
pl.clf()

tag=0
bunLens = [(i/10.0+0.001)*10.0**-9 for i in range(5,15,1)]

powTotals = []
powFreq = []
for length in bunLens:
    totTot = []
    totFreq = []
    lossGauss = []
    lossCos = []
    lossGaussRes = []
    lossCosRes = []
    lossGaussCum=[]
    for tarFile in listDir:
        gaussStore=[]
        tmp = sp.array(extract_dat(tarFile+fileNam))
        resDat, peakCount = peak_fit(tmp)
        powLossGauss=0.0
        powLossCos=0.0
        powLossResGauss = 0.0
        powLossResCos = 0.0
        for count in range(40,len(tmp),40):
            gaussAdd = 2*(bCur**2)*abs(tmp[count,1])*(gaussProf(tmp[count,0]*10.0**9,length))**2
            powLossGauss+=gaussAdd
            gaussStore.append(gaussAdd)
    ##        print tmp[count,0]*10.0**9
    ##        print cosProf(tmp[count,0]*10.0**9, bLength)
            cosAdd = 2*(bCur**2)*abs(tmp[count,1])*(cosProf(tmp[count,0]*10.0**9,length))**2
            powLossCos+=cosAdd
        lossGauss.append(powLossGauss)
        lossCos.append(powLossCos)
        for entry in peakCount:
            powLossResGauss+=2*(bCur**2)*entry[2]*(gaussProf(entry[1]*10.0**9,length))**2
            powLossResCos+=2*(bCur**2)*entry[2]*(cosProf(entry[1]*10.0**9,length))**2
        lossGaussRes.append(powLossResGauss)
        lossCosRes.append(powLossResCos)
        tag+=1
        lossGaussCum.append([gaussStore, tarFile.rstrip(fileNam).strip(directory)])
        totFreq.append(gaussStore)
        totTot.append(powLossGauss)
    powTotals.append(totTot)
    powFreq.append(totFreq)

powTotals=sp.array(powTotals)
powFreq=sp.array(powFreq)
bunLens=sp.array(bunLens)
pl.plot(bunLens*10.0**9, powTotals[:,0], 'k-', label="24 Screen Conductors (Equal Length)")
pl.plot(bunLens*10.0**9, powTotals[:,1], 'r-', label="24 Screen Conductors (Alternative Design)")
pl.plot(bunLens*10.0**9, powTotals[:,2], 'b-', label="24 Screen Conductors (Enclosed Conductors)")
pl.plot(bunLens*10.0**9, powTotals[:,3], 'm-', label="24 Screen Conductors (Alternating Length 20mm)")
pl.plot(bunLens*10.0**9, powTotals[:,4], 'g-', label="24 Screen Conductors (Tapered Length)")
pl.plot(bunLens*10.0**9, powTotals[:,5], 'k--', label="24 Screen Conductors (Tapered and Alternating)")
pl.plot(bunLens*10.0**9, powTotals[:,6], 'r--', label="24 Screen Conductors (Final Design)")
pl.xlabel("Bunch Length (ns)", fontsize="16.0")
pl.ylabel("Power Loss (W)", fontsize="16.0")
pl.legend(loc="upper right")
pl.grid(True)
##pl.show()
pl.clf()
