import csv, os, sys, math
import scipy as sp
import pylab as pl
from scipy import optimize as op

fitfunc = lambda p, x: p[2] + p[1]*x + p[0]*x**2
errfunc = lambda p, x, y, err: (y-fitfunc(p, x))/err

imp_err = 0.1
C = 3.0*10**8
Z0=377.0
lenTot = 0.01
rSep = 0.004
rWire = 0.0005
apDev = 0.005
##print cos_phi_bt
##print cos_phi_at
##print sp.arccos(cos_phi_bt)
##print sp.arccos(cos_phi_at)
length_total = 0.01
length_imp = 0.01
wire_sep = 0.003
cuDen = 1
cuWire = cuDen*sp.pi*rWire**2


def importDat(file_tar):
    tar = open(file_tar, 'r+')
    tempDat = tar.readlines()
    datStore = []
    for entry in tempDat[1:]:
        temp = map(float, entry.split(','))
        datStore.append(temp)

    return datStore


def analDipRe(data, freqList, lenDUT, lenTot, r_wire, r_sep, apPipe):
    reStore = []
    temp = []
    for i in range(0,len(data)):
        temp.append(C/(2*sp.pi*freqList[i]*r_sep**2)*2*data[i,1]/cuWire**2/lenTot) 
    return temp

def analSingRe(data, freqList, lenDUT, lenTot, r_wire, apPipe):
    reStore = []
    temp = []
    for i in range(0,len(data)):
        tempLin = []
        for j in range(1,len(data[0])):
            tempLin.append(2*data[i,j]/cuWire**2/lenDUT) 
        temp.append(tempLin)
    return temp


freq_list = []
for i in range(3,8,1):
    for j in range(1,11,1):
        freq_list.append(float((j)*10.0**i))

freq_listHeat = list(set(freq_list))
freq_listHeat.sort()

########   Theoretial Import #########

directory = "E:/PhD/1st_Year_09-10/Data/frequency-dom-test/data/project_dimensions_Jun_2012/"
filName = "graphiteJul2012Geo"

dipole_theory_input = directory+filName+"dip_vert.csv"
dipole_theory=[]
input_file = open(dipole_theory_input, 'r+')
linelist = input_file.readlines()
input_file.close()       
for row in linelist:
    dipole_theory.append(map(float, row.split(',')))

dipole_theory=sp.array(dipole_theory)

long_theory_input = directory+filName+"long.csv"
long_theory=[]
input_file = open(long_theory_input, 'r+')
linelist = input_file.readlines()
input_file.close()       
for row in linelist:
    long_theory.append(map(float, row.split(',')))

long_theory=sp.array(long_theory)

quad_theory_input = directory+filName+"quad_vert.csv"
quad_theory=[]
input_file = open(quad_theory_input, 'r+')
linelist = input_file.readlines()
input_file.close()       
for row in linelist:
    quad_theory.append(map(float, row.split(',')))

quad_theory=sp.array(dipole_theory)

######### Simulations #########


directory = "E:/PhD/1st_Year_09-10/Data/frequency-dom-test/data/project_dimensions_Jun_2012/simulation_data/results_graphite_plates_maxwell/" #Directory of data

horzSingReList = [directory+"y_scan_"+str(i)+".csv" for i in range(5,0,-1)]
horzDipReList = [directory+"y_dip_"+str(i)+".csv" for i in range(5,0,-1)]
horzSingReDat = []
horzDipReDat = []

for i in range(0,len(horzSingReList)):
    fileTar = horzSingReList[i]
    if i in range(1,3):
        temp = importDat(fileTar)
        horzSingReDat+=temp[1:-1]
    elif i==4:
        temp = importDat(fileTar)
        horzSingReDat+=temp[1:]
    else:
        horzSingReDat+=importDat(fileTar)


for i in range(0,len(horzDipReList)):
    fileTar = horzDipReList[i]
    if i in range(1,3):
        temp = importDat(fileTar)
        horzDipReDat+=temp[1:-1]
    elif i==4:
        temp = importDat(fileTar)
        horzDipReDat+=temp[1:]
    else:
        horzDipReDat+=importDat(fileTar)
        
horzSingReDat = sp.array(horzSingReDat)
horzSingReDat = sp.array(analSingRe(horzSingReDat, freq_listHeat, lenTot, lenTot, rWire, apDev))
horzDipReDat = sp.array(horzDipReDat)
horzDipReDatHeat = sp.array(analDipRe(horzDipReDat, freq_listHeat, lenTot, lenTot, rWire, rSep, apDev))

horzLong = []
horzTotTrans = []

savDir = directory+"vertGraph/"

try:
    os.mkdir(savDir)
except:
    pass

for i in range(0,len(horzSingReDat)):
    x_data = sp.linspace(-0.003,0.003,7)
    xPlot = sp.linspace(-0.004,0.004,1000)
    transLong = horzSingReDat[i,1:-1]
    pinit = [0.0, 1.0, 1.0]
    y_err = transLong
    out = op.leastsq(errfunc, pinit, args=(x_data,transLong,y_err), full_output=1)
    pfinal_real = out[0]
##    print pfinal_real
    yPlot = sp.polyval(pfinal_real, xPlot)
    covar_real=out[1]
    horzLong.append(pfinal_real[2])
    horzTotTrans.append(pfinal_real[0]*C/(2*sp.pi*freq_list[i]))
    pl.plot(x_data*10**3, transLong, 'kx', label="Data")
    pl.plot(xPlot*10**3, yPlot, 'r-', label="Fit")
    pl.legend(loc="upper center")
    pl.xlabel("Displacement (mm)", fontsize="16.0")
    pl.ylabel("$\Re{}e (Z_{\parallel}) (\Omega/m)$", fontsize="16.0")
    pl.savefig(savDir+'plot_'+str(i)+".pdf")
    pl.savefig(savDir+'plot_'+str(i)+".png")
    pl.savefig(savDir+'plot_'+str(i)+".eps")
    pl.clf()

horzLongHeat = sp.array(horzLong)
horzTotTransHeat = sp.array(horzTotTrans)
print horzTotTransHeat

distance_acc = 0.000001
imp_err = distance_acc/0.003

C = 3.0*10**8
Z0=377.0
lenTot = 0.15
rSep = 0.003
rWire = 0.0003
apDev = 0.005
##print cos_phi_bt
##print cos_phi_at
##print sp.arccos(cos_phi_bt)
##print sp.arccos(cos_phi_at)
length_total = 0.05
length_imp = 0.05
wire_sep = 0.003

def importDat(file_tar):
    tar = open(file_tar, 'r+')
    tempDat = tar.readlines()
    datStore = []
    for entry in tempDat[1:]:
        temp = map(float, entry.split(','))
        datStore.append(temp)

    return datStore


def analDipRe(data, freqList, lenDUT, lenTot, r_wire, r_sep, apPipe):
    reStore = []
    temp = []
    Z0 = 120*math.acosh(r_sep/(2*r_wire))
    for i in range(0,len(data)):
        temp.append(-2*Z0*C/(2*sp.pi*freqList[i]*r_sep**2)*sp.log(10**(data[i,1]/20))/lenTot) 
    return temp

def analDipIm(data, freqList, lenDUT, lenTot, r_wire, r_sep, apPipe):
    reStore = []
    temp = []
    Z0 = 120*math.acosh(r_sep/(2*r_wire))
    for i in range(0,len(data)):
        print sp.radians(data[i,1]), (2*sp.pi*freq_list[i]*lenTot/C)
        temp.append(-2*Z0*C/(2*sp.pi*freqList[i]*lenTot*r_sep**2)*(sp.radians(data[i,1])+(2*sp.pi*freq_list[i]*lenTot/C))) 
    pl.plot(freqList, sp.radians(data[:,1]))
##    pl.show()
    pl.clf()
    return temp

def analSingRe(data, freqList, lenDUT, lenTot, r_wire, apPipe):
    reStore = []
    temp = []
    Z0 = 60*sp.log(1.27*apPipe/r_wire)
    for i in range(0,len(data)):
        tempLin = []
        for j in range(1,len(data[0])):
            tempLin.append(2*Z0/lenTot*sp.log(10**(-data[i,j]/20))) 
        temp.append(tempLin)
    return temp

def analSingIm(data, freqList, lenDUT, lenTot, r_wire, apPipe):
    reStore = []
    temp = []
    Z0 = 60*sp.log(1.27*apPipe/r_wire)
    for i in range(0,len(data)):
        tempLin = []
        for j in range(1,len(data[0])):
            tempLin.append(-2*Z0*(sp.radians(data[i,j])+(2*sp.pi*freq_list[i]*lenTot/C))/lenTot) 
        temp.append(tempLin)
    return temp


freq_list = []
for i in range(6,9,1):
    for j in range(1,11,1):
        freq_list.append(float((j)*10.0**i))

freq_list = list(set(freq_list))
freq_list.sort()

########   Theoretial Import #########

directory = "E:/PhD/1st_Year_09-10/Data/frequency-dom-test/data/project_dimensions_Jun_2012/"
filName = "graphiteJul2012Geo"

dipole_theory_input = directory+filName+"dip_vert.csv"
dipole_theory=[]
input_file = open(dipole_theory_input, 'r+')
linelist = input_file.readlines()
input_file.close()       
for row in linelist:
    dipole_theory.append(map(float, row.split(',')))

dipole_theory=sp.array(dipole_theory)

long_theory_input = directory+filName+"long.csv"
long_theory=[]
input_file = open(long_theory_input, 'r+')
linelist = input_file.readlines()
input_file.close()       
for row in linelist:
    long_theory.append(map(float, row.split(',')))

long_theory=sp.array(long_theory)

quad_theory_input = directory+filName+"quad_vert.csv"
quad_theory=[]
input_file = open(quad_theory_input, 'r+')
linelist = input_file.readlines()
input_file.close()       
for row in linelist:
    quad_theory.append(map(float, row.split(',')))

quad_theory=sp.array(dipole_theory)

######### Simulations #########


directory = "E:/PhD/1st_Year_09-10/Data/frequency-dom-test/data/project_dimensions_Jun_2012/tmpAlt/results_graphite_plates/" #Directory of data

horzSingReList = [directory+"y_scan_db_"+str(i)+".csv" for i in range(3,0,-1)]
horzSingImList = [directory+"y_scan_arg_"+str(i)+".csv" for i in range(3,0,-1)]
horzDipReList = [directory+"y_dip_db_"+str(i)+".csv" for i in range(3,0,-1)]
horzDipImList = [directory+"y_dip_arg_"+str(i)+".csv" for i in range(3,0,-1)]
horzSingReDat = []
horzSingImDat = []
horzDipReDat = []
horzDipImDat = []

for i in range(0,len(horzSingReList)):
    fileTar = horzSingReList[i]
    if i!=2:
        temp = importDat(fileTar)
        horzSingReDat+=temp[:-1]
    else:
        horzSingReDat+=importDat(fileTar)

for i in range(0,len(horzSingImList)):
    fileTar = horzSingImList[i]
    if i!=2:
        temp = importDat(fileTar)
        horzSingImDat+=temp[:-1]
    else:
        horzSingImDat+=importDat(fileTar)

for i in range(0,len(horzDipReList)):
    fileTar = horzDipReList[i]
    if i!=2:
        temp = importDat(fileTar)
        horzDipReDat+=temp[:-1]
    else:
        horzDipReDat+=importDat(fileTar)

for i in range(0,len(horzDipImList)):
    fileTar = horzDipImList[i]
    if i!=2:
        temp = importDat(fileTar)
        horzDipImDat+=temp[:-1]
    else:
        horzDipImDat+=importDat(fileTar)

horzSingReDat = sp.array(horzSingReDat)
horzSingReDat = sp.array(analSingRe(horzSingReDat, freq_list, lenTot, lenTot, rWire, apDev))
horzSingImDat = sp.array(horzSingImDat)
horzSingImDat = sp.array(analSingIm(horzSingImDat, freq_list, lenTot, lenTot, rWire, apDev))
horzDipReDat = sp.array(horzDipReDat)
horzDipReDat = sp.array(analDipRe(horzDipReDat, freq_list, lenTot, lenTot, rWire, rSep, apDev))
##for i in range(0,len(horzDipImDat)-10):
##    horzDipImDat[i] = [horzDipImDat[i][0], float(horzDipImDat[i][1])-180]
##for i in range(len(horzDipImDat)-1,len(horzDipImDat)):
##    horzDipImDat[i] = [horzDipImDat[i][0], float(horzDipImDat[i][1])-360]
horzDipImDat = sp.array(horzDipImDat)
horzDipImDat = sp.array(analDipIm(horzDipImDat, freq_list, lenTot, lenTot, rWire, rSep, apDev))

horzLong = []
horzTotTrans = []

savDir = directory+"vertGraph/"

try:
    os.mkdir(savDir)
except:
    pass

for i in range(0,len(horzSingReDat)):
    x_data = sp.linspace(-0.002,0.002,3)
    xPlot = sp.linspace(-0.004,0.004,1000)
    transLongRe = horzSingReDat[i,1:-1]
    transLongIm = horzSingImDat[i,1:-1]
##    print len(x_data), len(transLongRe), len(transLongIm)
    pinit = [0.0, 1.0, 1.0]
    y_err = transLongRe
    out = op.leastsq(errfunc, pinit, args=(x_data,transLongRe,y_err), full_output=1)
    pfinal_real = out[0]
##    print pfinal_real
    yPlotRe = sp.polyval(pfinal_real, xPlot)
    covar_real=out[1]
    y_err = transLongIm
    out = op.leastsq(errfunc, pinit, args=(x_data,transLongIm,y_err), full_output=1)
    pfinal_imag = out[0]
##    print pfinal_real
    yPlotIm = sp.polyval(pfinal_imag, xPlot)    
    horzLong.append([pfinal_real[2], pfinal_imag[2]])
    horzTotTrans.append([pfinal_real[0]*C/(2*sp.pi*freq_list[i]),pfinal_imag[0]*C/(2*sp.pi*freq_list[i])])
    pl.plot(x_data*10**3, transLongRe, 'kx', label="Data")
    pl.plot(xPlot*10**3, yPlotRe, 'r-', label="Fit")
    pl.legend(loc="upper center")
    pl.xlabel("Displacement (mm)", fontsize="16.0")
    pl.ylabel("$\Re{}e (Z_{\parallel}) (\Omega/m)$", fontsize="16.0")
    pl.savefig(savDir+'plot_'+str(i)+"_re.pdf")
    pl.savefig(savDir+'plot_'+str(i)+"_re.png")
    pl.savefig(savDir+'plot_'+str(i)+"_re.eps")
    pl.clf()
    pl.plot(x_data*10**3, transLongIm, 'kx', label="Data")
    pl.plot(xPlot*10**3, yPlotIm, 'r-', label="Fit")
    pl.legend(loc="upper center")
    pl.xlabel("Displacement (mm)", fontsize="16.0")
    pl.ylabel("$\Re{}e (Z_{\parallel}) (\Omega/m)$", fontsize="16.0")
    pl.savefig(savDir+'plot_'+str(i)+"_im.pdf")
    pl.savefig(savDir+'plot_'+str(i)+"_im.png")
    pl.savefig(savDir+'plot_'+str(i)+"_im.eps")
    pl.clf()

horzLong = sp.array(horzLong)
horzTotTrans = sp.array(horzTotTrans)

##print freq_list, horzDipReDat
pl.loglog()
##pl.semilogy()
pl.plot(freq_list, horzDipReDat, 'kx', markersize=16, label="Simulation $\Re{}e(Z_{\perp, y}^{dipolar})$")
pl.plot(freq_list, horzDipImDat, 'rx', markersize=16, label="Simulation $\Im{}m(Z_{\perp, y}^{dipolar})$")
pl.plot(freq_listHeat, horzDipReDatHeat, 'k+', markersize=16, label="Simulation Power Loss $\Re{}e(Z_{\perp, y}^{dipolar})$")
pl.plot(dipole_theory[:,0], dipole_theory[:,1], 'b-', label="Theory $\Re{}e(Z_{\perp, y}^{dipolar})$")
pl.plot(dipole_theory[:,0], dipole_theory[:,2], 'r-', label="Theory $\Im{}m(Z_{\perp, y}^{dipolar})$")
pl.axis([10**2,10**9,10**3, 10**7])
pl.xlabel("Frequency (Hz)", fontsize="16.0")
pl.ylabel("$(Z_{\perp, Dipolar}) (\Omega/m^{2})$", fontsize="16.0")
pl.legend(loc="lower center")
##pl.show()
pl.savefig(savDir+"vertDip.pdf")
pl.savefig(savDir+"vertDip.eps")
pl.savefig(savDir+"vertDip.png")
pl.clf()

##print len(freq_list[-6:]), len(horzTotTrans[-6:,1]), len(horzDipImDat[-6:])
pl.loglog()
##pl.semilogy()
pl.plot(freq_list, (horzTotTrans[:,0]-horzDipReDat), 'kx', markersize=16, label="Simulation $\Re{}e(Z_{\perp, y}^{quadrupolar})$")
pl.plot(freq_list, (horzTotTrans[:,1]-horzDipImDat), 'rx', markersize=16, label="Simulation $\Im{}m(Z_{\perp, y}^{quadrupolar})$")
pl.plot(freq_listHeat, (horzTotTransHeat[:]-horzDipReDatHeat), 'k+', markersize=16, label="Simulation Power Loss $\Re{}e(Z_{\perp, y}^{quadrupolar})$")
pl.plot(quad_theory[:,0], quad_theory[:,1], 'b-', label="Theory $\Re{}e(Z_{\perp, y}^{quadrupolar})$")
pl.plot(quad_theory[:,0], quad_theory[:,2], 'r-', label="Theory $\Im{}m(Z_{\perp, y}^{quadrupolar})$")
pl.axis([10**2,10**9,10**3, 10**7])
pl.xlabel("Frequency (Hz)", fontsize="16.0")
pl.ylabel("$(Z_{\perp, Quadrupolar}) (\Omega/m^{2})$", fontsize="16.0")
pl.legend(loc="lower center")
pl.show()
pl.savefig(savDir+"vertQuad.pdf")
pl.savefig(savDir+"vertQuad.eps")
pl.savefig(savDir+"vertQuad.png")
pl.clf()

pl.loglog()
##pl.semilogy()
pl.plot(freq_list, (horzTotTrans[:,0]), 'kx', markersize=16, label="Simulation $\Re{}e(Z_{\perp, y}^{quadrupolar})$")
pl.plot(freq_list, (horzTotTrans[:,1]), 'rx', markersize=16, label="Simulation $\Im{}m(Z_{\perp, y}^{quadrupolar})$")
pl.plot(freq_listHeat, (horzTotTransHeat[:]), 'k+', markersize=16, label="Simulation Power Loss $\Re{}e(Z_{\perp, y}^{quadrupolar})$")
##pl.plot(quad_theory[:,0], quad_theory[:,1], 'b-', label="Theory $\Re{}e(Z_{\perp, y}^{quadrupolar})$")
##pl.plot(quad_theory[:,0], quad_theory[:,2], 'r-', label="Theory $\Im{}m(Z_{\perp, y}^{quadrupolar})$")
##pl.axis([10**2,10**9,10**3, 10**7])
pl.xlabel("Frequency (Hz)", fontsize="16.0")
pl.ylabel("$(Z_{\perp, Quadrupolar}) (\Omega/m^{2})$", fontsize="16.0")
pl.legend(loc="lower center")
##pl.show()
pl.savefig(savDir+"vertTot.pdf")
pl.savefig(savDir+"vertTot.eps")
pl.savefig(savDir+"vertTot.png")
pl.clf()

pl.loglog()
pl.plot(freq_list, horzLong[:,0], 'kx', markersize=16, label="Simulation $\Re{}e(Z_{\parallel, y})$")
pl.plot(freq_list, horzLong[:,1], 'rx', markersize=16, label="Simulation $\Im{}m(Z_{\parallel, y})$")
pl.plot(freq_listHeat, horzLongHeat, 'k+', markersize=16, label="Simulation Power Loss $\Re{}e(Z_{\parallel, y})$")
pl.plot(long_theory[:,0],long_theory[:,1], 'b-', markersize=16, label="Theory $\Re{}e(Z_{\parallel, y})$")
pl.plot(long_theory[:,0],long_theory[:,2], 'r-', markersize=16, label="Theory $\Im{}m(Z_{\parallel, y})$")
pl.axis([10**2,10**9,10**-6,10**2])
pl.xlabel("Frequency (Hz)", fontsize="16.0")
pl.ylabel("$(Z_{\parallel}) (\Omega/m)$", fontsize="16.0")
pl.legend(loc="lower center")
##pl.show()
pl.savefig(savDir+"vertLong.pdf")
pl.savefig(savDir+"vertLong.eps")
pl.savefig(savDir+"vertLong.png")
pl.clf()
