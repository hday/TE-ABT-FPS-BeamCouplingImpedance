import csv
import scipy as sp
import pylab as pl

C = 3.0*10**8
directory = "E:/PhD/1st_Year_09-10/Data/mki-heating/cst_model/" #Directory of data
plain_24 = "24-conductors/wakepotential-long.csv"

data_24_plain =[]

input_file = open(directory+plain_24, 'r+')
tar = csv.reader(input_file, delimiter=",")

i=0
for row in tar:
    if i!=0:
        row = map(float, row)
##        n = float(row[0])*10**6/f_rev
##        row[1] = float(row[1])/0.003
##        row[3] = float(row[3])/0.003
        data_24_plain.append(row)
    i+=1

input_file.close()
data_24_plain = sp.array(data_24_plain)

##pl.plot(data_24_plain[:,0], data_24_plain[:,1])
##pl.show()
##pl.clf()

time_spacing = data_24_plain[:,0]/C


print len(time_spacing)
result = sp.fft(data_24_plain[:,1])
between = time_spacing[1]-time_spacing[0]
print 1/between 
f = 1/between*sp.r_[0:(len(time_spacing))]/len(time_spacing)
n = len(f)
print f

pl.plot(data_24_plain[:,0],data_24_plain[:,1])
pl.show()

##pl.plot(f, abs(sp.real(result[0:n])))
##pl.plot(f, abs(sp.imag(result[0:n])))
####pl.axis([0,10000,-0.5,0.5])
##pl.show()
##pl.clf()
