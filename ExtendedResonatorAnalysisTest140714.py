import csv, time, os, sys
import scipy as sp
import pylab as pl
import numpy as np
import scipy.optimize as op
import scipy.interpolate as interp
from scipy import signal

####### Define constants for calculations ##########

C = 299792458.0
c = 299792458.0
Z0 = 377
lenMKP = 3.423+(2*0.035)+(2*0.005)
Zc = 320.0
mu0=4*sp.pi*10.0**-7
eps0= 8.854187817*10.0**-12
dAper = 0.045
rWire = 0.0005
lWire = 3.551
lDUT = 2.45
resCopper = 17.24*10.0**-9
resOuter = 1/(3.3*10.0**7)
ZChar = 60*sp.log(dAper/rWire)

def extract_dat(file_name):
    data = open(file_name, "r+")
    tar = csv.reader(data, delimiter=",")
    temp=[]
    for row in tar:
        row = map(float, row)
        temp.append(row)
    data.close()

    return temp

def eigenmodeGen(freqFile, qFile, rOverQFile):
    inputFreq=open(freqFile, "r+")
    datFreq=inputFreq.readlines()
    inputFreq.close()
    outputFreq=[]
    for line in datFreq[-20:-1]:
        temp=(line[25:].rstrip()).strip()
        outputFreq.append(float(temp))
    inputQ=open(qFile, "r+")
    datQ=inputQ.readlines()
    inputQ.close()
    outputQ=[]
    for line in datQ[-20:-1]:
        temp=(line[25:].rstrip()).strip()
        outputQ.append(float(temp))
    inputrOverQ=open(rOverQFile, "r+")
    datrOverQ=inputrOverQ.readlines()
    inputrOverQ.close()
    outputrOverQ=[]
    for line in datrOverQ[-20:-1]:
        temp=(line[25:].rstrip()).strip()
        outputrOverQ.append(float(temp))
    resDat=[]
    for i in range(0,len(outputFreq)):
        resDat.append([outputFreq[i], outputQ[i], outputrOverQ[i]])

    return resDat

def Z_bb(freq, data):
    return data[2]*data[1]/complex(1, data[1]*(freq/data[0] - data[0]/freq))

def gaussProf(freq, bLength):
    sigma = bLength/(4.0*2.0*sp.log(2.0))
    return sp.e**(-(2.0*sp.pi*freq*sigma)**2*(2**0.5))

def cosProf(freq, bunLength):
    return (sp.sin(sp.pi*freq*bunLength)/(sp.pi*freq*bunLength*(1-(freq*bunLength)**2)))**2


def readS21FromVNA(inputFile):
    tar=open(inputFile, "r+")
    inputData=tar.readlines()
    tar.close()
    outputData=[]
    for line in inputData[6:]:
        temp=line.split("\t")
        outputData.append(map(float,temp))
    return outputData

def logToLin(data):
    return 10**(data/20)

def linToLog(data):
    return 20*sp.log10(data)

def lumpImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return 2*Zc*(1-dataNorm)/dataNorm

def logImpFormula(dataDUT, dataREF, Zc):
    dataNorm = dataDUT/dataREF
    return -2*Zc*sp.log(dataNorm)

def skinDepth(freq, cond):
    return (1/(sp.pi*freq*mu0*cond))**0.5

def fileImport(tarFile):
    readPart=open(tarFile, 'r+')
    datUnPro = readPart.readlines()
    readPart.close()
    freqDat=[]
    qLoaded=[]
    s21DB=[]
    for row in datUnPro[20:]:
        freqDat.append(float(row.split(",")[0]))
        qLoaded.append(float(row.split(",")[1]))
        s21DB.append(float(row.split(",")[2]))

    return sp.array(freqDat), sp.array(qLoaded), sp.array(s21DB)

def resImpGet(tarArr1,tarArr2,tarArr3,attenCableLen):

    freqList, qTotal, transCoeff = tarArr1, tarArr2, tarArr3

    linTransCoeff = 10.0**(transCoeff/10.0)
    coupCoeff = linTransCoeff/(1-linTransCoeff)
    qUnloaded=qTotal*(1+coupCoeff)

    measAttenuation = 8.686*sp.pi*freqList/(C*qUnloaded)
    skinD = skinDepth(freqList, 1/resCopper)
    skinCor = (rWire+skinD)/rWire

    attenCable = attenCableLen*freqList*10.0**-9
    attenCopper = skinCor*8.686*(sp.pi*eps0*resCopper*freqList)**0.5/(sp.log(dAper/rWire))*((1/rWire)+(1/dAper)*(resCopper/resOuter)**0.5)
    resLow = 4*lWire*resCopper/(sp.pi*rWire**2)
    resHigh = resLow*rWire/(4*skinD)*skinCor

    skinPipe = skinDepth(freqList, resOuter)
    zMeas = resHigh*(measAttenuation-attenCopper-attenCable)/(attenCopper)

    return freqList, zMeas/lDUT

def resImpGetFromFile(targetFile):

    freqList, qTotal, transCoeff = fileImport(targetFile)

    linTransCoeff = 10.0**(transCoeff/10.0)
    coupCoeff = linTransCoeff/(1-linTransCoeff)
    qUnloaded=qTotal*(1+coupCoeff)

    measAttenuation = 8.686*sp.pi*freqList/(c*qUnloaded)
    skinD = skinDepth(freqList, 1/resCopper)
    skinCor = (rWire+skinD)/rWire

    attenCopper = skinCor*8.686*(sp.pi*eps0*resCopper*freqList)**0.5/(sp.log(dAper/rWire))*((1/rWire)+(1/dAper)*(resCopper/resOuter)**0.5)
    resLow = 4*lWire*resCopper/(sp.pi*rWire**2)
    resHigh = resLow*rWire/(4*skinD)*skinCor


    skinPipe = skinDepth(freqList, resOuter)
    zMeas = resHigh*(measAttenuation-attenCopper)/attenCopper

    return freqList, zMeas/lDUT

def resAnalVNARawData(dataArray):
    widths=sp.arange(3,10)
    peakList=signal.find_peaks_cwt(dataArray[:,3],widths)

    fitFunc = lambda p, x: (p[0]/(1+complex(0,1)*p[1]*(x/p[2] - p[2]/x))).real
    errFunc = lambda p, x, y, err: (y-fitFunc(p, x))

    fitNo = 5
    testList=sp.linspace(1,10,10)

    peakDataStash=[]
    for peak in peakList:
        pInit = [logToLin(dataArray[peak,3]),100,dataArray[peak,0]/10**9]
        freqVal = dataArray[peak-fitNo:peak+fitNo,0]/10**9
        yErr = logToLin(dataArray[peak-fitNo:peak+fitNo,3])
        temp = yErr
        out=op.leastsq(errFunc, pInit, args=(freqVal,temp,yErr), full_output=1)
        pFinal = out[0]
    ##    print pFinal
        peakDataStash.append([logToLin(dataArray[peak,3]), pFinal[1], transmissionDataLong[peak,0]/10**9])

    peakDataStash=sp.array(peakDataStash)
    check=0
    while check<len(peakDataStash):
        if peakDataStash[check,1]<100:
            peakDataStash=sp.vstack([peakDataStash[:check],peakDataStash[check+1:]])
        else:
            check+=1

    return peakDataStash


targetDirProbeMeas = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/11-07-14/mkiTank01-CR03/resLong/"
temp=[]
listOfFiles = [str(i)+"-"+str(i+200)+"MHZ.S2P" for i in range(0,2000,200)]
for inputFile in listOfFiles:
    tar=open(targetDirProbeMeas+inputFile)
    inputData=tar.readlines()
    tar.close()
    for line in inputData[6:]:
        bit=line.split("\t")
        temp.append(map(float,bit))

transmissionDataLong=sp.array(temp)
impConvLong = resAnalVNARawData(transmissionDataLong)


#### For S2P Short Cables ######

targetDirProbeMeas = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/11-07-14/mkiTank01-CR03/resShort/"
temp=[]
listOfFiles = [str(i)+"-"+str(i+200)+"MHZ.S2P" for i in range(0,2000,200)]
for inputFile in listOfFiles:
    tar=open(targetDirProbeMeas+inputFile)
    inputData=tar.readlines()
    tar.close()
    for line in inputData[6:]:
        bit=line.split("\t")
        temp.append(map(float,bit))
              
transmissionDataShort=sp.array(temp)
impConvShort = resAnalVNARawData(transmissionDataShort)


##### Taking Measurements from VNA ########

measurementsMKI110314Freq, measurementsMKI110314Imp = sp.array(resImpGetFromFile("E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/11-07-14/mkiTank01-CR03/resScript"))
dirMatch140714 = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/14-07-14/mkiTank01-CR03/S21FREQMATCHINGNOGATING.S2P"
tar=open(dirMatch140714,'r+')
inputData=tar.readlines()
tar.close()
temp=[]
for line in inputData[6:]:
    bit=line.split("\t")
    temp.append(map(float,bit))

matched140714 = sp.array(temp)
matched140714Imp = logImpFormula(logToLin(matched140714[:,3]), 1, 160)

dirMatch140714 = "E:/PhD/1st_Year_09-10/Data/LHC-MKI-data/NewDesign/14-07-14/mkiTank01-CR03/S21FREQMATCHINGLOWFREQ.S2P"
tar=open(dirMatch140714,'r+')
inputData=tar.readlines()
tar.close()
temp=[]
for line in inputData[6:]:
    bit=line.split("\t")
    temp.append(map(float,bit))

matched140714LowFreq = sp.array(temp)
matched140714LowFreqImp = logImpFormula(logToLin(matched140714LowFreq[:,3]), 1, 160)

##### Plotting Measurements ########

##pl.semilogy()
##pl.plot(transmissionDataLong[:,0]/10**9, (transmissionDataLong[:,3]), 'k-', label="MKI with 6m cables")
pl.plot(transmissionDataShort[:,0]/10**9, (transmissionDataShort[:,3]), 'r-', label="MKI only")
##for i in impConvLong:
##    pl.plot(impConvLong[i,0]/10**9, (impConvLong[i,3]), 'kx')
##for i in impConvShort:
##    pl.plot(impConvShort[i,0]/10**9, impConvShort[i,3], 'rx')

##for i in range(0,len(impConvLong)):
##    plotWidth=sp.linspace((transmissionDataLong[i,2]-0.05),(transmissionDataLong[i,2]+0.05),500)
##    plotValues = fitFunc(impConvLong[i,:],plotWidth)
##    
##    pl.plot(plotWidth, linToLog(plotValues), 'b-')
##for i in range(0,len(impConvShort)):
##    plotWidth=sp.linspace((transmissionDataShort[i,2]-0.05),(transmissionDataShort[i,2]+0.05),500)
##    plotValues = fitFunc(impConvShort[i,:],plotWidth)
    
##    pl.plot(plotWidth, linToLog(plotValues), 'b-')
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("S$_{21}$ (dB)", fontsize=16.0)
##pl.legend(loc="upper left")
##pl.ylim(-100,-20)
##pl.xlim(0,0.5)
pl.show ()
pl.clf()

##pl.plot(transmissionDataLong[:,0]/10**9, transmissionDataLong[:,1])
pl.plot(transmissionDataShort[:,0]/10**9, transmissionDataShort[:,1])
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("S$_{11}$ (dB)")
##pl.show()
pl.clf()

pl.plot(impConvShort[:,2], impConvShort[:,1], 'rx')
pl.plot(impConvLong[:,2], impConvLong[:,1], 'kx')
##pl.show()
pl.clf()

impResultsFreqShort, impResultsImpShort=resImpGet(impConvShort[:,2]*10**9,impConvShort[:,1], linToLog(impConvShort[:,0]), 0.0)
impResultsFreqLong, impResultsImpLong=resImpGet(impConvLong[:,2]*10**9,impConvLong[:,1], linToLog(impConvLong[:,0]), 0.000)
pl.plot(impResultsFreqShort/10**9, impResultsImpShort/2.97, 'k-', label="Manual Measurement of Q-factors from VNA, MKI only")
pl.plot(impResultsFreqLong/10**9, impResultsImpLong/2.97, 'r-', label="Manual Measurement of Q-factors from VNA, MKI+6m cable")
pl.plot(measurementsMKI110314Freq/10**9, measurementsMKI110314Imp, 'bx', markersize=16.0, label="Automated Measurement of Q-factors from VNA, MKI only")
pl.plot(measurementsMKI110314Freq/10**9, measurementsMKI110314Imp, 'b-', label="Automated Measurement of Q-factors from VNA, MKI only")
pl.plot(matched140714[:,0]/10**9, matched140714Imp-400, "m-")
pl.plot(matched140714LowFreq[:,0]/10**9, matched140714LowFreqImp-400, "g-")
##pl.axis([0,0.5,0,2])
pl.xlabel("Frequency (GHz)", fontsize=16.0)
pl.ylabel("$\Re{}e(Z_{\parallel})$ ($\Omega/m$)", fontsize=16.0)
##pl.legend(loc="upper left")
##pl.show()
pl.clf()

