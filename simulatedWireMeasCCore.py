import csv, os, sys, math
import scipy as sp
import pylab as pl
from scipy import optimize as op
import scipy.signal as sig
import impedance.impedance as imp


fitfunc = lambda p, x: p[2] + p[1]*x + p[0]*x**2
errfunc = lambda p, x, y, err: (y-fitfunc(p, x))/err

linfitfunc = lambda p, x: p[0] + p[1]*x
linerrfunc = lambda p, x, y, err: (y-linfitfunc(p, x))/err

distance_acc = 0.000001
imp_err = distance_acc/0.003

C = 3.0*10**8
Z0=377.0
lenTot = 0.1
rSep = 0.002
rWire = 0.0002
apDev = 0.005
##print cos_phi_bt
##print cos_phi_at
##print sp.arccos(cos_phi_bt)
##print sp.arccos(cos_phi_at)
length_total = 0.05
length_imp = 0.05
wire_sep = 0.003


freqList = []
for i in range(5,10):
    for j in range(1,11):
        freqList.append(j*10.0**i)
freqList=list(set(freqList))
freqList.sort()
freqList=sp.array(freqList)

########   Theoretial Import #########

directory = "E:/PhD/1st_Year_09-10/Data/frequency-dom-test/data/project_dimensions_Jun_2012/tmpAlt/analytical/"
fileName = ""

longTheory = sp.array(imp.readAnalDat(directory+"longitudinal.txt"))
dipoleTheory = sp.array(imp.readAnalDat(directory+"dipolar.txt"))
quadTheory = sp.array(imp.readAnalDat(directory+"quadrupolar.txt"))
constTheory = sp.array(imp.readAnalDat(directory+"constant.txt"))

wireRadiusLin = sp.linspace(0.00005,0.0010,20)
wireRadiusLog = sp.logspace(-7,-2,20000)
aperOuter = 0.02
zCharRadiusLin = imp.charImpSquare(wireRadiusLin, aperOuter)
zCharRadiusLog = imp.charImpSquare(wireRadiusLog, aperOuter)
##pl.loglog()
pl.plot(wireRadiusLin, zCharRadiusLin)
pl.plot(wireRadiusLog, zCharRadiusLog)
##pl.show()
pl.clf()

######### Simulations #########

dirWireRad = "E:/PhD/1st_Year_09-10/Data/asymmetricMeasTest/"
dbFile = dirWireRad+"dbWireRad.csv"
argFile = dirWireRad+"argWireRad.csv"
SecondOrdTest = dirWireRad+"2ndOrderTest05mm.csv"
dbMeasWire = sp.array(imp.hfssImport(dbFile, 502))
argMeasWire = sp.array(imp.hfssImport(argFile, 502))
SecOrTest = sp.array(imp.hfssImport(SecondOrdTest, 502))


tempImag = []
for i in range(1,19):
    testImag = imp.hfssImagImp(argMeasWire[:,0],argMeasWire[:,i], 0.1, zCharRadiusLin[i])
##    print testImag
    tempImag.append(testImag)
##print testImag

##pl.semilogx()
pl.loglog()
for i in range(3,6):
    pl.plot(dbMeasWire[:,0], imp.logImpFormula(imp.logToLin(dbMeasWire[:,i]),1,zCharRadiusLin[i])/0.1, label="Wire Thickness = "+str(wireRadiusLin[i])+"mm")
    pl.plot(dbMeasWire[:,0], (tempImag[i]-tempImag[i][0])/0.1, label="Wire Thickness = "+str(wireRadiusLin[i])+"mm")
pl.plot(SecOrTest[:,0], imp.logImpFormula(imp.logToLin(SecOrTest[:,1]),1,imp.charImpSquare(0.0005,aperOuter))/0.1,"mx")
pl.plot(longTheory[:,0]/10**9, longTheory[:,1]*2, "k-.",label="Theory")
pl.plot(longTheory[:,0]/10**9, longTheory[:,2]*2, "r-.",label="Theory")
pl.legend(loc="upper left")
pl.ylim(-1000,2200)
pl.show()
pl.clf()

for i in range(0,8):
    pl.plot(argMeasWire[:,0],tempImag[i]-tempImag[i][0])
    pl.ylim(-500,1000)
##pl.show()
pl.clf()

##directory = "E:/PhD/1st_Year_09-10/Data/frequency-dom-test/data/project_dimensions_Jun_2012/tmpAlt/results_c_core_low_mesh/" #Directory of data
##directory = "E:/PhD/1st_Year_09-10/Data/frequency-dom-test/data/project_dimensions_Jun_2012/results_ferr_c_core_long/" #Directory of data
##
##horzSingReList = [directory+"db_"+str(i)+".csv" for i in range(4,0,-1)]
##horzSingImList = [directory+"arg_"+str(i)+".csv" for i in range(4,0,-1)]
##horzDipReYList = [directory+"y_dip_db_"+str(i)+".csv" for i in range(4,0,-1)]
##horzDipImYList = [directory+"y_dip_arg_"+str(i)+".csv" for i in range(4,0,-1)]
##horzDipReXList = [directory+"x_dip_db_"+str(i)+".csv" for i in range(4,0,-1)]
##horzDipImXList = [directory+"x_dip_arg_"+str(i)+".csv" for i in range(4,0,-1)]
##horzSingReDat = []
##horzSingImDat = []
##horzSingReYDat = []
##horzSingImYDat = []
##horzSingReXDat = []
##horzSingImXDat = []
##horzDipReYDat = []
##horzDipImYDat = []
##horzDipReXDat = []
##horzDipImXDat = []
##
##for i in range(0,len(horzSingReList)):
##    fileTar = horzSingReList[i]
##    if i in range(0,len(horzSingReList)-1):
##        temp = importDat(fileTar)
##        horzSingReDat+=temp[:-1]
##    else:
##        horzSingReDat+=importDat(fileTar)
##
##for i in range(0,len(horzSingImList)):
##    fileTar = horzSingImList[i]
##    if i in range(0,len(horzSingImList)-1):
##        temp = importDat(fileTar)
##        horzSingImDat+=temp[:-1]
##    else:
##        horzSingImDat+=importDat(fileTar)
##
##for i in range(0,len(horzDipReYList)):
##    fileTar = horzDipReYList[i]
##    if i in range(0,len(horzDipReYList)-1):
##        temp = importDat(fileTar)
##        horzDipReYDat+=temp[:-1]
##    else:
##        horzDipReYDat+=importDat(fileTar)
##
##for i in range(0,len(horzDipImYList)):
##    fileTar = horzDipImYList[i]
##    if i in range(0,len(horzDipImYList)-1):
##        temp = importDat(fileTar)
##        horzDipImYDat+=temp[:-1]
##    else:
##        horzDipImYDat+=importDat(fileTar)
##
##for i in range(0,len(horzDipReXList)):
##    fileTar = horzDipReXList[i]
##    if i in range(0,len(horzDipReXList)-1):
##        temp = importDat(fileTar)
##        horzDipReXDat+=temp[:-1]
##    else:
##        horzDipReXDat+=importDat(fileTar)
##
##for i in range(0,len(horzDipImXList)):
##    fileTar = horzDipImXList[i]
##    if i in range(0,len(horzDipImXList)-1):
##        temp = importDat(fileTar)
##        horzDipImXDat+=temp[:-1]
##    else:
##        horzDipImXDat+=importDat(fileTar)
##
####for i in range(0,len(horzSingReDat)):
####    print len(horzSingReDat[i])
##
####for i in range(0,len(horzSingImDat)):
####    print len(horzSingImDat[i])
##
##
##horzSingReDat = sp.array(horzSingReDat)
##horzSingReDat = sp.array(analSingRe(horzSingReDat, freq_list, lenTot, lenTot, rWire, apDev))
##horzSingImDat = sp.array(horzSingImDat)
####for i in range(0,len(horzSingImDat)-10):
####    horzSingImDat[i] = [horzSingImDat[i][0], float(horzSingImDat[i][:])-180]
####for i in range(len(horzSingImDat)-1,len(horzSingImDat)):
####    horzSingImDat[i] = [horzSingImDat[i][0], float(horzSingImDat[i][:])-360]
##horzSingImDat = sp.array(analSingIm(horzSingImDat, freq_list, lenTot, lenTot, rWire, apDev))
##horzDipReYDat = sp.array(horzDipReYDat)
##horzDipReYDat = sp.array(analDipRe(horzDipReYDat, freq_list, lenTot, lenTot, rWire, rSep, apDev))
####for i in range(0,10):
####    horzDipImYDat[i] = [horzDipImYDat[i][0], float(horzDipImYDat[i][1])-180]
##horzDipImYDat = sp.array(horzDipImYDat)
##horzDipImYDat = sp.array(analDipIm(horzDipImYDat, freq_list, lenTot, lenTot, rWire, rSep, apDev))
##horzDipReXDat = sp.array(horzDipReXDat)
##horzDipReXDat = sp.array(analDipRe(horzDipReXDat, freq_list, lenTot, lenTot, rWire, rSep, apDev))
####for i in range(0,10):
####    horzDipImXDat[i] = [horzDipImXDat[i][0], float(horzDipImXDat[i][1])-180]
####for i in range(len(horzDipImXDat)-10,len(horzDipImXDat)):
####    horzDipImXDat[i] = [horzDipImXDat[i][0], float(horzDipImXDat[i][1])-180]
##horzDipImXDat = sp.array(horzDipImXDat)
##horzDipImXDat = sp.array(analDipIm(horzDipImXDat, freq_list, lenTot, lenTot, rWire, rSep, apDev))
##
##
##horzSingReXDat = horzSingReDat[:,:11]
##horzSingImXDat = horzSingImDat[:,:11]
##horzSingReYDat = sp.hstack((sp.column_stack((horzSingReDat[:,11:15],horzSingReDat[:,5])),horzSingReDat[:,15:]))
##horzSingImYDat = sp.hstack((sp.column_stack((horzSingImDat[:,11:15],horzSingImDat[:,5])),horzSingImDat[:,15:]))
##
##
##horzLong = []
##horzTotTrans = []
##horzConstX = []
##vertLong = []
##vertTotTrans = []
##vertConstX = []
##
##horzQuadStarAsym = []
##vertQuadStarAsym = []
##horzConstAsym = []
##vertConstAsym = []
##horzQuadStarReAsym = []
##horzQuadStarImAsym = []
##
##savDir = directory+"horzGraph/"
##savDirVert = directory+"vertGraph/"
##
##try:
##    os.mkdir(savDir)
##    os.mkdir(saveDirVert)
##except:
##    pass
##
##for i in range(0,len(horzSingReDat)):
##    x_data = sp.linspace(-0.005,0.005,11)
##    xPlot = sp.linspace(-0.005,0.005,1000)
##    transLongRe = horzSingReXDat[i,:]
##    transLongIm = horzSingImXDat[i,:]
##    pinit = [0.0, 1.0, 1.0]
##    y_err = transLongRe
##    out = op.leastsq(errfunc, pinit, args=(x_data,transLongRe,y_err), full_output=1)
##    pfinal_real = out[0]
####    print pfinal_real
##    yPlotRe = sp.polyval(pfinal_real, xPlot)
##    covar_real=out[1]
##    y_err = transLongIm
##    out = op.leastsq(errfunc, pinit, args=(x_data,transLongIm,y_err), full_output=1)
##    pfinal_imag = out[0]
####    print pfinal_real
##    yPlotIm = sp.polyval(pfinal_imag, xPlot)    
##    horzLong.append([pfinal_real[2], pfinal_imag[2]])
##    horzConstX.append([pfinal_real[1]*C/(2*sp.pi*freq_list[i]), pfinal_imag[1]*C/(2*sp.pi*freq_list[i])])
##    horzTotTrans.append([pfinal_real[0]*C/(2*sp.pi*freq_list[i]),pfinal_imag[0]*C/(2*sp.pi*freq_list[i])])
##    pl.plot(x_data*10**3, transLongRe, 'kx', label="Data")
##    pl.plot(xPlot*10**3, yPlotRe, 'r-', label="Fit")
##    pl.legend(loc="upper center")
##    pl.xlabel("Displacement (mm)", fontsize="16.0")
##    pl.ylabel("$\Re{}e (Z_{\parallel}) (\Omega/m)$", fontsize="16.0")
##    pl.savefig(savDir+'plot_'+str(i)+"_re.pdf")
##    pl.savefig(savDir+'plot_'+str(i)+"_re.png")
##    pl.savefig(savDir+'plot_'+str(i)+"_re.eps")
##    pl.clf()
##    pl.plot(x_data*10**3, transLongIm, 'kx', label="Data")
##    pl.plot(xPlot*10**3, yPlotIm, 'r-', label="Fit")
##    pl.legend(loc="upper center")
##    pl.xlabel("Displacement (mm)", fontsize="16.0")
##    pl.ylabel("$\Re{}e (Z_{\parallel}) (\Omega/m)$", fontsize="16.0")
##    pl.savefig(savDir+'plot_'+str(i)+"_im.pdf")
##    pl.savefig(savDir+'plot_'+str(i)+"_im.png")
##    pl.savefig(savDir+'plot_'+str(i)+"_im.eps")
##    pl.clf()
##    
##    pinit = [0.0,1.0]
##    y_err=transLongRe
##    out = op.leastsq(linerrfunc, pinit, args=(x_data,transLongRe,y_err), full_output=1)
##    pfinalLin_real = out[0]
##    yPlotReLin = sp.polyval(pfinalLin_real, xPlot)
##    covarLin_real=out[1]
##    y_err = transLongIm
##    out = op.leastsq(linerrfunc, pinit, args=(x_data,transLongIm,y_err), full_output=1)
##    pfinalLin_imag = out[0]
####    print pfinal_real
##    yPlotImLin = sp.polyval(pfinalLin_imag, xPlot)    
##    horzConstAsym.append([pfinal_real[1]*C/(2*sp.pi*freq_list[i]), pfinalLin_imag[1]*C/(2*sp.pi*freq_list[i])])
##    pl.plot(x_data*10**3, transLongRe, 'kx', label="Data")
##    pl.plot(xPlot*10**3, yPlotRe, 'r-', label="Fit")
##    pl.legend(loc="upper center")
##    pl.xlabel("Displacement (mm)", fontsize="16.0")
##    pl.ylabel("$\Re{}e (Z_{\parallel}) (\Omega/m)$", fontsize="16.0")
##    pl.savefig(savDir+'linfitplot_'+str(i)+"_re.pdf")
##    pl.savefig(savDir+'linfitplot_'+str(i)+"_re.png")
##    pl.savefig(savDir+'linfitplot_'+str(i)+"_re.eps")
##    pl.clf()
##    pl.plot(x_data*10**3, transLongIm, 'kx', label="Data")
##    pl.plot(xPlot*10**3, yPlotIm, 'r-', label="Fit")
##    pl.legend(loc="upper center")
##    pl.xlabel("Displacement (mm)", fontsize="16.0")
##    pl.ylabel("$\Re{}e (Z_{\parallel}) (\Omega/m)$", fontsize="16.0")
##    pl.savefig(savDir+'linfitplot_'+str(i)+"_im.pdf")
##    pl.savefig(savDir+'linfitplot_'+str(i)+"_im.png")
##    pl.savefig(savDir+'linfitplot_'+str(i)+"_im.eps")
##    pl.clf()    
##
##    y_data = sp.linspace(-0.005,0.005,11)
##    yPlot = sp.linspace(-0.005,0.005,1000)
##    transLongRe = horzSingReYDat[i,:]
##    transLongIm = horzSingImYDat[i,:]
##    pinit = [0.0, 1.0, 1.0]
##    y_err = transLongRe
##    out = op.leastsq(errfunc, pinit, args=(x_data,transLongRe,y_err), full_output=1)
##    pfinal_real = out[0]
####    print pfinal_real
##    yPlotRe = sp.polyval(pfinal_real, xPlot)
##    covar_real=out[1]
##    y_err = transLongIm
##    out = op.leastsq(errfunc, pinit, args=(x_data,transLongIm,y_err), full_output=1)
##    pfinal_imag = out[0]
####    print pfinal_real
##    yPlotIm = sp.polyval(pfinal_imag, xPlot)    
##    vertLong.append([pfinal_real[2], pfinal_imag[2]])
##    vertConstX.append([pfinal_real[1]*C/(2*sp.pi*freq_list[i]), pfinal_imag[1]*C/(2*sp.pi*freq_list[i])])
##    vertTotTrans.append([pfinal_real[0]*C/(2*sp.pi*freq_list[i]),pfinal_imag[0]*C/(2*sp.pi*freq_list[i])])
##    pl.plot(y_data*10**3, transLongRe, 'kx', label="Data")
##    pl.plot(yPlot*10**3, yPlotRe, 'r-', label="Fit")
##    pl.legend(loc="upper center")
##    pl.xlabel("Displacement (mm)", fontsize="16.0")
##    pl.ylabel("$\Re{}e (Z_{\parallel}) (\Omega/m)$", fontsize="16.0")
##    pl.savefig(savDirVert+'plot_'+str(i)+"_re.pdf")
##    pl.savefig(savDirVert+'plot_'+str(i)+"_re.png")
##    pl.savefig(savDirVert+'plot_'+str(i)+"_re.eps")
##    pl.clf()
##    pl.plot(y_data*10**3, transLongIm, 'kx', label="Data")
##    pl.plot(yPlot*10**3, yPlotIm, 'r-', label="Fit")
##    pl.legend(loc="upper center")
##    pl.xlabel("Displacement (mm)", fontsize="16.0")
##    pl.ylabel("$\Re{}e (Z_{\parallel}) (\Omega/m)$", fontsize="16.0")
##    pl.savefig(savDirVert+'plot_'+str(i)+"_im.pdf")
##    pl.savefig(savDirVert+'plot_'+str(i)+"_im.png")
##    pl.savefig(savDirVert+'plot_'+str(i)+"_im.eps")
##    pl.clf()
##
##    pinit = [0.0,1.0]
##    y_err=transLongRe
##    out = op.leastsq(linerrfunc, pinit, args=(x_data,transLongRe,y_err), full_output=1)
##    pfinalLin_real = out[0]
##    yPlotReLin = sp.polyval(pfinalLin_real, xPlot)
##    covarLin_real=out[1]
##    y_err = transLongIm
##    out = op.leastsq(linerrfunc, pinit, args=(x_data,transLongIm,y_err), full_output=1)
##    pfinalLin_imag = out[0]
####    print pfinal_real
##    yPlotImLin = sp.polyval(pfinalLin_imag, xPlot)    
##    vertConstAsym.append([pfinal_real[1]*C/(2*sp.pi*freq_list[i]), pfinalLin_imag[1]*C/(2*sp.pi*freq_list[i])])
####    pl.plot(x_data*10**3, transLongRe, 'kx', label="Data")
####    pl.plot(xPlot*10**3, yPlotRe, 'r-', label="Fit")
####    pl.legend(loc="upper center")
####    pl.xlabel("Displacement (mm)", fontsize="16.0")
####    pl.ylabel("$\Re{}e (Z_{\parallel}) (\Omega/m)$", fontsize="16.0")
####    pl.savefig(savDir+'linfitplot_'+str(i)+"_re.pdf")
####    pl.savefig(savDir+'linfitplot_'+str(i)+"_re.png")
####    pl.savefig(savDir+'linfitplot_'+str(i)+"_re.eps")
####    pl.clf()
####    pl.plot(x_data*10**3, transLongIm, 'kx', label="Data")
####    pl.plot(xPlot*10**3, yPlotIm, 'r-', label="Fit")
####    pl.legend(loc="upper center")
####    pl.xlabel("Displacement (mm)", fontsize="16.0")
####    pl.ylabel("$\Re{}e (Z_{\parallel}) (\Omega/m)$", fontsize="16.0")
####    pl.savefig(savDirVert+'linfitplot_'+str(i)+"_im.pdf")
####    pl.savefig(savDirVert+'linfitplot_'+str(i)+"_im.png")
####    pl.savefig(savDirVert+'linfitplot_'+str(i)+"_im.eps")
####    pl.clf()
##
##    ####### Asymmetric Section ########
##
##    disp = sp.linspace(0.005,0.001,5)
##    transLongHorzRe = horzSingReXDat[i,:]
##    transLongHorzIm = horzSingImXDat[i,:]
##    transLongVertRe = horzSingReYDat[i,:]
##    transLongVertIm = horzSingImYDat[i,:]
##    tmpRe = []
##    tmpIm = []
##    for k in range(0,len(transLongHorzRe)/2):
##        transPerRe = (1/(2*disp[k]**2))*transLongVertRe[k]+transLongVertRe[-(1+k)]-(transLongHorzRe[k]+transLongHorzRe[-(1+k)])
##        transPerIm = (1/(2*disp[k]**2))*transLongVertIm[k]+transLongVertIm[-(1+k)]-(transLongHorzIm[k]+transLongHorzIm[-(1+k)])
##        tmpRe.append(transPerRe*C/(2*sp.pi*freq_list[i]))
##        tmpIm.append(transPerIm*C/(2*sp.pi*freq_list[i]))
##
##    horzQuadStarReAsym.append(tmpRe)
##    horzQuadStarImAsym.append(tmpIm)
##   
##horzLong = [horzSingReXDat[:,4], horzSingImXDat[:,4]]
##
##horzLong = sp.array(horzLong)
##horzConstX = sp.array(horzConstX)
##horzTotTrans = sp.array(horzTotTrans)
##vertLong = sp.array(vertLong)
##vertConstX = sp.array(vertConstX)
##vertTotTrans = sp.array(vertTotTrans)
##vertConstAsym = sp.array(vertConstAsym)
##horzConstAsym = sp.array(horzConstAsym)
##
##horzQuadStarReAsym = sp.array(horzQuadStarReAsym)
##horzQuadStarImAsym = sp.array(horzQuadStarImAsym)
##
##
##test = horzQuadStarReAsym[:,0]+(horzDipReXDat-horzDipReYDat)/2
##test1 = horzQuadStarImAsym[:,0]+(horzDipImXDat-horzDipImYDat)/2
##pl.semilogx()
####pl.loglog()
####pl.semilogy()
##pl.plot(freq_list, test/20, 'kx', markersize=16, label="Simulation $\Re{}e(Z_{\perp, y}^{quadrupolar})$")
##pl.plot(freq_list, test1/20, 'rx', markersize=16, label="Simulation $\Im{}m(Z_{\perp, y}^{quadrupolar})$")
##pl.plot(quad_theory[:,0], quad_theory[:,1], 'b-', label="Theory $\Re{}e(Z_{\perp, y}^{quadrupolar})$")
##pl.plot(quad_theory[:,0], quad_theory[:,2], 'r-', label="Theory $\Im{}m(Z_{\perp, y}^{quadrupolar})$")
##pl.axis([10**6,10**10,-10**5, 2.5*10**5])
##pl.xlabel("Frequency (Hz)", fontsize="16.0")
##pl.ylabel("$(Z_{\perp, Quadrupolar}) (\Omega/m^{2})$", fontsize="16.0")
##pl.legend(loc="upper right")
##pl.show()
##pl.savefig(savDirVert+"asymQuad.pdf")
##pl.savefig(savDirVert+"asymQuad.eps")
##pl.savefig(savDirVert+"asymQuad.png")
##pl.clf()
##
##
